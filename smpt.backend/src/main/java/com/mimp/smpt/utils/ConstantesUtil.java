package com.mimp.smpt.utils;

public class ConstantesUtil {

    public static final String FORMATO_FECHA_CORTO = "dd/MM/yyyy";
    public static final String FORMATO_FECHA_RAYA_CORTO = "dd-MM-yyyy";
    public static final String FORMATO_FECHA_LARGO = "dd/MM/yyyy HH:mm:ss";

    public static final String IND_ACTIVO = "1";
    public static final String IND_INACTIVO = "0";

    public static final String INCLUIDO_ACTA_SUPERVISION_SI = "1";
    public static final String INCLUIDO_ACTA_SUPERVISION_NO = "0";

    /**
     * Parámetro tipo de documento de identidad "TIPO_DOCUMENTO_IDENTIDAD"
     * registrado en la tabla PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_DOCUMENTO_IDENTIDAD = "TIPO_DOCUMENTO_IDENTIDAD";
    public static final String PARAM_TIPO_DOCUMENTO_IDENTIDAD_RUC = "TIPO_DOCUMENTO_IDENTIDAD_RUC";
    public static final String PARAM_TIPO_DOCUMENTO_IDENTIDAD_DNI_CE = "TIPO_DOCUMENTO_IDENTIDAD_DNI_CE";
    public static final String PARAM_TIPO_DOCUMENTO_IDENTIDAD_DNI = "TIPO_DOCUMENTO_IDENTIDAD_DNI";
    public static final String PARAM_TIPO_DOCUMENTO_IDENTIDAD_CE = "TIPO_DOCUMENTO_IDENTIDAD_CE";

    public static final String PARAM_TIPO_INVOLUCRADO = "TIPO_INVOLUCRADO";
    public static final String PARAM_TIPO_PREFIJO = "TIPO_PREFIJO";

    /**
     * Parámetro que sostiene el valor de identificación interno del tipo de Prefijo
     * involucrado
     */
    public static final Long PARAM_TIP_PREFIJO_INVOLUCRADO = 29L;

    public static final Long PARAM_TIP_REL_PASO_CANCELARFLUJO = 291L;
    public static final Long PARAM_TIP_REL_PASO_FINALIZADOFLUJO = 292L;

    /**
     * flujos cancelados y finalizados Parámetro que sostiene el valor de
     * identificación interno del tipo de Acta involucrado
     */
    public static final Long PARAM_TIP_ACTA_INVOLUCRADO = 30L;
    public static final Long PARAM_TIP_ACTA_INICIO = 317L;
    public static final Long PARAM_TIP_ACTA_SUPERVISION = 318L;

    /**
     * Parámetro que sostiene el valor de identificación interno del tipo de
     * Involucrado
     */
    public static final Long PARAM_TIP_INVOLUCRADO = 28L;
    /**
     * Parámetro que sostiene el valor de identificación interno del tipo de
     * Involucrado
     */
    public static final Long PARAM_TIP_ESTADO_CONFIGURACION = 44L;

    /**
     * Parámetro división supervisora "DIVISION_SUPERVISORA" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_DIVISION_SUPERVISORA = "DIVISION_SUPERVISORA";

    /**
     * Parámetro división supervisora "SITUACION_UNIDAD_MINERA" registrado en la
     * tabla PGIM_TP_PARAMETRO
     */
    public static final String PARAM_SITUACION_UNIDAD_MINERA = "SITUACION_UNIDAD_MINERA";

    /**
     * Parámetro tipo de actividad "TIPO_ACTIVIDAD" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_ACTIVIDAD = "TIPO_ACTIVIDAD";

    /**
     * Parámetro división supervisora "TIPO_UNIDAD_MINERA" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_UNIDAD_MINERA = "TIPO_UNIDAD_MINERA";

    /**
     * Parámetro división supervisora "METODO_MINADO" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_METODO_MINADO = "METODO_MINADO";

    /**
     * Parámetro división supervisora "TIPO_YACIMIENTO" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_YACIMIENTO = "TIPO_YACIMIENTO";

    /**
     * Parámetro división supervisora "TIPO_SUSTANCIA" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_SUSTANCIA = "TIPO_SUSTANCIA";

    /**
     * Parámetro categoría ocupacional "CATEGORIA_OCUPACIONAL" registrado en la
     * tabla PGIM_TP_PARAMETRO
     */
    public static final String PARAM_CATEGORIA_OCUPACIONAL = "CATEGORIA_OCUPACIONAL";

    /**
     * Parámetro tipo de documento de identidad "TIPO_ACTIVIDAD_CIIU" registrado en
     * la tabla PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_ACTIVIDAD_CIIU = "TIPO_ACTIVIDAD_CIIU";

    /**
     * Parámetro tipo de origen de documento "TIPO_ORIGEN_DOCUMENTO" registrado en
     * la tabla PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_ORIGEN_DOCUMENTO = "TIPO_ORIGEN_DOCUMENTO";

    /**
     * Parámetro tipo de cumplimiento "TIPO_CUMPLIMIENTO" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_CUMPLIMIENTO = "TIPO_CUMPLIMIENTO";

    /**
     * Parámetro No aplica de la entidad tipo de cumplimiento "TIPO_CUMPLIMIENTO"
     * registrado en la tabla PGIM_TP_VALOR_PARAMETRO
     */

    public static final Long PARAM_COD_NINGUNO_TC = 0L;
    public static final Long PARAM_COD_NO_APLICA_TC = 1L;
    public static final Long PARAM_COD_CUMPLE_TC = 2L;
    public static final Long PARAM_COD_NO_CUMPLE_TC = 3L;

    public static final Long PARAM_ID_NO_APLICA_TC = 309L;
    public static final Long PARAM_ID_CUMPLE_TC = 307L;
    public static final Long PARAM_ID_NO_CUMPLE_TC = 308L;

    /**
     * Parámetro especialidad "ESPECIALIDAD_SUPERVISION" registrado en la tabla
     * PGIM_TM_ESPECIALIDAD
     */
    public static final String PARAM_TIPO_ESPECIALIDAD_SUPERVISION = "ESPECIALIDAD_SUPERVISION";

    /**
     * Parámetro especialidad "ESPECIALIDAD_SUPERVISION" registrado en la tabla
     * PGIM_TM_ESPECIALIDAD
     */
    public static final String PARAM_TIPO_ESTADO_CONFIGURACION = "ESTADO_CONFIGURACION";

    /**
     * Parámetro relacionado con los plazos de revisión de informes de supervisión.
     */
    public static final String PARAM_PLAZOS_REVISION_INFORME = "PLAZOS_REVISION_INFORME";

    /**
     * Valor del parámetro de la entrega del informe para revisión.
     */
    public static final Long PARAM_PLAZOS_REVINFORME_ENTREGA_CO_CLAVE = 1L;

    /**
     * Parámetro de tipo supervision "TIPO_SUPERVISION" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_SUPERVISION = "TIPO_SUPERVISION";

    /**
     * Parámetro de nombre de programa supervision "NOMBRE_PROGRAMA_SUPERVISION"
     * registrado en la tabla PGIM_TC_PRGRM_SUPERVISION
     */
    public static final String PARAM_NOMBRE_PROGRAMA_SUPERVISION = "NOMBRE_PROGRAMA_SUPERVISION";

    /**
     * Parámetro de nombre de contrato supervision "NOMBRE_CONTRATO_SUPERVISION"
     * registrado en la tabla PGIM_TC_CONTRATO
     */
    public static final String PARAM_NOMBRE_CONTRATO_SUPERVISION = "NOMBRE_CONTRATO_SUPERVISION";

    /**
     * Parámetro de nombre fase proceso "FASE_PROCESO" registrado en la tabla
     * PGIM_FASE_PROCESO
     */
    public static final String PARAM_NOMBRE_FASE_PROCESO = "FASE_PROCESO";

    /**
     * Parámetro de nombre paso proceso "PASO_PROCESO" registrado en la tabla
     * PGIM_PASO_PROCESO
     */
    public static final String PARAM_NOMBRE_PASO_PROCESO = "PASO_PROCESO";

    public static final Long PARAM_TIPO_SUPERVISION_PROGRAMADA = 287L;

    public static final Long PARAM_TIPO_SUPERVISION_NOPROGRAMADA = 288L;

    /**
     * Parámetro que sostiene el valor de identificación interno del tipo de
     * documento RUC.
     */
    public static final Long PARAM_TDI_RUC_DNI_CE = 3L;
    public static final Long PARAM_TDI_RUC = 6L;
    public static final Long PARAM_TDI_DNI = 5L;
    public static final Long PARAM_TDI_CE = 7L;

    public static final String PARAM_TIPO_SEGURO = "TIPO_SEGURO";

    public static final String PARAM_TIPO_EVENTO = "TIPO_EVENTO";

    public static final String PARAM_AGENTE_CAUSANTE = "AGENTE_CAUSANTE";

    public static final String PARAM_TIPO_ACCIDENTE = "TIPO_ACCIDENTE";

    public static final String PARAM_TIPO_INCIDENTE = "TIPO_INCIDENTE";

    public static final String PARAM_TIPO_AUTORIZACION = "TIPO_AUTORIZACION";

    /**
     * Nombre del Estrato que sostiene el valor de identificación interno del
     * idEstrato
     */
    public static final String NOMBRE_ESTRATO_AGENTE_SUPERVISADO = "NOMBRE_ESTRATO";

    /**
     * Número de contrato nuContrato
     */
    public static final String NUMERO_CODIGO_CONTRATO = "CODIGO_CONTRATO";

    /**
     * Parámetro que sostiene el valor de identificación interno del tipo unidad
     * minera de concesión de beneficio.
     */
    public static final Long PARAM_TUM_ID_CONCESION_BENEFICIO = 36L;

    /**
     * Parámetro que sostiene el valor de identificación interno de la empresa
     * involucrada como TITULAR_MINERO.
     */
    public static final Long PARAM_TEI_TITULAR_MINERO = 65L;

    /**
     * Parámetro que sostiene el valor de identificación interno de la empresa
     * involucrada como CONTRATISTA.
     */
    public static final Long PARAM_TEI_CONTRATISTA = 66L;

    public static final Long PARAM_TE_ACCIDENTE_MORTAL = 63L;

    public static final Long PARAM_TE_INCIDENTE_PELIGROSO = 64L;

    public static final String PARAM_TAMANIO_EMPRESA = "TAMANIO_EMPRESA";

    public static final Long PARAM_TUM_CONCESION_BENEFICIO = 36L;

    // Constantes de Procesos

    public static final Long PARAM_PROCESO_EVENTO = 1L;

    public static final Long PARAM_PROCESO_AUTORIZACION = 6L;

    public static final Long PARAM_PROCESO_DENUNCIA = 7L;

    public static final Long PARAM_PROCESO_SUPERVISION = 2L;

    public static final Long PARAM_PROCESO_UNIDAD_MINERA = 3L;

    public static final Long PARAM_PROCESO_FISCALIZACION = 4L;

    public static final Long PARAM_PROCESO_LIQUIDACION = 10L;

    public static final Long PARAM_PROCESO_CONTRATO = 5L;

    public static final Long PARAM_PROCESO_PROGRAMACION = 9L;

    public static final Long PARAM_PROCESO_RANKING_RIESGOS = 11L;

    public static final Long PARAM_PROCESO_CONFIGURACION_RIESGO = 12L;

    public static final Long PARAM_PROCESO_MEDIDA_ADM = 13L;

    public static final Long PARAM_MEDIDA_ADM_UNIDAD_MINERA = 405L;

    public static final Long PARAM_MEDIDA_ADM_SUPERVISION = 406L;

    public static final Long PARAM_MEDIDA_ADM_FISCALIZACION = 407L;

    public static final Long PARAM_ACTA_SUPERVISION = 27L;

    public static final Long PARAM_INFORME_SUPERVISION = 29L;

    public static final Long PARAM_INFORME_SUPERVISION_PROPIA = 163L;

    public static final Long PARAM_INFORME_SUPERVISION_FALLIDA = 70L;

    public static final Long PARAM_INFORME_SUPERVISION_FALLIDA_PROPIA = 164L;

    public static final Long PARAM_LIQUIDACION_INFORME_GESTION = 63L;

    public static final String PARAM_TABLA_TC_SUPERVISION = "PGIM_TC_SUPERVISION";
    public static final String PARAM_TABLA_TM_EVENTO = "PGIM_TM_EVENTO";
    public static final String PARAM_TABLA_TM_AUTORIZACION = "PGIM_TM_AUTORIZACION";
    public static final String PARAM_TABLA_TM_DENUNCIA = "PGIM_TM_DENUNCIA";
    public static final String PARAM_TABLA_TM_UNIDAD_MINERA = "PGIM_TM_UNIDAD_MINERA";
    public static final String PARAM_TABLA_TC_CONTRATO = "PGIM_TC_CONTRATO";
    public static final String PARAM_TABLA_TC_LIQUIDACION = "PGIM_TC_LIQUIDACION";
    public static final String PARAM_TABLA_TC_PAS = "PGIM_TC_PAS";
    public static final String PARAM_TABLA_TC_PRGRM_SUPERVISION = "PGIM_TC_PRGRM_SUPERVISION";
    public static final String PARAM_TABLA_TC_RANKING_RIESGO = "PGIM_TC_RANKING_RIESGO";
    public static final String PARAM_TABLA_TM_CONFIGURA_RIESGO = "PGIM_TM_CONFIGURA_RIESGO";
    public static final String PARAM_TABLA_TC_MEDIDA_ADM = "PGIM_TC_MEDIDA_ADM";

    /**
     * Parámetros de integración SYM.
     */
    public static final String PARAM_SYM_WEB_INICIO_SYM = "pages/inicioSYM/inicio";
    public static final String PARAM_SYM_WEB_MAS_INFORMACION = "pages/infExpediente/inicio";
    public static final String PARAM_SYM_REST_TOKEN = "remote/sym/symSeguridad/getToken";
    public static final String PARAM_SYM_REST_LISTAR_EXP_REPORTE = "remote/sym/expediente/listarExpedienteParaReporteBean/";
    public static final String PARAM_SYM_REST_BUSCAR_INFRACCION = "remote/sym/infraccion/buscarInfraccion/";
    public static final String PARAM_SYM_INVOKER = "PGIM";
    public static final String PARAM_SYM_SESSION = "$2a$10$0tpuXgJVlTMR4xNnKejyZ.Qmkoi01KLiK25HJow/4Jl";// llave para
                                                                                                        // encriptar en
                                                                                                        // la pgim, y
                                                                                                        // para
                                                                                                        // desencriptar
                                                                                                        // en el SYM-WEB

    /**
     * Parámetro de integración SIGED.
     */
    // public static final String PARAM_SIGED_EXPEDIENTE_DOCUMENTO =
    // "remote/expediente/documentos/{nroexp}/{files}";
    // public static final String PARAM_SIGED_EXPEDIENTE_CREAR =
    // "remote/expediente/crear";
    // public static final String PARAM_SIGED_DOCUMENTO_ARCHIVO =
    // "remote/archivo/list/{iddocumento}";
    // public static final String PARAM_SIGED_AGREGAR_DOCUMENTO =
    // "remote/expediente/agregarDocumento/{versionar}";
    // public static final String PARAM_SIGED_AGREGAR_ARCHIVO =
    // "remote/documento/agregarArchivo/{versionar}";
    // public static final String PARAM_SIGED_DESCARGAR_ARCHIVO =
    // "remote/archivo/descarga/{idarchivo}";
    // public static final String PARAM_SIGED_ANULAR_ARCHIVO =
    // "remote/nuevodocumento/anularArchivo";
    // public static final String PARAM_SIGED_ANULAR_DOCUMENTO =
    // "remote/documento/anular";
    // public static final String PARAM_SIGED_OBTENER_USUARIO =
    // "remote/usuario/obtenerUsuario";

    public static final String PARAM_SIGED_REVERTIR_FIRMA = "remote/firmaDigital/revertirFirma";

    public static final String PARAM_SIGED_ENUMERAR_DOCUMENTO = "remote/nuevodocumento/enumerarDocumento/{nroExpediente}/{idDocumento}/{idUsuarioEnumerador}";
    public static final String PARAM_SIGED_EXPEDIENTE_REENVIAR = "remote/expediente/reenviar/{aprobacion}";
    public static final String PARAM_SIGED_ES_FERIADO = "remote/ubigeo/esFeriado";

    public static final String PARAM_SIGED_EXPEDIENTE_LISTAR_POR_USUARIO = "remote/expediente/listByUser/{iduser}/{estado}";

    public static final String PARAM_SIGED_DEVOLVER_EXPEDIENTE = "remote/nuevodocumento/devolverExpediente";
    public static final String PARAM_SIGED_APROBAR_EXPEDIENTE = "remote/expediente/aprobar/{paraAprobacion}";

    public static final String PARAM_SIGED_CLIENTE_CONSULTA = "remote/cliente/find";
    public static final String PARAM_SIGED_LISTADO_AREAS = "remote/unidad/list";

    public static final String PARAM_SURVEY_ADD_FEATURES = "FeatureServer/1/addFeatures";
    public static final String PARAM_SURVEY_UPDATE_FEATURES = "FeatureServer/1/updateFeatures";

    public static final String PARAM_REQUEST_METHOD_GET = "GET";
    public static final String PARAM_REQUEST_METHOD_POST = "POST";
    public static final String PARAM_REQUEST_PROPERTY_ACCEPT_ENCODING = "Accept-Encoding";
    public static final String PARAM_REQUEST_PROPERTY_CONTENT_TYPE = "Content-Type";
    public static final String PARAM_REQUEST_PROPERTY_USER_AGENT = "User-Agent";

    /**
     * Parámetro resultante de la operación correcta tras la llamada del método del
     * servicio web del Siged.
     */
    public static final String PARAM_RESULTADO_SUCCESS = "1";
    public static final String PARAM_RESULTADO_FAIL = "0";
    public static final String PARAM_RESULTADO_FAIL_DOS = "2";
    public static final String PARAM_RESULTADO_ERROR_EXCEPTION = "500";
    public static final String PARAM_appNameInvokes = "pgim";

    public static final String PARAM_codigoTipoIdentificacion = "1";
    public static final String PARAM_razonSocial = "Osinergmin";
    public static final String PARAM_nroIdentificacion = "20376082114";
    public static final String PARAM_tipoCliente = "3";

    public static final String PARAM_estaEnFlujo = "S";

    /**
     * Constante que señala si se señala como firmado o no el documento. Se ha
     * colocado el valor 78 porque N, indicada por la especificación del Siged no
     * funciona como debe.
     */
    public static final String PARAM_negativo = "78";

    public static final String PARAM_afirmativo = "83";

    public static final String PARAM_creaExpediente = "N";

    public static final String PARAM_creaExpedienteSi = "S";

    public static final String PARAM_nroFolios = "0";

    public static final String PARAM_publico = "N";

    // public static final String PARAM_usuarioCreador = "7748";
    // public static final String PARAM_usuarioCreador = "2530";

    public static final String PARAM_ubigeo = "150101";

    public static final String PARAM_expedienteFisico = "N";

    public static final String PARAM_historico = "N";

    // public static final String PARAM_destinatario = "7748";

    public static final String PARAM_documentoPrincipal = "83";

    public static final String PARAM_noPropietario = "701";

    public static final String PARAM_mismoPropietario = "702";

    public static final String PARAM_noConfigurado = "703";

    public static final String MENSAJE_ErrorSiged = "Siged reporta un problema al ejecutar %s, el código del error es: [%s] y el mensaje: [%s])";

    public static final String MENSAJE_ErrorSNE = "SNE reporta un problema al ejecutar %s, el código del error es: [%s] y el mensaje: [%s])";

    /**
     * Constante para la generación de documentos.
     */
    public static final Long PARAM_tipo_doc_DJI = 92L;

    public static final Long PARAM_TIPO_DOC_OTROS = 4L;

    /**
     * Ficha de hechos constatados
     */
    public static final Long PARAM_SUBCAT_DOC_HC = 31L;

    /**
     * Ficha de hechos constatados - con las anoaciones del especialista técnico
     * legal
     */
    public static final Long PARAM_SUBCAT_DOC_FHC = 32L;

    /**
     * Ficha de instrucción preliminar.
     */
    public static final Long PARAM_SUBCAT_DOC_FIP = 39L;

    /**
     * Observaciones al informe de supervisión.
     */
    public static final Long PARAM_SUBCAT_DOC_OIS = 33L;

    /**
     * Conformidades al informe de supervisión.
     */
    public static final Long PARAM_SUBCAT_DOC_CIS = 34L;

    /**
     * Informe de supervisión.
     */
    public static final Long PARAM_SUBCAT_DOC_IS = 29L;

    /**
     * Informe de gestión.
     */
    public static final Long PARAM_SUBCAT_DOC_IG = 63L;

    /**
     * Acta de supervisión.
     */
    public static final Long PARAM_SUBCAT_DOC_AS = 27L;

    /**
     * Informe de supervisión fallida.
     */
    public static final Long PARAM_SUBCAT_DOC_ISF = 69L;

    /**
     * Informe IAIP.
     */
    public static final Long PARAM_SUBCAT_DOC_IAIP = 41L;
    /**
     * Formato de Liquidación de actas
     */
    public static final Long PARAM_SUBCAT_DOC_FLA = 61L;
    /**
     * Formato de Liquidación de informe
     */
    public static final Long PARAM_SUBCAT_DOC_FLI = 62L;
    /**
     * Formato de Liquidación de informe de gestión
     */
    public static final Long PARAM_SUBCAT_DOC_FLIGES = 63L;
    /**
     * Formato de Liquidación de informe de supervisión fallida
     */
    public static final Long PARAM_SUBCAT_DOC_FLISF = 69L;
    /**
     * Formato de Liquidación de informe
     */
    public static final Long PARAM_SUBCAT_DOC_PEN = 64L;

    /**
     * Autorización de ampliación.
     */
    public static final Long PARAM_SUBCAT_DOC_AUTOAMP = 36L;

    public static final Long PARAM_SUBCAT_DOC_CREDENCIAL = 7L;

    public static final Long PARAM_TIPO_DOC_TDR = 269L;

    public static final Long PARAM_id_rol_SUPERVISOR = 4L;

    /**
     * Código del rol de proceso interesado.
     */
    public static final String PROCESO_CO_ROL_INTERESADO = "INTE";

    public static final Long PARAM_TIPO_FECHA_PROGRAMADA = 293L;

    public static final Long PARAM_TIPO_FECHA_REAL = 294L;

    /**
     * Tiempo de espera de los métodos de integración con el Siged. Se encuentra en
     * milisegundos.
     */
    public static final int PARAM_TIMEOUT_SIGED = 60000;

    /**
     * Parámetro que sostiene la transición de pasos en el marco de un proceso -
     * flujo de Supervisión
     */
    public static final Long PARAM_RELACION_ASIGNAR_COORDSUPER = 1L;

    public static final Long PARAM_RELACION_COORDSUPER_FIRMARDJI = 2L;

    public static final Long PARAM_RELACION_COORD_SUPER_CANCELAR_SUPER = 3L;

    public static final Long PARAM_RELACION_FIRMARDJI_GENTDR = 4L;

    public static final Long PARAM_RELACION_GENERA_TDR_CREDENCIAL_CANCELAR_SUPER = 6L;

    public static final Long PARAM_RELACION_APROBAR_TDR_CREDENCIAL_DEFINIR_ANTECEDENTES = 10L;

    public static final Long PARAM_RELACION_GESTIONAR_SALDO_CANCELAR_SUPER = 12L;

    public static final Long PARAM_RELACION_DEFINIR_ANTECEDENTES_CANCELAR_SUPER = 14L;

    public static final Long PARAM_RELACION_APROBAR_REV_ANTECEDENTES_INICIASUPERV = 16L;

    public static final Long PARAM_RELACION_APROBAR_REV_ANTECEDENTES_CANCELAR_SUPER = 17L;

    public static final Long PARAM_RELACION_FIRMARMEMO_COMPLETARSUPFALLIDA = 21L;

    public static final Long PARAM_RELACION_INICIASUPERV_SOLDOCCAMPO = 22L;

    public static final Long PARAM_RELACION_PREACTASUPER_ELAINFO = 25L;

    // public static final Long PARAM_RELACION_ELAINFO_REVINFO = 26L; // constante
    // repetida
    public static final Long PARAM_RELACION_ELABORARINFORME_APROBARINFORME = 26L;

    public static final Long PARAM_RELACION_CONFIRMARHC_REVISARHC = 28L;

    public static final Long PARAM_RELACION_GENTDR_FIRMARDJI = 44L;

    public static final Long PARAM_RELACION_REVISARHC_CONTINUARPAS = 86L;

    public static final Long PARAM_RELACION_CONFIRMARHC_CONTINUARARCH = 87L;

    public static final Long PARAM_RELACION_INISUPCAMP_REPROSUP = 88L;

    public static final Long PARAM_RELACION_SOLDOCUM_REPROSUP = 89L;

    public static final Long PARAM_RELACION_MEDICIONES_REPROSUP = 90L;

    public static final Long PARAM_RELACION_PRESENTARACTA_REPROSUP = 91L;

    public static final Long PARAM_RELACION_VISARHC_SUPCOMPLETA = 97L;

    public static final Long PARAM_RELACION_COORSUPER_GENERARTDR = 98L;

    public static final Long PARAM_RELACION_REVISARHC_CONTINUARARCH = 99L;

    public static final Long PARAM_SC_FICHA_CONFORMIDAD_INFORME_SUPERVISION = 34L;

    public static final Long PARAM_SC_RESPUESTA_SOLICITUD_AMPLIACIÓN_PERIODO_SUPERVISION = 36L;

    public static final Long PARAM_RP_REVISAR_APROBAR_INF_SUP_CONF_HECHOS_CONSTATADOS_INFRACCIONES = 27L;

    public static final Long PARAM_RP_APROBAR_SOLICITUD_AMPLIACIÓN_PLAZO_CONF_HECHOS_CONSTATADOS_INFRACCIONES = 42L;

    public static final Long PARAM_RP_VERIF_NOTIF_FISICA_OFICIO_CONCLUSION_CONF_HECHOS_CONSTATADOS_INFRACCIONES = 737L;

    public static final Long PARAM_RP_VERIF_NOTIF_ELECTR_OFICIO_CONCLUSION_CONF_HECHOS_CONSTATADOS_INFRACCIONES = 741L;

    public static final Long PARAM_SC_INFORME_ARCHIVADO_INSTRUCCIÓN_PRELIMINAR = 41L;

    public static final Long PARAM_RELACION_ELABORAR_INFOR_CONF_HECHOS_CONSTATADOS_INFRACCIONES = 45L;

    public static final Long PARAM_RP_CONTINUAR_ARCHIVADO_APROBAR_IAIP = 34L;

    public static final Long PARAM_RP_CONTINUAR_PAS_COMPLETAR_SUPERVISIÓN_INICIO_PAS = 32L;

    public static final String PARAM_TIPO_COMPONENTE_MINERO = "TIPO_COMPONENTE_MINERO";

    public static final Long PARAM_SC_INFORME_INSTRUCCION_INICIO_PAS = 42L;

    /**
     * parámetros de relación de pasos del flujo de Programación
     */
    public static final Long PARAM_RELACION_ELABORAR_PROG_VISAR_PROG = 50L;

    public static final Long PARAM_RELACION_VISAR_PROG_REVISAR_PROG = 51L;

    public static final Long PARAM_RELACION_APROBAR_PROG_REALIZAR_SEGUIMIENTO = 55L;

    public static final Long PARAM_RELACION_REALIZAR_SEGUIMIENTO_CERRAR_PROG = 57L;

    public static final Long PARAM_RELACION_REALIZAR_SEGUIMIENTO_ELABORAR_PROG = 58L;

    public static final Long PARAM_RELACION_VISAR_PROG_ANULAR_LB = 82L;

    public static final Long PARAM_RELACION_ANULAR_LB_REALIZAR_SEGUIMIENTO = 84L;

    /**
     * parámetros de relación de pasos del flujo de liquidación
     */

    public static final Long PARAM_RELACION_ELABORARSOLLQ_REVISARSOLLIQ = 60L;

    public static final Long PARAM_RELACION_REVISARSOLLIQ_APROBARLIQ = 63L;

    public static final Long PARAM_RELACION_APROBARLIQ_TRAMITARLIQ = 65L;

    public static final Long PARAM_RELACION_ELABORARSOLLQ_LIQANULADA = 61L;

    public static final Long PARAM_RELACION_TRAMITARLIQ_REGISPENALIDAD = 67L;

    public static final Long PARAM_RELACION_REVISARPENAL_ENVIARLIQPAGO = 73L;

    public static final Long PARAM_RELACION_REVISAROBSPNENAL_ENVIARLIQPAGO = 76L;

    public static final Long PARAM_RELACION_ENVIARLIQPAGO_REVISARSOLLIQ = 77L;

    public static final Long PARAM_RELACION_ENVIARLIQPAGO_TRAMFACTURA = 78L;

    public static final Long PARAM_RELACION_COMPLETARLIQ_LIQCOMPLETA = 81L;

    public static final Long PARAM_RELACION_TRAMILIQ_ENVIARLIQPAGO = 85L;

    /**
     * Parámetro que sostiene la transición de pasos en el marco de un proceso -
     * flujo de Generación de Ranking
     */
    public static final Long PARAM_RELACION_ASIGNAR_CALIFICARIESGO = 500L;

    public static final Long PARAM_RELACION_CALIFICARIESGO_CANCELARANKING = 501L;

    public static final Long PARAM_RELACION_CANCELARANKING_RANKINGCANCELADO = 502L;

    public static final Long PARAM_RELACION_CALIFICARIESGO_APRUEBARANKING = 503L;

    public static final Long PARAM_RELACION_APRUEBARANKING_RANKINGAPROBADO = 504L;

    public static final Long PARAM_RELACION_APRUEBARANKING_CALIFICARIESGO = 505L;

    /**
     * Parámetros para la gestión de alertas
     */
    public static final String PARAM_ENLACE_REENVIAR_SUP = "/supervision/supervisiones/2/%s/1/0";

    public static final String PARAM_ENLACE_REENVIAR_LIQ = "/contratos/liquidacion/2/%s/1/0";

    // public static final String PARAM_ENLACE_REENVIAR_PRO =
    // "/programacion/2/%s/0";

    public static final String PARAM_ENLACE_REENVIAR_PRO = "/programacion/2/%s/1/0";

    public static final Long PARAM_ALERTA_PASO = 299L;

    public static final Long PARAM_ALERTA_SUP_INI = 300L;

    public static final Long PARAM_ALERTA_SUPER_INICIADA_REL = 16L;

    public static final Long PARAM_ALERTA_SUP_FALL = 301L;

    public static final Long PARAM_ALERTA_SUPER_FALLIDA_REL = 18L;

    public static final Long PARAM_ALERTA_SUP_FIN = 302L;

    public static final Long PARAM_ALERTA_SUPER_FINALIZADA_REL = 25L;

    public static final Long PARAM_ALERTA_SUP_CAN = 303L;

    public static final Long PARAM_ALERTA_SUPER_CANCELADA_REL1 = 3L;

    public static final Long PARAM_ALERTA_SUPER_CANCELADA_REL2 = 6L;

    public static final Long PARAM_ALERTA_SUPER_CANCELADA_REL3 = 12L;

    public static final Long PARAM_ALERTA_SUPER_CANCELADA_REL4 = 14L;

    public static final Long PARAM_ALERTA_SUPER_CANCELADA_REL5 = 17L;

    public static final Long PARAM_ALERTA_SUP_IAIP = 304L;

    public static final Long PARAM_DETALLE_ALERTA_ASIGNACION = 305L;

    public static final Long PARAM_DETALLE_ALERTA_SUP_CONOCIMIENTO = 306L;

    public static final Long PARAM_ALERTA_SUPER_FINALIZADA_IAIP_REL = 35L;

    public static final String PARAM_NO_ALERTA_SUP = "Supervisión: %s - Código %s";

    public static final String PARAM_NO_ALERTA_LIQ = "Liquidación: %s - %s";

    public static final String PARAM_NO_ALERTA_PRO = "Programa: %s - %s";

    public static final String PARAM_NO_ALERTA_FIS = "Fiscalización: %s - %s";

    public static final String PARAM_ALERTA_TRANS_PASO = "Asignación de paso realizada: <br><b>%s</b> (%s) ---> <b>%s</b> (%s)";

    public static final String PARAM_DETALLE_ALERTA_PASO = "Se le ha asignado un <b>nuevo paso en la %s</b>, por favor revise el detalle en el objeto de trabajo.";

    public static final String PARAM_DETALLE_ALERTA_RESPONSABLE = "Se le informa, en su calidad de <b>responsable</b> que ha ocurrido una <b>nueva asignación de paso en la %s</b>, por favor revise el detalle en el objeto de trabajo.";

    public static final String PARAM_DETALLE_ALERTA_SUP_INTERESADO = "Se le informa, en su calidad de <b>interesado/a</b> que ha ocurrido una <b>nueva asignación de paso en la supervisión</b>, por favor revise el detalle en el objeto de trabajo.";

    public static final String PARAM_SIGED_LOG_DE_USO_INI = "SIGED: Inicia llamada al servicio %s";

    public static final String PARAM_SIGED_LOG_DE_USO_FIN = "SIGED: Finaliza llamada al servicio %s";

    public static final String PARAM_SIGED_LOG_DE_USO_FIN_ERROR = "SIGED: Finaliza llamada al servicio con error %s";

    public static final String PARAM_SURVEY_LOG_DE_USO_INI = "SURVEY: Inicia llamada al servicio %s";

    public static final String PARAM_SURVEY_LOG_DE_USO_FIN = "SURVEY: Finaliza llamada al servicio %s";

    public static final String PARAM_SURVEY_LOG_DE_USO_FIN_ERROR = "SURVEY: Finaliza llamada al servicio con error %s";

    /**
     * Parámetros de estado del hecho constatado.
     */
    public static final String PARAM_HC_ESTADO_VIGENTE = "V";

    public static final String PARAM_HC_ESTADO_HISTORICO = "H";

    public static final Long PARAM_HC_ROL_SUPERVISOR = 4L;

    public static final Long PARAM_HC_ROL_ESP_TECNICO = 2L;

    public static final Long PARAM_HC_ROL_ESP_LEGAL = 5L;

    public static final Long PARAM_HC_ROL_ALL = 0L;

    /**
     * Parámetros de estado de la línea base del programa
     */
    public static final Long PARAM_LB_CREADA = 343L;

    public static final Long PARAM_LB_APROBADA = 344L;

    public static final Long PARAM_LB_CERRADA = 345L;

    public static final Long PARAM_LB_SUSTITUIDA = 355L;

    public static final Long PARAM_LB_ANULADA = 356L;

    /**
     * Parámetros de integración SISSEG.
     */
    public static final String PARAM_SISSEG_VALIDAR_ACCESO = "rest/validarAcceso/";
    public static final String PARAM_SISSEG_PERMISOS_PAGINA = "rest/permisos/{idUsuario}/{idAplicacion}";
    public static final String PARAM_SISSEG_MODULOS = "rest/modulos/{idUsuario}/{idAplicacion}";
    public static final String PARAM_SISSEG_KEY_APLICACION = "rest/obtenerkeyaplicacion/{idAplicacion}";
    public static final String PARAM_SISSEG_ID_USUARIO = "rest/obteneridusuario/{username}";
    public static final String PARAM_SISSEG_VERIFICAR_ACCESO = "rest/verificaracceso/{idUsuario}/{idAplicacion}/{idPagina}";
    public static final String PARAM_SISSEG_DATOS_USUARIO = "rest/usuario/{idUsuario}";

    /**
     * Fases del proceso de Supervisión.
     */
    public static final Long PARAM_SUPERVISION_NINGUNA = 0L;
    public static final Long PARAM_EVENTO_DOCUMENTOS_VARIOS = 1L;
    public static final Long PARAM_AUTORIZACION_DOCUMENTOS_VARIOS = 7L;
    public static final Long PARAM_SUPERVISION_PRE_SUPERVISION = 2L;
    public static final Long PARAM_SUPERVISION_SUPERVISION_CAMPO = 3L;
    public static final Long PARAM_SUPERVISION_POST_SUPERVISION_CAMPO = 4L;
    public static final Long PARAM_SUPERVISION_REV_INFO_SUPERVISION = 5L;
    public static final Long PARAM_SUPERVISION_APROB_RESULTADOS = 6L;

    /**
     * Parámetros de integración PIDO.
     */
    public static final String PARAM_PIDO_SUNAT_CONSULTAR_RUC = "consultarucdatosprincipales/contribuyentes/consultar/1.1";
    public static final String PARAM_PIDO_SUNAT_CONSULTAR_DNI = "registroidentificacion/ciudadano/obtener/1.1";
    public static final String PARAM_PIDO_LOG_DE_USO_INI = "PIDO: Inicia llamada al servicio %s";
    public static final String PARAM_PIDO_LOG_DE_USO_FIN = "PIDO: Finaliza llamada al servicio %s";
    public static final String PARAM_PIDO_LOG_DE_USO_FIN_ERROR = "PIDO: Finaliza llamada al servicio con error %s";
    public static final String PARAM_PIDO_TAG_RAIZ = "soap:Envelope";
    public static final String PARAM_PIDO_TAG_ITEM_RESULTADO = "ns2:";
    public static final String PARAM_PIDO_CORESULTADO_OK = "0000";

    public static final Long PROCESO_ROL_NINGUNO = 0L;
    public static final Long PROCESO_ROL_SUPERVISOR = 4L;
    public static final Long PROCESO_ROL_ESP_TECNICO = 2L;
    public static final Long PROCESO_ROL_ESP_LEGAL = 5L;

    /**
     * Plantillas.
     */
    public static final String PARAM_PLANTILLA_TDR = "PLANTILLA_TDR.docx";
    public static final String PARAM_nombre_plantillaid_TDR = "PLANTILLA_TDR.docx";
    public static final String PARAM_PLANTILLA_ACTA_SUPERVISION = "PLANTILLA_ACTA_SUPERVISION.docx";
    public static final String PARAM_PLANTILLA_ACTA_INICIO_SUPERVISION = "PLANTILLA_ACTA_INICIO_SUPERVISION.docx";
    public static final String PARAM_PLANTILLA_FICHA_HECHOS_CONSTATADOS = "PLANTILLA_FICHA_HECHOS_CONSTATADOS.docx";
    public static final String PARAM_PLANTILLA_FICHA_HECHOS_CONSTATADOS_ESPTL = "PLANTILLA_FICHA_HECHOS_CONSTATADOS_ESPTL.docx";
    public static final String PARAM_PLANTILLA_RESUMEN_INST_PRELIMINAR = "PLANTILLA_RESUMEN_INST_PRELIMINAR.docx";
    public static final String PLANTILLA_FICHA_OBS_INF_SUPERVISION = "PLANTILLA_FICHA_OBS_INF_SUPERVISION.docx";
    public static final String PLANTILLA_FICHA_CONF_INF_SUPERVISION = "PLANTILLA_FICHA_CONF_INF_SUPERVISION.docx";
    public static final String PLANTILLA_FORMATO_LIQUIDACION_ACTAS = "PLANTILLA_FORMATO_LIQUIDACION_ACTAS.docx";
    public static final String PLANTILLA_FORMATO_LIQUIDACION_INFORME = "PLANTILLA_FORMATO_LIQUIDACION_INFORME.docx";
    public static final String PLANTILLA_FORMATO_LIQUIDACION_PENALIDAD = "PLANTILLA_FORMATO_LIQUIDACION_PENALIDAD.docx";
    public static final String PLANTILLA_FORMATO_MATRIZ_SUPERVISION = "PLANTILLA_FORMATO_MATRIZ_SUPERVISION.docx";
    public static final String PLANTILLA_REQUERIMIENTO_DOCUMENTACION = "PLANTILLA_REQUERIMIENTO_DOCUMENTACION.docx";
    public static final String PLANTILLA_RECEPCION_DOCUMENTACION = "PLANTILLA_RECEPCION_DOCUMENTACION.docx";
    public static final String PLANTILLA_FORMATO_RIESGOS = "PLANTILLA_FORMATO_RIESGOS.docx";
    public static final String PLANTILLA_REPORTE_HORIZONTAL = "PLANTILLA_REPORTE_HORIZONTAL.docx";
    public static final String PLANTILLA_FICHA_INFORMATIVA_UM = "PLANTILLA_FICHA_INFORMATIVA_UM.docx";
    public static final String PLANTILLA_FICHA_COMPONENTES_UM = "PLANTILLA_FICHA_COMPONENTES_UM.docx";
    public static final String PLANTILLA_DJI = "PLANTILLA_DJI.docx";
    public static final String PLANTILLA_CREDENCIAL = "PLANTILLA_CREDENCIAL.docx";
    public static final String PLANTILLA_PEN_PER_CONTRATO_SUPERVISORA = "PLANTILLA_PENALIDAD_PERIODO_CONTRATO_SUPERVISORA.docx";
    public static final String PLANTILLA_EJECUCION_PRESUPUESTAL = "PLANTILLA_EJECUCION_PRESUPUESTAL.docx";
    public static final String PLANTILLA_CON_SAL_CONTRATO_SUPERVISORA = "PLANTILLA_CONTROL_SALDO_CONTRATO_SUPERVISORA.docx";
    public static final String PLANTILLA_PRESUPUESTO_GASTO_SUPERVISION = "PLANTILLA_PRESUPUESTO_GASTO_SUPERVISION.docx";
    public static final String PARAM_PLANTILLA_CADENA_VACIA = "''";
    public static final String PARAM_TIPO_HC_SUP = "SUP";
    public static final String PARAM_TIPO_HC_ESP = "ESP";

    public static final Integer PLAZO_DIAS = 3;

    public static final Long PARAM_FR_FICHA_OBSERVACIONES = 335L;
    public static final Long PARAM_TIPO_REVISION = 337L;
    public static final Long PARAM_TIPO_NUEVA = 336L;

    public static final Long PARAM_DOI_INTERNO = 285L;
    public static final Long PARAM_SC_FICHA_OBSERVACION_INFORME_SUPERVISION = 33L;
    public static final Long PARAM_SC_FICHA_APROBACION_INFORME_SUPERVISION = 34L;
    public static final Long PARAM_TRD_OBSERVACION = 338L;
    public static final Long PARAM_TRD_APROBACION = 339L;

    public static final Long PARAM_SC_FICHA_JUSTIFICACION = 60L;

    public static final Long PARAM_SC_ACTA_SUPERVISION_FINAL = 27L;

    /**
     * Validación de generación de documentos.
     */
    public static final String MSG_ARCHIVO_FIRMADO = "Se ha detectado un archivo firmado digitalmente, para mantener la consistencia de la información, revierta la firma antes de generar el documento.";

    public static final String MSG_ARCHIVOS_FIRMADOS = "Se han detectado archivos firmados digitalmente, para mantener la consistencia de la información, revierta las firmas antes de generar el documento.";

    /**
     * Parámetro que sostiene el valor del estado de la línea base del programa
     */
    public static final Long PARAM_LINEA_PROGRAMA_ESTADO_APROBADA = 344L;

    public static final String PARAM_TIPO_ENTREGABLE = "TIPO_ENTREGABLE";

    public static final Long PARAM_TIPO_ENTREGABLE_ACTAS = 346L;

    public static final Long PARAM_TIPO_ENTREGABLE_INFORMES = 347L;

    public static final Long PARAM_TIPO_ENTREGABLE_INFORMESGESTION = 348L;

    public static final Long PARAM_TIPO_ENTREGABLE_INFORMES_FALLIDA = 359L;

    public static final Long PARAM_TIPO_ESTADIO_CONSUMO_PRECOMPROMETIDO = 349L;

    public static final Long PARAM_TIPO_ESTADIO_CONSUMO_COMPROMETIDO = 350L;

    public static final Long PARAM_TIPO_ESTADIO_CONSUMO_PORLIQUIDAR = 351L;

    public static final Long PARAM_TIPO_ESTADIO_CONSUMO_LIQUIDADO = 352L;

    public static final Long PARAM_TIPO_ESTADIO_CONSUMO_FACTURADO = 353L;

    public static final Long PARAM_TIPO_ESTADIO_CONSUMO_PENALIZADO = 354L;

    public static final Long PARAM_USUARIO_SIGED_CONTABILIDAD = 360L;

    /**
     * Parámetro de integración SOAP- SIGED
     * 
     */
    public static final String PARAM_SIGED_SOAP_COD_SISTEMA = "PGIM";
    public static final String PARAM_MODULO_GENERICO = "PGIM-Siged: General de Documentos";
    /*
     * Servicio listar documento expediente. endpoint del servicio
     */
    public static final String PARAM_L_DOCUMENTOS_URL = "SRV_AP403-4_EXPEDIENTE/expediente/listarDocumentos/v1.1";
    public static final String PARAM_L_DOCUMENTOS_ACCION = "expediente/documentos";
    public static final String PARAM_L_DOCUMENTOS_TIPO_ACCION = "L";
    public static final String PARAM_L_DOCUMENTOS_ACCION_SERVICIO = "expediente/documentos";
    public static final String PARAM_TIPO_ACCION = "L";
    // public static final String PARAM_ID_ROL = "5";

    public static final String PARAM_L_TRAZABILIDAD_URL = "SRV_AP403-8_EXPEDIENTE/expediente/listarTrazabilidadDocumentos/v1.1";
    public static final String PARAM_L_TRAZABILIDAD_ACCION_SERVICIO = "trazabilidad/list";

    public static final String PARAM_ID_ROL_USUARIO_FINAL = "5";
    public static final String PARAM_ID_ROL_SUPERVISOR_CONCESIONARIA = "89";

    public static final String PARAM_MSG_NO_L_DOCUMENTOS = "De acuerdo con la configuración de usuarios en el Siged, usted no tiene acceso al listado de documentos del expediente: %s";

    public static final String PARAM_CREAR_EXP_URL = "SRV_AP403-3_EXPEDIENTE/expediente/crearExpediente/v1.1";

    public static final String PARAM_L_ARCHIVOS_URL = "SRV_AP403-5_EXPEDIENTE/expediente/listarArchivos/v1.1";
    public static final String PARAM_L_ARCHIVOS_ACCION_SERVICIO = "archivo/list";

    public static final String PARAM_AGREGAR_DOC_URL = "SRV_AP403-2_EXPEDIENTE/expediente/agregarDocumento/v1.1";
    public static final String PARAM_AGREGAR_DOC_ACCION_SERVICIO = "expediente/agregarDocumento";
    public static final String PARAM_AGREGAR_DOC_TIPO_ACCION = "A";

    public static final String PARAM_AGREGAR_ARCHIVO_URL = "SRV_AP403-6_EXPEDIENTE/expediente/agregarArchivoADocumento/v1.1";
    public static final String PARAM_AGREGAR_ARCHIVO_ACCION_SERVICIO = "expediente/documentos";
    public static final String PARAM_AGREGAR_ARCHIVO_TIPO_ACCION = "L";

    public static final String PARAM_DESCARGA_ARCHIVO_URL = "SRV_AP403-1_EXPEDIENTE/expediente/descargarArchivo/v1.1";
    public static final String PARAM_DESCARGA_ARCHIVO_ACCION_SERVICIO = "archivo/descarga";
    public static final String PARAM_DESCARGA_ARCHIVO_TIPO_ACCION = "L";

    public static final String PARAM_ANULAR_ARCHIVO_URL = "SRV_AP403-11_EXPEDIENTE/expediente/anularArchivo/v1.1";
    public static final String PARAM_ANULAR_ARCHIVO_ACCION_SERVICIO = "nuevodocumento/anularArchivo";
    public static final String PARAM_ANULAR_ARCHIVO_TIPO_ACCION = "A";

    public static final String PARAM_ANULAR_DOCUMENTO_URL = "SRV_AP403-12_EXPEDIENTE/expediente/anularDocumento/v1.1";
    public static final String PARAM_ANULAR_DOCUMENTO_ACCION_SERVICIO = "documento/anular";
    public static final String PARAM_ANULAR_DOCUMENTO_TIPO_ACCION = "A";

    public static final String PARAM_OBTENER_USUARIO_URL = "SRV_AP403-13_EXPEDIENTE/expediente/obtenerUsuario/v1.1";

    public static final String PARAM_ACCESO_EXPEDIENTE_URL = "SRV_AP403-16_EXPEDIENTE/expediente/accesoExpediente/v1.1";

    public static final String PARAM_REENVIAR_EXPEDIENTE_URL = "SRV_AP403-18_EXPEDIENTE/expediente/reenviarExpediente/v1.1";

    public static final String PARAM_DEVOLVER_EXPEDIENTE_URL = "SRV_AP403-23_EXPEDIENTE/expediente/devolverExpediente/v1.1";

    public static final String PARAM_APROBAR_EXPEDIENTE_URL = "SRV_AP403-22_EXPEDIENTE/expediente/aprobarExpediente/v1.1";

    public static final String PARAM_ENUMERAR_DOCUMENTO_URL = "SRV_AP403-19_EXPEDIENTE/expediente/enumeracionDocumetos/v1.1";

    public static final String PARAM_ES_FERIADO_URL = "SRV_AP403-21_EXPEDIENTE/expediente/esFeriado/v1.1";

    public static final String PARAM_REVERTIR_FIRMA_URL = "SRV_AP403-24_EXPEDIENTE/expediente/revertirFirma/v1.1";

    /**
     * Servicio Buscar Expediente OSB
     */
    public static final String PARAM_BUSCAR_EXPEDIENTE_URL = "SRV_AP403-7_EXPEDIENTE/expediente/buscarExpediente/v1.1";
    public static final String PARAM_BUSCAR_EXPEDIENTE_ACCION_SERVICIO = "expediente/find";
    public static final String PARAM_BUSCAR_EXPEDIENTE_TIPO_ACCION_SERVICIO = "L";
    public static final String PARAM_BUSCAR_EXPEDIENTE_MODULO = "AP403-07";

    public static final Long PARAM_ESPECIALIDAD_VENTILACION = 5L;

    /**
     * Integración Siges para flujos de aprobación
     */
    public static final Long PARAM_REENVIAR = 367L;
    public static final Long PARAM_APROBAR = 368L;
    public static final Long PARAM_DEVOLVER = 369L;
    public static final Long PARAM_EXCEPCION = 370L;

    public static final Long PARAM_RP_REVISAR_APROBAR_INF_SUPERVISION_CONFIRMAR_HECHOS_CONSTATADOS = 27L;

    public static final Long PARAM_RP_REVISAR_APROBAR_INF_SUPERVISION_MEMOOFICIO_CONFORMIDAD = 726L;

    public static final Long PARAM_RP_REVISAR_APROBAR_INF_SUPERVISION_ELABORAR_PRESENTAR_INF_SUPERVISION = 37L;

    public static final String IND_REQ_APROBACION_SI = "1";
    public static final String IND_REQ_APROBACION_NO = "0";

    public static final Long PARAM_SC_DOCX = 377L;

    public static final Long PARAM_SC_PDF = 378L;

    /**
     * Estados de la configuración
     */
    public static final Long PARAM_ESTADO_CONFIG_RIESGO_CREADA = 373L;
    public static final Long PARAM_ESTADO_CONFIG_RIESGO_OBSERVADA = 374L;
    public static final Long PARAM_ESTADO_CONFIG_RIESGO_APROBADA = 375L;
    public static final Long PARAM_ESTADO_CONFIG_RIESGO_CANCELADA = 376L;

    public static final Long PARAM_SC_ACTA_SUPERVISION = 27L;
    public static final Long PARAM_SC_ACTA_INICIO_SUPERVISION = 9L;
    public static final Long PARAM_SC_ACTA_REQUERIMIENTO_DOCUMENTACION = 10L;
    public static final Long PARAM_SC_ACTA_RECEPCION_DOCUMENTACION = 11L;
    public static final Long PARAM_SC_MATRIZ_SUPERVISION = 26L;

    public static final Long PARAM_SC_FICHA_RIESGOS_PARAM_GESTION = 158L;
    public static final Long PARAM_SC_FICHA_RIESGOS_PARAM_TECNICOS = 157L;

    public static final Long PARAM_GRUPO_RIESGO_TECNICO = 1L;
    public static final Long PARAM_GRUPO_RIESGO_GESTION = 2L;

    public static final String PARAM_ESTADO_ACTIVO_CLIENTE_SIGED = "65";

    /**
     * Estados de registro AGOL / SURVEY
     */
    public static final String PARAM_SURVEY_ESTADO_SUPERV_INICIADA = "1";
    public static final String PARAM_SURVEY_ESTADO_SUPERV_FINALIZADA = "0";

    // public static final String PARAM_SURVEY_REGISTRO_EXITOSO = "true";
    // public static final String PARAM_SURVEY_REGISTRO_ERROR = "false";

    public static final Long PARAM_TIPO_NORMA = 31L;
    public static final Long PARAM_DIVISION_ITEM = 32L;
    public static final Long PARAM_MEDIDA_ADMINISTRATIVA = 49L;
    public static final Long PARAM_TIPO_OBJETO = 50L;
    public static final String PARAM_MEDIO_DENUNCIA = "MEDIO_DENUNCIA";

    public static final Long PARAM_TIPO_MATERIA = 53L;
    public static final Long PARAM_TIPO_REPORTE = 52L;
    public static final String PARAM_MATERIA = "TIPO_MATERIA";
    public static final String PARAM_REPORTE = "TIPO_REPORTE";

    /**
     * Parámetro nombre de tipo norma "TIPO_NORMA" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_NOMBRE_TIPO_NORMA = "TIPO_NORMA";

    /**
     * Parámetro nombre de tipo de division ítem "TIPO_DIVISION_ITEM" registrado en
     * la tabla PGIM_TP_PARAMETRO
     */
    public static final String PARAM_NOMBRE_TIPO_DIVISION_ITEM = "TIPO_DIVISION_ITEM";

    /**
     * Parámetro de nombre "IND_CONSISTENCIA_ALEATORIA" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_INDICE_CONSISTENCIA_ALEATORIA = "IND_CONSISTENCIA_ALEATORIA";

    /**
     * Parámetro de nombre "IND_RATIO_CONSISTENCIA_ACEPTABLE" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_INDICE_RATIO_CONSISTENCIA_ACEPTABLE = "IND_RATIO_CONSISTENCIA_ACEPTABLE";

    /**
     * Parámetro de nombre "TIPO_ORIGEN_DATO_RIESGO" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_ORIGEN_DATO_RIESGO = "TIPO_ORIGEN_DATO_RIESGO";

    /**
     * Parámetro de nombre "TIPO_MEDIDA_ADMINISTRATIVA" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_MEDIDA_ADMINISTRATIVA = "TIPO_MEDIDA_ADMINISTRATIVA";

    /**
     * Parámetro de nombre "TIPO_OBJ_RELACIONADO_MED_ADM" registrado en la tabla
     * PGIM_TP_PARAMETRO
     */
    public static final String PARAM_TIPO_OBJ_RELACIONADO_MED_ADM = "TIPO_OBJ_RELACIONADO_MED_ADM";

    // SNE:
    public static final String PARAM_SNE_ACTIVO = "A";

    public static final String PARAM_SNE_INACTIVO = "I";

    public static final String PARAM_SNE_PENDIENTE_ACTIVACION = "P";

    public static final String PARAM_SNE_NO_ESTA_AFILIADO = "N";

    public static final Long PARAM_RELACION_ASIGNARCONF_REGISTRARCONF = 718L;

    public static final Long PARAM_RELACION_REGISTRARCONF_CANCELARCONF = 719L;

    public static final Long PARAM_RELACION_CANCELARCONF_CONFCANCELADA = 720L;

    public static final Long PARAM_RELACION_REGISTRARCONF_APROBARCONF = 721L;

    public static final Long PARAM_RELACION_APROBARCONF_CONFAPROBADA = 722L;

    public static final Long PARAM_RELACION_APROBARCONF_REGISTRARCONF = 725L;

    public static final Long PARAM_TIPO_ESTADO_CONF_CREADA = 373L;

    public static final Long PARAM_TIPO_ESTADO_CONF_OBSERVADA = 374L;

    public static final Long PARAM_TIPO_ESTADO_CONF_APROBADA = 375L;

    public static final Long PARAM_TIPO_ESTADO_CONF_CANCELADA = 376L;

    public static final Long PARAM_TIPO_ESTADO_CONF_PARA_APROBAR = 408L;

    public static final Long PARAM_TIPO_ESTADO_CONF_PARA_CANCELAR = 409L;

    public static final String PARAM_EXTENSION_PDF = "PDF";

    public static final String PARAM_EXTENSION_EXCEL = "XLSX";

    /**
     * imagen del logo de osinergmin en base64 para hacer usado en los reportes en
     * formato excel
     */
    public static final String PARAM_LOGO_OSI = "LOGO_OSI.png";
}
