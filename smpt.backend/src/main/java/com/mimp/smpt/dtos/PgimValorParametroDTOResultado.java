package com.mimp.smpt.dtos;

import java.math.BigDecimal;

public class PgimValorParametroDTOResultado extends PgimValorParametroDTO {

    /**
     * @see com.mimp.smpt.models.repository.ValorParametroRepository#obtenerValorParametroPorID()
     */
    public PgimValorParametroDTOResultado(Long idValorParametro, Long coClave, String noValorParametro,
            String deValorParametro, BigDecimal nuOrden, BigDecimal nuValorNumerico) {
        super();
        this.setIdValorParametro(idValorParametro);
        this.setCoClave(coClave);
        this.setNoValorParametro(noValorParametro);
        this.setDeValorParametro(deValorParametro);
        this.setNuOrden(nuOrden);

        this.setNuValorNumerico(nuValorNumerico);

    }

    /**
     * De prueba
     * 
     * @param idValorParametro
     * @param noValorParametro
     */
    public PgimValorParametroDTOResultado(Long idValorParametro, String noValorParametro) {
        super();
        this.setIdValorParametro(idValorParametro);
        this.setNoValorParametro(noValorParametro);
    }

    /**
     * @see pe.gob.osinergmin.pgim.models.repository.ValorParametroRepository#filtrarPorNombreParametro()
     * @param idValorParametro
     * @param coClave
     * @param noValorParametro
     * @param deValorParametro
     * @param nuOrden
     * @param nuValorNumerico
     * @param deValorAlfanum
     */
    public PgimValorParametroDTOResultado(Long idValorParametro, Long coClave, String noValorParametro,
            String deValorParametro, BigDecimal nuOrden, BigDecimal nuValorNumerico, String deValorAlfanum) {
        super();
        this.setIdValorParametro(idValorParametro);
        this.setCoClave(coClave);
        this.setNoValorParametro(noValorParametro);
        this.setDeValorParametro(deValorParametro);
        this.setNuOrden(nuOrden);
        this.setNuValorNumerico(nuValorNumerico);
        this.setDeValorAlfanum(deValorAlfanum);
    }
}
