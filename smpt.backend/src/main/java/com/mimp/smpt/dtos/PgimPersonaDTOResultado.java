package com.mimp.smpt.dtos;

import java.util.Date;

public class PgimPersonaDTOResultado extends PgimPersonaDTO {
    /**
     * Permite potar los datos de una persona natural.
     * 
     * @see com.mimp.smpt.models.repository.PersonaRepository#listarPorCoDocumentoIdentidad()
     * 
     * @param idPersona
     * @param coDocumentoIdentidad
     * @param noRazonSocial
     */
    public PgimPersonaDTOResultado(Long idPersona, String coDocumentoIdentidad, String noRazonSocial) {
        super();

        this.setIdPersona(idPersona);
        this.setCoDocumentoIdentidad(coDocumentoIdentidad);
        this.setNoRazonSocial(noRazonSocial);
    }

    /**
     * Permite potar los datos de una persona natural.
     * 
     * @see com.mimp.smpt.models.repository.PersonaRepository#obtenerPersonaPorId()
     * 
     * @param idPersona
     * @param coDocumentoIdentidad
     * @param noPersona
     * @param apPaterno
     * @param apMaterno
     */
    public PgimPersonaDTOResultado(Long idPersona, String coDocumentoIdentidad, String noPersona, String apPaterno,
            String apMaterno) {
        super();

        this.setIdPersona(idPersona);
        this.setCoDocumentoIdentidad(coDocumentoIdentidad);
        this.setNoPersona(noPersona);
        this.setApPaterno(apPaterno);
        this.setApMaterno(apMaterno);
    }

    /**
     * Permite potar los datos de una persona natural.
     * 
     * @see com.mimp.smpt.models.repository.PersonaRepository#listarPorPersona()
     * @see com.mimp.smpt.models.repository.PersonaRepository#listarResponsablesXinstanciaProc()
     * 
     * @param idPersona
     * @param coDocumentoIdentidad
     * @param noPersona
     * @param apPaterno
     * @param apMaterno
     */
    public PgimPersonaDTOResultado(Long idPersona, String coDocumentoIdentidad, String noPersona,
            String noRazonSocial) {
        super();

        this.setIdPersona(idPersona);
        this.setCoDocumentoIdentidad(coDocumentoIdentidad);
        this.setNoPersona(noPersona);
        this.setNoRazonSocial(noRazonSocial);
    }

    /**
     * Permite potar los datos de una persona natural o jurdica.
     * 
     * @see com.mimp.smpt.models.repository.PersonaRepository#listarPersonas()
     * 
     * @param idPersona
     * @param noRazonSocial
     * @param idTipoDocIdentidad
     * @param descIdTipoDocIdentidad
     * @param coDocumentoIdentidad
     * @param tiSexo
     */
    public PgimPersonaDTOResultado(Long idPersona, String noRazonSocial, Long idTipoDocIdentidad,
            String descIdTipoDocIdentidad, String coDocumentoIdentidad, String tiSexo, String noPersona,
            String apPaterno, String apMaterno) {
        super();

        this.setIdPersona(idPersona);
        this.setNoRazonSocial(noRazonSocial);
        this.setIdTipoDocIdentidad(idTipoDocIdentidad);
        this.setDescIdTipoDocIdentidad(descIdTipoDocIdentidad);
        this.setCoDocumentoIdentidad(coDocumentoIdentidad);
        this.setTiSexo(tiSexo);
        this.setNoPersona(noPersona);
        this.setApPaterno(apPaterno);
        this.setApMaterno(apMaterno);
    }

    /**
     * Permite potar los datos de una persona natural o jurdica.
     * 
     * @see com.mimp.smpt.models.repository.PersonaRepository#obtenerPersonalNatuJuriPorId()
     * 
     * @param idPersona
     * @param idTipoDocIdentidad
     * @param descIdTipoDocIdentidad
     * @param coDocumentoIdentidad
     * @param noRazonSocial
     * @param noCorto
     * @param noPersona
     * @param apPaterno
     * @param apMaterno
     * @param tiSexo
     * @param feNacimiento
     * @param deTelefono
     * @param deTelefono2
     * @param deCorreo
     * @param deCorreo2
     * @param idUbigeo
     * @param descIdUbigeo
     * @param descUbigeo
     * @param diPersona
     * @param flAfiliadoNtfccionElctrnca
     * @param deCorreoNtfccionElctrnca
     * @param feAfiliadoDesde
     * @param cmNota
     */
    public PgimPersonaDTOResultado(Long idPersona, Long idTipoDocIdentidad, String descIdTipoDocIdentidad,
            String coDocumentoIdentidad, String noRazonSocial, String noCorto, String noPersona, String apPaterno,
            String apMaterno, String tiSexo, Date feNacimiento, String deTelefono, String deTelefono2, String deCorreo,
            String deCorreo2, Long idUbigeo, String descUbigeo, String diPersona, String flAfiliadoNtfccionElctrnca,
            String deCorreoNtfccionElctrnca, Date feAfiliadoDesde, String cmNota) {
        super();
        // Identificación
        this.setIdPersona(idPersona); // N y J
        this.setIdTipoDocIdentidad(idTipoDocIdentidad); // N y J
        this.setDescIdTipoDocIdentidad(descIdTipoDocIdentidad); // N y J
        this.setCoDocumentoIdentidad(coDocumentoIdentidad); // N y J
        this.setNoRazonSocial(noRazonSocial); // J
        this.setNoCorto(noCorto);
        this.setNoPersona(noPersona); // N
        this.setApPaterno(apPaterno);// N
        this.setApMaterno(apMaterno);// N
        this.setTiSexo(tiSexo);// N
        this.setFeNacimiento(feNacimiento);
        // Contacto
        this.setDeTelefono(deTelefono);
        this.setDeTelefono2(deTelefono2);
        this.setDeCorreo(deCorreo);
        this.setDeCorreo2(deCorreo2);
        // Ubicación
        this.setIdUbigeo(idUbigeo);
        // this.setDescIdUbigeo(descIdUbigeo);
        this.setDescUbigeo(descUbigeo);
        this.setDiPersona(diPersona);
        // Notificaciones electrónicas
        this.setFlAfiliadoNtfccionElctrnca(flAfiliadoNtfccionElctrnca);
        this.setDeCorreoNtfccionElctrnca(deCorreoNtfccionElctrnca);
        this.setFeAfiliadoDesde(feAfiliadoDesde);
        // Otros
        this.setCmNota(cmNota);
    }

    /**
     * Permite potar los datos de una persona natural o jurdica.
     * 
     * @see com.mimp.smpt.models.repository.PersonaRepository#existePersona()
     * 
     */
    public PgimPersonaDTOResultado(Long idPersona, Long idTipoDocIdentidad, String descIdTipoDocIdentidad,
            String coDocumentoIdentidad) {
        super();
        this.setIdPersona(idPersona); // N y J
        this.setIdTipoDocIdentidad(idTipoDocIdentidad); // N y J
        this.setDescIdTipoDocIdentidad(descIdTipoDocIdentidad);
        this.setCoDocumentoIdentidad(coDocumentoIdentidad); // N y J
    }
}
