package com.mimp.smpt.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.mimp.smpt.dtos.PgimUbigeoDTO;
import com.mimp.smpt.services.UbigeoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * Controlador del componente componente para los ubigeos, este componente
 * contiene los metodos necesarios para el crud y validaciones respectivas.
 * contiene los siguientes metodos: listar listarPageable filtrar obtener
 * registrar modificar eliminar listarPorPalabraClave
 * listarPorPalabraClaveAlternativo
 * 
 * @descripción: Ubigeo
 *
 * @author: hruiz
 * @version: 1.0
 * @fecha_de_creación: 17/07/2020
 *
 */
@RestController
@Slf4j
@RequestMapping("/ubigeos")
public class UbigeoController {

	@Autowired
	private UbigeoService ubigeoService;

	@GetMapping("/filtrar/palabraClave/{palabra}")
	public ResponseEntity<?> listarPorPalabraClave(@PathVariable String palabra) {

		Map<String, Object> respuesta = new HashMap<>();
		List<PgimUbigeoDTO> lPgimUbigeoDTO = null;

		if (palabra == "_vacio_") {
			lPgimUbigeoDTO = new ArrayList<PgimUbigeoDTO>();
			respuesta.put("mensaje", "No se encontraron ubigeos");
			respuesta.put("PgimUbigeoDTO", lPgimUbigeoDTO);

			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
		}

		try {
			lPgimUbigeoDTO = this.ubigeoService.listarPorPalabraClave(palabra);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Ocurrió un error al realizar la consulta de las unidades mineras");
			respuesta.put("error", e.getMostSpecificCause().getMessage());

			log.error(e.getMostSpecificCause().getMessage());

			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "Se encontraron los ubigeos");
		respuesta.put("lPgimUbigeoDTO", lPgimUbigeoDTO);

		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
	}

	@GetMapping("/filtrar/palabraClaveAlternativo/{palabra}")
	public ResponseEntity<?> listarPorPalabraClaveAlternativo(@PathVariable String palabra) {

		Map<String, Object> respuesta = new HashMap<>();
		List<PgimUbigeoDTO> lPgimUbigeoDTO = null;

		if (palabra == "_vacio_") {
			lPgimUbigeoDTO = new ArrayList<PgimUbigeoDTO>();
			respuesta.put("mensaje", "No se encontraron ubigeos");
			respuesta.put("PgimUbigeoDTO", lPgimUbigeoDTO);

			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
		}

		try {
			lPgimUbigeoDTO = this.ubigeoService.listarPorPalabraClaveAlternativo(palabra);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Ocurrió un error al realizar la consulta de las unidades mineras");
			respuesta.put("error", e.getMostSpecificCause().getMessage());

			log.error(e.getMostSpecificCause().getMessage());

			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "Se encontraron los ubigeos");
		respuesta.put("lPgimUbigeoDTO", lPgimUbigeoDTO);

		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
	}

}
