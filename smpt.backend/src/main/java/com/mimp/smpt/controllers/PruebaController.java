package com.mimp.smpt.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/prueba")
public class PruebaController {

    @GetMapping
    public ResponseEntity hello() {
        return ResponseEntity.ok("Hello World desde el API REST");
    }
}
