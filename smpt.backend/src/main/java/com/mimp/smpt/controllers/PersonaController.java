package com.mimp.smpt.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.mimp.smpt.dtos.PgimPersonaDTO;
import com.mimp.smpt.dtos.PgimValorParametroDTO;
import com.mimp.smpt.dtos.ResponseDTO;
import com.mimp.smpt.models.entity.PgimPersona;
import com.mimp.smpt.services.ParametroService;
import com.mimp.smpt.services.PersonaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * Controlador para la gestión de las funcionalidades relacionadas con la
 * Persona natural y juridica
 * 
 * @descripción: Persona
 *
 * @author: jvalerio
 * @version: 1.0
 * @fecha_de_creación: 22/08/2020
 */
@RestController
@Slf4j
@RequestMapping("/personas")
public class PersonaController {

    @Autowired
    private ParametroService parametroService;

    @Autowired
    private PersonaService personaService;

    @PostMapping("/listarPersonas")
    public ResponseEntity<Page<PgimPersonaDTO>> listarPersonas(@RequestBody PgimPersonaDTO filtroPersona,
            Pageable paginador) {
        Page<PgimPersonaDTO> lPgimPersonaDTO = this.personaService.listarPersonas(filtroPersona, paginador);
        return new ResponseEntity<Page<PgimPersonaDTO>>(lPgimPersonaDTO, HttpStatus.OK);
    }

    /**
     * Permite obtener las configuraciones necesarias para el listado de personas
     * naturales y juridicas. Acá se incluyen configuraciones como: Tipo de
     * documento de identidad ya sea RUC, DNI O CE.
     * 
     * @return
     */
    @GetMapping("/obtenerConfiguraciones")
    public ResponseEntity<?> obtenerConfiguraciones() {

        Map<String, Object> respuesta = new HashMap<>();

        List<PgimValorParametroDTO> lPgimValorParametroDTOTipoDocIdentidad = null;
        try {
            // lPgimValorParametroDTOTipoDocIdentidad = this.parametroService
            // .filtrarPorTipoDocIdentidad(ConstantesUtil.PARAM_TIPO_DOCUMENTO_IDENTIDAD);
        } catch (DataAccessException e) {
            respuesta.put("mensaje", "Ocurrió un error al realizar la consulta de estrato");
            respuesta.put("error", e.getMostSpecificCause().getMessage());
            log.error(e.getMostSpecificCause().getMessage());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        respuesta.put("mensaje", "Se obtuvieron todas las configuraciones");
        respuesta.put("lPgimValorParametroDTOTipoDocIdentidad", lPgimValorParametroDTOTipoDocIdentidad);
        return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
    }

    @GetMapping("/obtenerPersonalNatuJuriPorId/{idPersona}")
    public ResponseEntity<?> obtenerPersonalNatuJuriPorId(@PathVariable Long idPersona) {
        String mensaje;
        Map<String, Object> respuesta = new HashMap<>();

        PgimPersonaDTO pgimPersonaDTO = null;

        try {
            pgimPersonaDTO = this.personaService.obtenerPersonalNatuJuriPorId(idPersona);
        } catch (DataAccessException e) {
            respuesta.put("mensaje", "Ocurrió un error al intentar recuperar el agente supervisado");
            respuesta.put("error", e.getMostSpecificCause().getMessage());
            log.error(e.getMostSpecificCause().getMessage());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (pgimPersonaDTO == null) {
            mensaje = String.format("El agente supervisado con el id: %d no existe en la base de datos", idPersona);
            respuesta.put("mensaje", mensaje);

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NO_CONTENT);
        }

        respuesta.put("mensaje", "El agente supervisado ha sido recuperado");
        respuesta.put("pgimPersonaDTO", pgimPersonaDTO);

        return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
    }

    @PostMapping("/crearPersona")
    public ResponseEntity<?> crearPersona(@Valid @RequestBody PgimPersonaDTO pgimPersonaDTO,
            BindingResult resultadoValidacion) throws Exception {

        PgimPersonaDTO pgimPersonaDTOCreado = null;
        Map<String, Object> respuesta = new HashMap<>();

        if (resultadoValidacion.hasErrors()) {
            List<String> errores = null;

            errores = resultadoValidacion.getFieldErrors().stream()
                    .map(err -> String.format("La propiedad '%s' %s", err.getField(), err.getDefaultMessage()))
                    .collect(Collectors.toList());

            respuesta.put("mensaje", "Se han encontrado inconsistencias para crear una persona natural o juridica");
            respuesta.put("error", errores.toString());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.BAD_REQUEST);
        }

        try {
            pgimPersonaDTOCreado = this.personaService.crearPersona(pgimPersonaDTO);
        } catch (DataAccessException e) {
            respuesta.put("mensaje", "Ocurrió un error al intentar crear una persona natural o juridica");
            respuesta.put("error", e.getMostSpecificCause().getMessage());
            log.error(e.getMostSpecificCause().getMessage());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        respuesta.put("mensaje", "La persona natural o juridica ha sido creada");
        respuesta.put("pgimPersonaDTOCreado", pgimPersonaDTOCreado);

        return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
    }

    @GetMapping("/obtenerConfiguraciones/generales/{idPersona}")
    public ResponseEntity<?> obtenerConfiguracionesGenerales(@PathVariable Long idPersona) {

        Map<String, Object> respuesta = new HashMap<>();

        List<PgimValorParametroDTO> lPgimValorParametroDTOTipoDocIdentidad = null;
        List<PgimValorParametroDTO> lPgimValorParametroDTOTipoDocIdentidadRuc = null;
        List<PgimValorParametroDTO> lPgimValorParametroDTOTipoDocIdentidadDniCe = null;
        List<PgimValorParametroDTO> lPgimValorParametroDTOTipoDocIdentidadDni = null;
        List<PgimValorParametroDTO> lPgimValorParametroDTOTipoDocIdentidadCe = null;
        try {

            // lPgimValorParametroDTOTipoDocIdentidad = this.parametroService
            // .filtrarPorTipoDocIdentidad(ConstantesUtil.PARAM_TIPO_DOCUMENTO_IDENTIDAD);
            // lPgimValorParametroDTOTipoDocIdentidadRuc = this.parametroService
            // .filtrarPorTipoDocIdentidadRuc(ConstantesUtil.PARAM_TIPO_DOCUMENTO_IDENTIDAD_RUC);
            // lPgimValorParametroDTOTipoDocIdentidadDniCe = this.parametroService
            // .filtrarPorTipoDocIdentidadDniCe(ConstantesUtil.PARAM_TIPO_DOCUMENTO_IDENTIDAD_DNI_CE);
            // lPgimValorParametroDTOTipoDocIdentidadDni = this.parametroService
            // .filtrarPorTipoDocIdentidadDni(ConstantesUtil.PARAM_TIPO_DOCUMENTO_IDENTIDAD_DNI);
            // lPgimValorParametroDTOTipoDocIdentidadCe = this.parametroService
            // .filtrarPorTipoDocIdentidadCe(ConstantesUtil.PARAM_TIPO_DOCUMENTO_IDENTIDAD_CE);
        } catch (DataAccessException e) {
            respuesta.put("mensaje", "Ocurrió un error al realizar la consulta de los parámetros");
            respuesta.put("error", e.getMostSpecificCause().getMessage());
            log.error(e.getMostSpecificCause().getMessage());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        respuesta.put("mensaje", "Se obtuvieron todas las configuraciones");

        respuesta.put("lPgimValorParametroDTOTipoDocIdentidad", lPgimValorParametroDTOTipoDocIdentidad);
        respuesta.put("lPgimValorParametroDTOTipoDocIdentidadRuc", lPgimValorParametroDTOTipoDocIdentidadRuc);
        respuesta.put("lPgimValorParametroDTOTipoDocIdentidadDniCe", lPgimValorParametroDTOTipoDocIdentidadDniCe);
        respuesta.put("lPgimValorParametroDTOTipoDocIdentidadDni", lPgimValorParametroDTOTipoDocIdentidadDni);
        respuesta.put("lPgimValorParametroDTOTipoDocIdentidadCe", lPgimValorParametroDTOTipoDocIdentidadCe);

        return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
    }

    @PutMapping("/modificarPersona")
    public ResponseEntity<?> modificarPersona(@Valid @RequestBody PgimPersonaDTO pgimPersonaDTO,
            BindingResult resultadoValidacion) throws Exception {

        PgimPersona pgimPersonaActual = null;
        PgimPersonaDTO pgimPersonaDTOModificada = null;
        String mensaje;
        Map<String, Object> respuesta = new HashMap<>();

        if (resultadoValidacion.hasErrors()) {
            List<String> errores = null;

            errores = resultadoValidacion.getFieldErrors().stream()
                    .map(err -> String.format("La propiedad '%s' %s", err.getField(), err.getDefaultMessage()))
                    .collect(Collectors.toList());

            respuesta.put("mensaje", "Se han encontrado inconsistencias para modificar una persona natural o juridica");
            respuesta.put("error", errores.toString());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.BAD_REQUEST);
        }

        try {
            pgimPersonaActual = this.personaService.getByIdPersona(pgimPersonaDTO.getIdPersona());

            if (pgimPersonaActual == null) {
                mensaje = String.format(
                        "La persona natural o juridica %s que intenta actualizar no existe en la base de datos",
                        pgimPersonaDTO.getIdPersona());
                respuesta.put("mensaje", mensaje);

                return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
            }
        } catch (DataAccessException e) {
            respuesta.put("mensaje", "Ocurrió un error intentar recuperar la persona natural o juridica a actualizar");
            respuesta.put("error", e.getMostSpecificCause().getMessage());
            log.error(e.getMostSpecificCause().getMessage());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        try {
            pgimPersonaDTOModificada = this.personaService.modificarPersona(pgimPersonaDTO, pgimPersonaActual);
        } catch (DataAccessException e) {
            respuesta.put("mensaje", "Ocurrió un error al intentar modificar la persona natural o juridica");
            respuesta.put("error", e.getMostSpecificCause().getMessage());
            log.error(e.getMostSpecificCause().getMessage());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        respuesta.put("mensaje", "La persona natural o juridica ha sido modificada");
        respuesta.put("pgimPersonaDTOModificada", pgimPersonaDTOModificada);

        return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
    }

    @DeleteMapping("/eliminarPersona/{idPersona}")
    public ResponseEntity<ResponseDTO> delete(@PathVariable Long idPersona) throws Exception {
        ResponseDTO responseDTO = null;
        String mensaje;

        PgimPersona pgimPersActual = null;

        try {
            pgimPersActual = this.personaService.getByIdPersona(idPersona);

            if (pgimPersActual == null) {
                mensaje = String.format(
                        "La persona natural o juridica %s que intenta eliminar no existe en la base de datos",
                        idPersona);
                responseDTO = new ResponseDTO("mensaje", mensaje);

                return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
            }
        } catch (DataAccessException e) {
            log.error(e.getMostSpecificCause().getMessage());
            mensaje = String.format("Ocurrió un error intentar recuperar la persona natural o juridica a actualizar",
                    e.getMostSpecificCause().getMessage());

            responseDTO = new ResponseDTO("error", mensaje);
            log.error(e.getMostSpecificCause().getMessage());
            return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
        }

        try {
            this.personaService.eliminarPersona(pgimPersActual);
        } catch (DataAccessException e) {
            log.error(e.getMostSpecificCause().getMessage());
            mensaje = String.format("Ocurrió un error intentar eliminar una persona natural o juridica",
                    e.getMostSpecificCause().getMessage());
            log.error(e.getMostSpecificCause().getMessage());
            responseDTO = new ResponseDTO("error", mensaje);

            return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
        }

        responseDTO = new ResponseDTO("success", "La persona fue eliminada");

        return ResponseEntity.status(HttpStatus.CREATED).body(responseDTO);
    }

    @GetMapping("/existePersona/{idPersona}/{idTipoDocumento}/{numeroDocumento}")
    public ResponseEntity<?> existePersona(@PathVariable Long idPersona, @PathVariable Long idTipoDocumento,
            @PathVariable String numeroDocumento) {

        if (idPersona == 0) {
            idPersona = null;
        }

        Map<String, Object> respuesta = new HashMap<>();
        List<PgimPersonaDTO> lPgimPersonaDTO = null;

        try {
            lPgimPersonaDTO = this.personaService.existePersona(idPersona, idTipoDocumento, numeroDocumento);
        } catch (DataAccessException e) {
            respuesta.put("mensaje", "Ocurrió un error al realizar verificar si ya exite una persona accidentado");
            respuesta.put("error", e.getMostSpecificCause().getMessage());
            log.error(e.getMostSpecificCause().getMessage());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        respuesta.put("mensaje", "Si fue posible determinar la existencia de la persona accidentada");
        respuesta.put("lPgimPersonaDTO", lPgimPersonaDTO);

        return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
    }

    /**
     * Me permite listar por numero de documento de identidad de la persona juridica
     * o natural
     * 
     * @param palabra = coDocumentoIdentidad
     * @return
     */
    @GetMapping("/filtrar/coDocumentoIdentidad/{palabra}")
    public ResponseEntity<?> listarPorCoDocumentoIdentidad(@PathVariable String palabra) {

        Map<String, Object> respuesta = new HashMap<>();
        List<PgimPersonaDTO> lPgimPersonaDTOCoDocumentoIdentidad = null;

        if (palabra == "_vacio_") {
            lPgimPersonaDTOCoDocumentoIdentidad = new ArrayList<PgimPersonaDTO>();
            respuesta.put("mensaje", "No se encontraron número de documetno de identidad");
            respuesta.put("lPgimPersonaDTOCoDocumentoIdentidad", lPgimPersonaDTOCoDocumentoIdentidad);

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
        }

        try {
            lPgimPersonaDTOCoDocumentoIdentidad = this.personaService.listarPorCoDocumentoIdentidad(palabra);
        } catch (DataAccessException e) {
            respuesta.put("mensaje", "Ocurrió un error al realizar la consulta de número de documetno de identidad");
            respuesta.put("error", e.getMostSpecificCause().getMessage());
            log.error(e.getMostSpecificCause().getMessage());

            return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        respuesta.put("mensaje", "Se encontraron los números de documetno de identidad");
        respuesta.put("lPgimPersonaDTOCoDocumentoIdentidad", lPgimPersonaDTOCoDocumentoIdentidad);

        return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
    }
}
