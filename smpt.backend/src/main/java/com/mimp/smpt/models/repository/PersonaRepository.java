package com.mimp.smpt.models.repository;

import java.util.List;

import com.mimp.smpt.dtos.PgimPersonaDTO;
import com.mimp.smpt.models.entity.PgimPersona;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PersonaRepository extends JpaRepository<PgimPersona, Long> {

        /**
         * Permite obtener la persona en base a su identificador.
         * 
         * @param idPersona
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimPersonaDTOResultado("
                        + "pers.idPersona, pers.coDocumentoIdentidad, pers.noPersona, "
                        + "pers.apPaterno, pers.apMaterno) " + "FROM PgimPersona pers "
                        + "WHERE pers.idPersona = :idPersona " + "AND pers.esRegistro = '1'")
        PgimPersonaDTO obtenerPersonaPorId(@Param("idPersona") Long idPersona);

        /**
         * Permite obtener la persona jurídica por búsqueda con palabra clave.
         * 
         * @param idPersona
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimPersonaDTOResultado("
                        + "pers.idPersona, pers.coDocumentoIdentidad, pers.noRazonSocial) "
                        + "FROM PgimPersona pers WHERE pers.esRegistro = 1 "
                        + "AND (?1 IS NULL OR LOWER(pers.coDocumentoIdentidad) LIKE LOWER(CONCAT('%', ?1, '%'))  "
                        + "OR LOWER(pers.noRazonSocial) LIKE LOWER(CONCAT('%', ?1, '%')) "
                        + "OR LOWER(pers.noCorto) LIKE LOWER(CONCAT('%', ?1, '%')) )")
        List<PgimPersonaDTO> listarPersonaJuridicaPorPalabraClave(String palabraClave);

        /**
         * Permite obtener la persona jurídica por búsqueda con palabra clave.
         * 
         * @param idPersona
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimPersonaDTOResultado("
                        + "pers.idPersona, pers.coDocumentoIdentidad, pers.noPersona ||' '|| pers.apPaterno ||' '|| pers.apMaterno, pers.noRazonSocial) "
                        + "FROM PgimPersona pers WHERE pers.esRegistro = 1 "
                        + "AND (:palabraClave IS NULL OR LOWER(pers.coDocumentoIdentidad ||' - '|| pers.noPersona ||' '|| pers.apPaterno ||' '|| pers.apMaterno) "
                        + "LIKE LOWER(CONCAT('%', :palabraClave, '%'))  )")
        List<PgimPersonaDTO> listarPorPersona(String palabraClave);

        @Query("SELECT new com.mimp.smpt.dtos.PgimPersonaDTOResultado(" + "pers.idPersona, " + "pers.noRazonSocial, "
                        + "pers.tipoDocIdentidad.idValorParametro, pers.tipoDocIdentidad.noValorParametro, "
                        + "pers.coDocumentoIdentidad, " + "CASE " + "when pers.tiSexo is 1 then 'Masculino' "
                        + "when pers.tiSexo is 0 then 'Femenino' end, "
                        + "pers.noPersona, pers.apPaterno, pers.apMaterno " + " ) "
                        + "FROM PgimPersona pers WHERE pers.esRegistro = 1 AND pers.tipoDocIdentidad.idValorParametro = 5  "
                        + "AND (:noRazonSocial IS NULL OR LOWER(pers.noRazonSocial || ' ' || pers.noPersona || ' ' || pers.apPaterno || ' ' || pers.apMaterno) LIKE LOWER(CONCAT('%', :noRazonSocial, '%')) "
                        + " ) "
                        + "AND (:coDocumentoIdentidad IS NULL OR LOWER(pers.coDocumentoIdentidad) LIKE LOWER(CONCAT('%', :coDocumentoIdentidad, '%')) ) "
                        + "AND (:idTipoDocIdentidad IS NULL OR LOWER(pers.tipoDocIdentidad.idValorParametro) LIKE LOWER(CONCAT('%', :idTipoDocIdentidad, '%')) ) "

                        + "AND (:textoBusqueda IS NULL OR ( "
                        + "LOWER(pers.noRazonSocial || ' ' || pers.noPersona || ' ' || pers.apPaterno || ' ' || pers.apMaterno) LIKE LOWER(CONCAT('%', :textoBusqueda, '%')) "
                        + "OR LOWER(pers.coDocumentoIdentidad) LIKE LOWER(CONCAT('%', :textoBusqueda, '%')) " + " ))")
        Page<PgimPersonaDTO> listarPersonas(@Param("noRazonSocial") String noRazonSocial,
                        @Param("coDocumentoIdentidad") String coDocumentoIdentidad,
                        @Param("idTipoDocIdentidad") Long idTipoDocIdentidad,
                        @Param("textoBusqueda") String textoBusqueda, Pageable paginador);

        @Query("SELECT new com.mimp.smpt.dtos.PgimPersonaDTOResultado("
                        + "pers.idPersona, pers.coDocumentoIdentidad, pers.noRazonSocial) "
                        + "FROM PgimPersona pers WHERE pers.esRegistro = 1 "
                        + "AND (:palabraClave IS NULL OR LOWER(pers.coDocumentoIdentidad) "
                        + "LIKE LOWER(CONCAT('%', :palabraClave, '%'))  )")
        List<PgimPersonaDTO> listarPorCoDocumentoIdentidad(String palabraClave);

        /**
         * Obtener las propiedades de la persona por el idPersona
         * 
         * @param idPersona identificador de la persona
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimPersonaDTOResultado("
                        // Identificación
                        + "pers.idPersona, pers.tipoDocIdentidad.idValorParametro, pers.tipoDocIdentidad.noValorParametro, "
                        + "pers.coDocumentoIdentidad, UPPER(pers.noRazonSocial), pers.noCorto, UPPER(pers.noPersona), UPPER(pers.apPaterno), UPPER(pers.apMaterno), "
                        + "pers.tiSexo, " + "pers.feNacimiento, "
                        // Contacto
                        + "pers.deTelefono, pers.deTelefono2, pers.deCorreo, pers.deCorreo2, "
                        // Ubicación
                        + "ubig.idUbigeo, "
                        + "((SELECT TRIM(ubde.noUbigeo) FROM PgimUbigeo ubde WHERE ubde.coUbigeo = SUBSTR(ubig.coUbigeo, 0, 2) || '0000') || ', ' || "
                        + "(SELECT TRIM(ubdi.noUbigeo) FROM PgimUbigeo ubdi where ubdi.coUbigeo = SUBSTR(ubig.coUbigeo, 0, 4) || '00') || ', ' || "
                        + "TRIM(ubig.noUbigeo)), pers.diPersona, "
                        // Notificaciones electrónicas
                        + "pers.flAfiliadoNtfccionElctrnca, pers.deCorreoNtfccionElctrnca, pers.feAfiliadoDesde, "
                        // Otros
                        + "pers.cmNota " + " ) "
                        + "FROM PgimPersona pers LEFT OUTER JOIN pers.pgimUbigeo ubig WHERE pers.esRegistro = 1  "
                        + "AND pers.idPersona = :idPersona ")
        PgimPersonaDTO obtenerPersonalNatuJuriPorId(@Param("idPersona") Long idPersona);

        @Query("SELECT new com.mimp.smpt.dtos.PgimPersonaDTOResultado("
                        + "pers.idPersona, pers.tipoDocIdentidad.idValorParametro, pers.tipoDocIdentidad.noValorParametro, "
                        + "pers.coDocumentoIdentidad "
                        // + "pers.noPersona, pers.apPaterno, pers.apMaterno, "
                        // + "pers.feNacimiento, pers.diPersona, "
                        // + "pers.deTelefono "
                        + " ) " + "FROM PgimPersona pers " + "WHERE pers.esRegistro = '1' "
                        + "AND (:idPersona IS NULL OR pers.idPersona != :idPersona) "
                        + "AND pers.tipoDocIdentidad.idValorParametro = :idTipoDocumento "
                        + "AND pers.coDocumentoIdentidad = :numeroDocumento ")
        List<PgimPersonaDTO> existePersona(@Param("idPersona") Long idAccidentado,
                        @Param("idTipoDocumento") Long idTipoDocumento,
                        @Param("numeroDocumento") String numeroDocumento);
}
