package com.mimp.smpt.models.repository;

import java.util.List;

import com.mimp.smpt.dtos.PgimValorParametroDTO;
import com.mimp.smpt.models.entity.PgimValorParametro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * En ésta interface ValorParametroRepository esta conformado pos sus metodos de
 * filtrar por nombres de parametro.
 * 
 * @descripción: Lógica de negocio de la entidad Valor parametro
 * 
 * @author: hdiaz
 * @version: 1.0
 * @fecha_de_creación: 25/05/2020
 * @fecha_de_ultima_actualización: 25/06/2020
 */
public interface ValorParametroRepository extends JpaRepository<PgimValorParametro, Long> {

        /**
         * Permite obtener la lista de valores de parámetro del nombre del parámetro.
         * 
         * @param nombre Nombre del parámetro del que se requieren sus valores.
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "vapa.idValorParametro, vapa.coClave, vapa.noValorParametro, vapa.deValorParametro, vapa.nuOrden, vapa.nuValorNumerico, vapa.deValorAlfanum)"
                        + "FROM PgimValorParametro vapa WHERE vapa.esRegistro = 1 AND vapa.pgimParametro.coParametro = :nombre ORDER BY vapa.nuOrden, vapa.noValorParametro")
        List<PgimValorParametroDTO> filtrarPorNombreParametro(@Param("nombre") String nombre);

        /**
         * Permite obtener la lista de valores de parámetro del nombre del parámetro.
         * 
         * @param nombre Nombre del parámetro del que se requieren sus valores.
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "vapa.idValorParametro, vapa.coClave, vapa.noValorParametro, vapa.deValorParametro, vapa.nuOrden, vapa.nuValorNumerico)"
                        + "FROM PgimValorParametro vapa WHERE vapa.esRegistro = 1 AND vapa.pgimParametro.coParametro = :nombre "
                        + "AND vapa.idValorParametro = :idValorParametro "
                        + "ORDER BY vapa.nuOrden, vapa.noValorParametro")
        List<PgimValorParametroDTO> filtrarPorNombreParametro(@Param("nombre") String nombre,
                        @Param("idValorParametro") Long idValorParametro);

        /**
         * Permite filtrar por subtipo de supervision
         * 
         * @param nombre
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' AND subt.idValorParametro = '287' OR subt.idValorParametro = '288' ")
        List<PgimValorParametroDTO> filtrarPorNombreTipoSupervision(@Param("nombre") String nombre);

        /**
         * Permite filtrar por tipo de involucrado para los representantes de AS y de
         * los trabajadores
         * 
         * @param nombre
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_TIP_INVOLUCRADO ")
        List<PgimValorParametroDTO> filtrarPorNombreTipoInvolucrado(@Param("nombre") String nombre);

        /**
         * Permite filtrar por tipo de estado de configuracion para el ranking de riesgo
         * 
         * @param nombre
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_TIP_ESTADO_CONFIGURACION ") // Falta
                                                                                                                                     // el
                                                                                                                                     // parametro
                                                                                                                                     // de
                                                                                                                                     // configuración
        List<PgimValorParametroDTO> filtrarPorNombreTipEstadoConfig(@Param("nombre") String nombre);

        /**
         * Permite filtrar por tipo de nombre prefijo para los representantes de AS y de
         * los trabajadores
         * 
         * @param nombre
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_TIP_PREFIJO_INVOLUCRADO ")
        List<PgimValorParametroDTO> filtrarPorNombreTipoPrefijo(@Param("nombre") String nombre);

        /**
         * Permite filtrar por tipo de documento de identidad de las personas naturales
         * y juridicas
         * 
         * @param nombre
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_TDI_RUC_DNI_CE ")
        List<PgimValorParametroDTO> filtrarPorTipoDocIdentidad(@Param("nombre") String nombre);

        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.idValorParametro = 6 ")
        List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadRuc(@Param("nombre") String nombre);

        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.idValorParametro = 5 OR subt.idValorParametro = 7 ")
        List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadDniCe(@Param("nombre") String nombre);

        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.idValorParametro = 5 ")
        List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadDni(@Param("nombre") String nombre);

        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.idValorParametro = 7 ")
        List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadCe(@Param("nombre") String nombre);

        /**
         * Permite obtener un registro de la emtidad valor de parámetro
         * 
         * @param idValorParametro
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "vapa.idValorParametro, vapa.coClave, vapa.noValorParametro, vapa.deValorParametro, vapa.nuOrden, vapa.nuValorNumerico)"
                        + "FROM PgimValorParametro vapa WHERE vapa.esRegistro = 1 AND vapa.idValorParametro = :idValorParametro")
        PgimValorParametroDTO obtenerValorParametroPorID(@Param("idValorParametro") Long idValorParametro);

        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_TIPO_NORMA ")
        List<PgimValorParametroDTO> filtrarPorTipoNorma(@Param("nombre") String nombre);

        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_DIVISION_ITEM ")
        List<PgimValorParametroDTO> filtrarPorDivisionItem(@Param("nombre") String nombre);

        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_MEDIDA_ADMINISTRATIVA ")
        List<PgimValorParametroDTO> filtrarPorNombreTipoMedidaAdm(@Param("nombre") String nombre);

        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_TIPO_OBJETO ")
        List<PgimValorParametroDTO> filtrarPorNombreTipoObjeto(@Param("nombre") String nombre);

        /**
         * Me permite filtrar por el nombre de tipo de denuncia
         * 
         * @param nombre
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( "
                        + "subt.idValorParametro, subt.noValorParametro ) "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.coParametro = :nombre ")
        List<PgimValorParametroDTO> filtrarPorMedioDenuncia(@Param("nombre") String nombre);

        /**
         * Permite filtrar por tipo de documento de identidad de las personas
         * denunciantes que son naturales y jurídicas
         * 
         * @param nombre
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( " + "subt.idValorParametro, "
                        + "CASE WHEN subt.idValorParametro " + "IS 6 THEN 'Jurídica' ELSE 'Natural' END " + ") "
                        + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_TDI_RUC_DNI_CE ")
        List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadDenuncia(@Param("nombre") String nombre);

        /**
         * Me permite filtrar por el nombre de materia
         * 
         * @param nombre
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( " + "subt.idValorParametro, "
                        + "subt.deValorParametro " + ") " + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_TIPO_MATERIA ")
        List<PgimValorParametroDTO> filtrarPorTipoMateria(@Param("nombre") String nombre);

        /**
         * Me permite filtrar por el nobre de reporte
         * 
         * @param nombre
         * @return
         */
        @Query("SELECT new com.mimp.smpt.dtos.PgimValorParametroDTOResultado( " + "subt.idValorParametro, "
                        + "subt.noValorParametro " + ") " + "FROM PgimValorParametro subt WHERE subt.esRegistro = '1' "
                        + "AND subt.pgimParametro.idParametro = com.mimp.smpt.utils.ConstantesUtil.PARAM_TIPO_REPORTE ")
        List<PgimValorParametroDTO> filtrarPorTipoReporte(@Param("nombre") String nombre);
}