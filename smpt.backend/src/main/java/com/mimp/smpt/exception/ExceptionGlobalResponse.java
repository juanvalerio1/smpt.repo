package com.mimp.smpt.exception;

import com.mimp.smpt.dtos.ResponseDTO;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ExceptionGlobalResponse extends ResponseEntityExceptionHandler {

	@ExceptionHandler(NotFoundException.class)
	public final ResponseEntity<ResponseDTO> notFoundExeption(NotFoundException ex) {
		log.error(ex.getMessage());
		ResponseDTO response = new ResponseDTO("error", ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(BadRequestException.class)
	public final ResponseEntity<ResponseDTO> badRequestException(BadRequestException ex) {
		log.error(ex.getMessage());
		ResponseDTO response = new ResponseDTO("error", ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodNotAllowedException.class)
	public final ResponseEntity<ResponseDTO> methodNotAllowedException(MethodNotAllowedException ex) {
		log.error(ex.getMessage());
		ResponseDTO response = new ResponseDTO("error", ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.METHOD_NOT_ALLOWED);
	}

	@ExceptionHandler(SgmException.class)
	public final ResponseEntity<ResponseDTO> sgmExceptionPersonalizado(SgmException ex) {
		log.error("Ingreso a SgmException global===========");
		log.error(ex.getMensaje());
		ResponseDTO response = new ResponseDTO(ex.getTipo(), ex.getMensaje());
		return new ResponseEntity<>(response, HttpStatus.NOT_IMPLEMENTED);
	}

	/*
	 * agregar la dependencia de spring security para este método
	 */

	// @ExceptionHandler(AccessDeniedException.class)
	// public final ResponseEntity<ResponseDTO>
	// accesoDenegadoException(AccessDeniedException ex) {
	// log.error("Ingreso a accesoDenegadoException global===========");
	// log.error(ex.getMessage());
	// ResponseDTO response = new ResponseDTO("error", "No estas autorizado para
	// acceder a este recurso");
	// return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
	// }

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ResponseDTO> manejarTodasExcepciones(Exception ex) {

		log.error("Ingreso a Exception global===========");
		log.error(ex.getMessage());

		ex.printStackTrace();

		String message = ex.getCause() != null
				? (ex.getCause().getCause() != null ? ex.getCause().getCause().getMessage() : ex.toString())
				: ex.toString();

		ResponseDTO response = new ResponseDTO("error", message);

		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
