package com.mimp.smpt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmptApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmptApplication.class, args);
	}

}
