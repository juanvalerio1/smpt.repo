package com.mimp.smpt.services.impl;

import java.util.List;

import com.mimp.smpt.dtos.PgimUbigeoDTO;
import com.mimp.smpt.models.repository.UbigeoRepository;
import com.mimp.smpt.services.UbigeoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Servicio para la gestión de la interacción con la base de datos y otros
 * servicios.
 * 
 * @descripción: Lógica de negocio de la entidad Ubigeo
 * 
 * @author: ddelaguila
 * @version: 1.0
 * @fecha_de_creación: 24/05/2020
 * @fecha_de_ultima_actualización: 10/06/2020
 */
@Service
@Transactional(readOnly = true)
public class UbigeoServiceImpl implements UbigeoService {

	// @Autowired
	// private UbigeoRepositoryAlternativo ubigeoRepositoryAlternativo;

	@Autowired
	private UbigeoRepository ubigeoRepository;

	@Override
	public List<PgimUbigeoDTO> listarPorPalabraClave(String palabra) {
		return this.ubigeoRepository.listarPorPalabraClave(palabra);
	}

	@Override
	public List<PgimUbigeoDTO> listarPorPalabraClaveAlternativo(String palabra) {
		return this.ubigeoRepository.listarPorPalabraClaveAlternativo(palabra);
	}

}
