package com.mimp.smpt.services.impl;

import java.util.Date;
import java.util.List;

import com.mimp.smpt.dtos.PgimPersonaDTO;
import com.mimp.smpt.models.entity.PgimPersona;
import com.mimp.smpt.models.entity.PgimUbigeo;
import com.mimp.smpt.models.entity.PgimValorParametro;
import com.mimp.smpt.models.repository.PersonaRepository;
import com.mimp.smpt.services.PersonaService;
import com.mimp.smpt.utils.ConstantesUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

/**
 * Servicio para la gestión de la interacción con la base de datos y otros
 * servicios.
 * 
 * @descripción: Lógica de negocio de la entidad Persona
 * 
 * @author: ddelaguila
 * @version: 1.0
 * @fecha_de_creación: 30/05/2020
 * @fecha_de_ultima_actualización: 30/05/2020
 */
@Service
@Slf4j
@Transactional(readOnly = true)
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	PersonaRepository personaRepository;

	@Override
	public PgimPersona getByIdPersona(Long idPersona) {
		return this.personaRepository.findById(idPersona).orElse(null);
	}

	@Override
	public Page<PgimPersonaDTO> listarPersonas(PgimPersonaDTO filtroPersona, Pageable paginador) {
		Page<PgimPersonaDTO> pPgimPersonaDTO = this.personaRepository.listarPersonas(filtroPersona.getNoRazonSocial(),
				filtroPersona.getCoDocumentoIdentidad(), filtroPersona.getIdTipoDocIdentidad(),
				filtroPersona.getTextoBusqueda(), paginador);
		return pPgimPersonaDTO;
	}

	@Transactional(readOnly = false)
	@Override
	public PgimPersonaDTO crearPersona(PgimPersonaDTO pgimPersonaDTO) throws Exception {
		PgimPersona pgimPersona = new PgimPersona();

		this.configurarValores(pgimPersonaDTO, pgimPersona);

		pgimPersona.setEsRegistro(ConstantesUtil.IND_ACTIVO);
		pgimPersona.setFeCreacion(new Date());
		pgimPersona.setUsCreacion("prueba");
		pgimPersona.setIpCreacion("127.0.0.0");

		// Contacto
		pgimPersona.setDeTelefono(pgimPersonaDTO.getDeTelefono());
		pgimPersona.setDeTelefono2(pgimPersonaDTO.getDeTelefono2());
		pgimPersona.setDeCorreo(pgimPersonaDTO.getDeCorreo());
		pgimPersona.setDeCorreo2(pgimPersonaDTO.getDeCorreo2());

		// Ubicación
		pgimPersona.setDiPersona(pgimPersonaDTO.getDiPersona());

		// Notificaciones electrónicas
		pgimPersona.setFlAfiliadoNtfccionElctrnca(pgimPersonaDTO.getFlAfiliadoNtfccionElctrnca());
		pgimPersona.setDeCorreoNtfccionElctrnca(pgimPersonaDTO.getDeCorreoNtfccionElctrnca());

		// Otros
		pgimPersona.setCmNota(pgimPersonaDTO.getCmNota());

		// Ubicación
		if (pgimPersonaDTO.getIdUbigeo() != null) {
			PgimUbigeo pgimUbigeo = new PgimUbigeo();
			pgimUbigeo.setIdUbigeo(pgimPersonaDTO.getIdUbigeo());
			pgimPersona.setPgimUbigeo(pgimUbigeo);
		} else {
			pgimPersona.setPgimUbigeo(null);
		}

		// Notificaciones electrónicas
		pgimPersona.setFeAfiliadoDesde(pgimPersonaDTO.getFeAfiliadoDesde());

		PgimPersona pgimPersonaCreado = this.personaRepository.save(pgimPersona);

		PgimPersonaDTO pgimPersonaDTOCreado = this.obtenerPersonalNatuJuriPorId(pgimPersonaCreado.getIdPersona());

		return pgimPersonaDTOCreado;
	}

	@Transactional(readOnly = false)
	private PgimPersona configurarValores(PgimPersonaDTO pgimPersonaDTO, PgimPersona pgimPersona) throws Exception {

		// Identificación
		try {
			pgimPersona.setTipoDocIdentidad(new PgimValorParametro());
			pgimPersona.getTipoDocIdentidad().setIdValorParametro(5L);
			// pgimPersona.getTipoDocIdentidad().setIdValorParametro(pgimPersonaDTO.getIdTipoDocIdentidad());

			pgimPersona.setCoDocumentoIdentidad(pgimPersonaDTO.getCoDocumentoIdentidad());
			pgimPersona.setNoPersona(pgimPersonaDTO.getNoPersona().toUpperCase());
			pgimPersona.setApPaterno(pgimPersonaDTO.getApPaterno().toUpperCase());
			pgimPersona.setApMaterno(pgimPersonaDTO.getApMaterno().toUpperCase());
			pgimPersona.setTiSexo(pgimPersonaDTO.getTiSexo());
			pgimPersona.setFeNacimiento(pgimPersonaDTO.getFeNacimiento());
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return pgimPersona;

	}

	@Transactional(readOnly = false)
	@Override
	public PgimPersonaDTO modificarPersona(PgimPersonaDTO pgimPersonaDTO, PgimPersona pgimPersona) throws Exception {

		pgimPersona = this.configurarValores(pgimPersonaDTO, pgimPersona);

		pgimPersona.setFeActualizacion(new Date());
		pgimPersona.setUsActualizacion("prueba");
		pgimPersona.setIpActualizacion("127.0.0.0");

		// Identificación
		if (pgimPersonaDTO.getNoRazonSocial() != null) {
			pgimPersona.setNoRazonSocial(pgimPersonaDTO.getNoRazonSocial().toUpperCase());
			pgimPersona.setNoCorto(pgimPersonaDTO.getNoCorto());
		}

		// Contacto
		pgimPersona.setDeTelefono(pgimPersonaDTO.getDeTelefono());
		pgimPersona.setDeTelefono2(pgimPersonaDTO.getDeTelefono2());
		pgimPersona.setDeCorreo(pgimPersonaDTO.getDeCorreo());
		pgimPersona.setDeCorreo2(pgimPersonaDTO.getDeCorreo2());

		// Ubicación
		pgimPersona.setDiPersona(pgimPersonaDTO.getDiPersona());

		// Notificaciones electrónicas
		pgimPersona.setFlAfiliadoNtfccionElctrnca(pgimPersonaDTO.getFlAfiliadoNtfccionElctrnca());
		pgimPersona.setDeCorreoNtfccionElctrnca(pgimPersonaDTO.getDeCorreoNtfccionElctrnca());

		// Otros
		pgimPersona.setCmNota(pgimPersonaDTO.getCmNota());

		// Ubicación
		if (pgimPersonaDTO.getIdUbigeo() != null) {
			PgimUbigeo pgimUbigeo = new PgimUbigeo();
			pgimUbigeo.setIdUbigeo(pgimPersonaDTO.getIdUbigeo());
			pgimPersona.setPgimUbigeo(pgimUbigeo);
		} else {
			pgimPersona.setPgimUbigeo(null);
		}

		// Notificaciones electrónicas
		pgimPersona.setFeAfiliadoDesde(pgimPersonaDTO.getFeAfiliadoDesde());

		PgimPersona pgimPersonaModificado = this.personaRepository.save(pgimPersona);
		PgimPersonaDTO pgimPersonaDTOResultado = this
				.obtenerPersonalNatuJuriPorId(pgimPersonaModificado.getIdPersona());

		return pgimPersonaDTOResultado;

	}

	@Override
	public PgimPersonaDTO obtenerPersonalNatuJuriPorId(Long idPersona) {
		return this.personaRepository.obtenerPersonalNatuJuriPorId(idPersona);
	}

	@Override
	public PgimPersonaDTO obtenerPersona(Long idPersona) {
		return null;
	}

	@Transactional(readOnly = false)
	@Override
	public void eliminarPersona(PgimPersona pgimPersonaActual) {
		pgimPersonaActual.setEsRegistro(ConstantesUtil.IND_INACTIVO);

		pgimPersonaActual.setFeActualizacion(new Date());
		pgimPersonaActual.setUsActualizacion("prueba");
		pgimPersonaActual.setIpActualizacion("127.0.0.0");

		this.personaRepository.save(pgimPersonaActual);

	}

	@Override
	public List<PgimPersonaDTO> existePersona(Long idPersona, Long idTipoDocumento, String numeroDocumento) {
		List<PgimPersonaDTO> lPgimAccidentadoDTO = this.personaRepository.existePersona(idPersona, idTipoDocumento,
				numeroDocumento);

		return lPgimAccidentadoDTO;
	}

	@Override
	public List<PgimPersonaDTO> listarPorCoDocumentoIdentidad(String palabra) {
		List<PgimPersonaDTO> lPgimPersonaDTO = this.personaRepository.listarPorCoDocumentoIdentidad(palabra);

		return lPgimPersonaDTO;
	}
}