package com.mimp.smpt.services;

import java.util.List;

import com.mimp.smpt.dtos.PgimPersonaDTO;
import com.mimp.smpt.models.entity.PgimPersona;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public interface PersonaService {

    /**
     * Permite obtener el ID de la persona
     * 
     * @param idPersona
     * @return
     */
    PgimPersona getByIdPersona(Long idPersona);

    /**
     * Permite listar las Persona natirales y juridicas con las propiedades de
     * filtros según corresponda.
     * 
     * @param filtroPersona
     * @param paginador
     * @return
     */
    Page<PgimPersonaDTO> listarPersonas(PgimPersonaDTO filtroPersona, Pageable paginador);

    /**
     * Permirte crear una persona juridica o natural
     *
     * @param pgimPersonaDTO
     * @param auditoriaDTO
     * @return
     * @throws Exception
     */
    PgimPersonaDTO crearPersona(PgimPersonaDTO pgimPersonaDTO) throws Exception;

    /***
     * Permite modificar una persona juridica o natural
     * 
     * @param pgimPersonaDTO
     * @param pgimPersona
     * @param auditoriaDTO
     * @return
     */
    PgimPersonaDTO modificarPersona(PgimPersonaDTO pgimPersonaDTO, PgimPersona pgimPersona) throws Exception;

    /**
     * Permite obtener las propiedades necesaria poer el ID de una persona juridica
     * o natural
     * 
     * @param idPersona
     * @return
     */
    PgimPersonaDTO obtenerPersonalNatuJuriPorId(Long idPersona);

    /**
     * Permite obtener las propiedades de una persona juridica o natural para la
     * tarjeta de informacion
     * 
     * @param idPersona
     * @return
     */
    PgimPersonaDTO obtenerPersona(Long idPersona);

    /***
     * Permite eliminar una persona juridica o natural
     * 
     * @param pgimPersonaActual
     * @param auditoriaDTO
     */
    void eliminarPersona(PgimPersona pgimPersonaActual);

    /**
     * Permnite verificar si la persona natural o juridica existe o no.
     * 
     * @param idPersona
     * @param idTipoDocumento
     * @param numeroDocumento
     * @return
     */
    List<PgimPersonaDTO> existePersona(Long idPersona, Long idTipoDocumento, String numeroDocumento);

    /***
     * Permite listar por número de documento de identidad
     * 
     * @param palabra Palabra clave utilizada para buscar en la lista de personas
     * @return
     */
    List<PgimPersonaDTO> listarPorCoDocumentoIdentidad(String palabra);
}
