package com.mimp.smpt.services;

import java.util.List;

import com.mimp.smpt.dtos.PgimValorParametroDTO;

/**
 * Interfaz para la gestión de los servicios relacionados con los parámetros.
 * 
 * @descripción: Parámetro
 *
 * @author: hdiaz
 * @version: 1.0
 * @fecha_de_creación: 24/05/2020
 */
public interface ParametroService {

    /**
     * Permite obtener una lista de valores de parámetros de acuerdo con el
     * parámetro.
     * 
     * @param nombre Nombre del parámetro del que se requiere obtener sus valores.
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorNombreParametro(String nombre);

    /**
     * Permite obtener una lista de valores de parámetros de acuerdo con el
     * parámetro.
     * 
     * @param nombre           Nombre del parámetro del que se requiere obtener sus
     *                         valores.
     * @param idValorParametro IdValor del parametro requerido
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorNombreParametro(String nombre, Long idValorParametro);

    PgimValorParametroDTO obtenerValorParametroPorID(Long idValorParametro);

    /**
     * Permite filtrar el tipo de involucrado para los representantes de AS y de los
     * trabajadores
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorNombreTipoInvolucrado(String nombre);

    /**
     * Permite filtrar el tipo de estado de configuracion del ranking de riesgo
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorNombreTipEstadoConfig(String nombre);

    /**
     * Permite filtrar el tipo de prefijos para los representantes de AS y de los
     * trabajadores
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorNombreTipoPrefijo(String nombre);

    /**
     * Permite filtrar el tipo de documento de identidad de la persona natural y
     * juridica
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorTipoDocIdentidad(String nombre);

    List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadRuc(String nombre);

    List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadDniCe(String nombre);

    List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadDni(String nombre);

    List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadCe(String nombre);

    /**
     * Me permite filtrar por el nombre de tipo de denuncia
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorMedioDenuncia(String nombre);

    // List<PgimSubtipoSupervisionDTO> filtrarPorNombreTipoSupervision(String
    // nombre);
    List<PgimValorParametroDTO> filtrarPorNombreTipoSupervision(String nombre);

    /**
     * Me permite listar por el nombre de tipo de norma
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorTipoNorma(String nombre);

    /**
     * Me permite listar por el nombre de la division de item de normas
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorDivisionItem(String nombre);

    /**
     * Me permite listar por el tipo de medidas administrativas
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorNombreTipoMedidaAdm(String nombre);

    /**
     * Me permite listar por el tipo de objeto para las medidas administrativas
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorNombreTipoObjeto(String nombre);

    /**
     * Permite filtrar por tipo de documento de identidad de las personas
     * denunciantes que son naturales y jurídicas
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadDenuncia(String nombre);

    /**
     * Me permite filtrar la lista por tipo de materia.
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorTipoMateria(String nombre);

    /**
     * Me permite filtrar la lista por tipo de reporte.
     * 
     * @param nombre
     * @return
     */
    List<PgimValorParametroDTO> filtrarPorTipoReporte(String nombre);
}