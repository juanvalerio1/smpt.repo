package com.mimp.smpt.services;

import java.util.List;

import com.mimp.smpt.dtos.PgimUbigeoDTO;

public interface UbigeoService {

	/**
	 * Permite buscar los ubigeos por palabra clave.
	 * 
	 * @param palabra
	 * @return
	 */
	List<PgimUbigeoDTO> listarPorPalabraClave(String palabra);

	List<PgimUbigeoDTO> listarPorPalabraClaveAlternativo(String palabra);

}
