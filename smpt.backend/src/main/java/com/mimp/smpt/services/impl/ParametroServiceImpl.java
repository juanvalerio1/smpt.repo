package com.mimp.smpt.services.impl;

import java.util.List;

import com.mimp.smpt.dtos.PgimValorParametroDTO;
import com.mimp.smpt.models.repository.ValorParametroRepository;
import com.mimp.smpt.services.ParametroService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Servicio para la gestión de la interacción con la base de datos y otros
 * servicios.
 * 
 * @descripción: Lógica de negocio de las entidades Parámetro y valores de
 *               parámetros
 *
 * @author: hdiaz
 * @version: 1.0
 * @fecha_de_creación: 24/05/2020
 * @fecha_de_ultima_actualización: 24/05/2020
 */
@Service
@Transactional(readOnly = true)
public class ParametroServiceImpl implements ParametroService {

    @Autowired
    private ValorParametroRepository valorParametroRepository;

    @Override
    public List<PgimValorParametroDTO> filtrarPorNombreParametro(String nombre) {
        // String appKey = this.sisegLoginRepository.obtenerKeyAplicacionById(921);
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorNombreParametro(nombre);

        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorNombreTipEstadoConfig(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorNombreTipEstadoConfig(nombre);

        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorNombreParametro(String nombre, Long idValorParametro) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorNombreParametro(nombre, idValorParametro);

        return lPgimValorParametroDTO;
    }

    @Override
    public PgimValorParametroDTO obtenerValorParametroPorID(Long idValorParametro) {
        PgimValorParametroDTO pgimValorParametroDTO = this.valorParametroRepository
                .obtenerValorParametroPorID(idValorParametro);

        return pgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorNombreTipoSupervision(String nombre) {
        List<PgimValorParametroDTO> lPgimSubtipoSupervisionDTO = this.valorParametroRepository
                .filtrarPorNombreTipoSupervision(nombre);
        return lPgimSubtipoSupervisionDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorNombreTipoInvolucrado(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorNombreTipoInvolucrado(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorNombreTipoPrefijo(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorNombreTipoPrefijo(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorTipoDocIdentidad(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorTipoDocIdentidad(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadRuc(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorTipoDocIdentidadRuc(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadDniCe(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorTipoDocIdentidadDniCe(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadDni(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorTipoDocIdentidadDni(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadCe(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorTipoDocIdentidadCe(nombre);
        return lPgimValorParametroDTO;
    }

    /**
     * Me permite listar por el nombre de tipo de norma
     * 
     * @param nombre
     * @return
     */
    @Override
    public List<PgimValorParametroDTO> filtrarPorTipoNorma(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository.filtrarPorTipoNorma(nombre);
        return lPgimValorParametroDTO;
    }

    /**
     * Me permite listar por el nombre de la division de item de normas
     * 
     * @param nombre
     * @return
     */
    @Override
    public List<PgimValorParametroDTO> filtrarPorDivisionItem(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorDivisionItem(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorNombreTipoMedidaAdm(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorNombreTipoMedidaAdm(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorNombreTipoObjeto(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorNombreTipoObjeto(nombre);
        return lPgimValorParametroDTO;

    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorMedioDenuncia(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorMedioDenuncia(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorTipoDocIdentidadDenuncia(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorTipoDocIdentidadDenuncia(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorTipoMateria(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorTipoMateria(nombre);
        return lPgimValorParametroDTO;
    }

    @Override
    public List<PgimValorParametroDTO> filtrarPorTipoReporte(String nombre) {
        List<PgimValorParametroDTO> lPgimValorParametroDTO = this.valorParametroRepository
                .filtrarPorTipoReporte(nombre);
        return lPgimValorParametroDTO;
    }
}