import { SidenavToggleDirective } from './sidenav-toggle.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownAnchorDirective } from './dropdown-anchor.directive';
import { DropdownLinkDirective } from './dropdown-link.directive';
import { DropdownDirective } from './dropdown.directive';
import { FontSizeDirective } from './font-size.directive';
import { ScrollToDirective } from './scroll-to.directive';

const directives = [
  FontSizeDirective,
  ScrollToDirective,
  DropdownDirective,
  DropdownAnchorDirective,
  DropdownLinkDirective,
  SidenavToggleDirective,
]

@NgModule({
  declarations: directives,
  imports: [
    CommonModule
  ],
  exports: directives
})
export class DirectivesModule { }
