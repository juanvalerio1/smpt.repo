import { pgimAnimations } from './../../../../shared/animations/pgim-animations';
import { Paginador } from './../../../../core/components/paginador/paginador';
import { finalize, switchMap, debounceTime, tap } from 'rxjs/operators';
import { ETipoAccionCRUD } from './../../../../core/enums/tipo-accion';
import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import { PgimPersonaDTO } from 'src/app/core/models/PgimPersonaDTO';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { PersonaService } from 'src/app/core/services/persona.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AppConfirmService } from 'src/app/core/components/app-confirm/app-confirm.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-persona-lista',
  templateUrl: './persona-lista.component.html',
  styleUrls: ['./persona-lista.component.scss'],
  animations: [pgimAnimations],
})
export class PersonaListaComponent implements OnInit {
  @ViewChild('txtBuscar', { static: true }) txtBuscar: ElementRef;

  @ViewChild(MatSidenav)
  private sideNav: MatSidenav;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Input() pgimPersonaDTO: PgimPersonaDTO;
  /**
   * Grupo de controles.
   */
  formFiltroBusqueda: FormGroup;

  /**
   * Filtro de búsqueda.
   */
  filtroBusqueda: PgimPersonaDTO;

  /**
   * Lista de personas.
   */
  lPgimPersonaDTO: PgimPersonaDTO[];

  /**
   * Señala si la carga de la lista de personas se encuentra en curso.
   */
  enProceso = false;

  /**
   * Portador de los datos de paginación.
   */
  paginador: Paginador;

  /**
   * Lista de numero de contrato buscados por aproximación.
   */
  lPgimPersonaDTOCoDocIdentidad: PgimPersonaDTO[];

  /**
   * Lista de numero de contrato buscados por aproximación.
   */
  // lPgimValorParametroDTOTipoDocIdentidad: PgimValorParametroDTO[];

  /**
   * Señala si la carga de los numeros de docuemnto de identidad por autocompletado se encuentra en curso.
   */
  cargandoPersonaCoDocIdentidad = false;

  /**
   * Señala si la carga de los numeros de expedientes por autocompletado se encuentra en curso.
   */
  cargandoPersonaNoRazonSocial = false;

  /**
   * Lista de numero de expediente buscados por aproximación.
   */
  lPgimPersonaDTONoRazonSocial: PgimPersonaDTO[];

  constructor(
    private personaService: PersonaService,
    private formBuilder: FormBuilder,
    // public authService: AuthService,
    private dialog: MatDialog,
    private router: Router,
    private confirmService: AppConfirmService,
    private matSnackBar: MatSnackBar
  ) {
    this.configurarPaginador();
    this.filtroBusqueda = new PgimPersonaDTO();
  }

  configurarPaginador() {
    this.paginador = new Paginador();
    this.paginador.page = 0;
    this.paginador.size = 20;
    this.paginador.totalElements = 0;
    this.paginador.pageSizeOptions = [10, 20, 40, 60, 100];
    this.paginador.sort = 'apPaterno,asc';
  }

  /**
   * Permite reiniciar el paginador.
   */
  reIniciarPaginador() {
    this.paginador.page = 0;
    this.paginador.totalElements = 0;
  }

  ngOnInit(): void {
    this.inicioFormFiltroBusqueda();
    this.listarPersonas();
  }

  /**
   * Inicia la configuración de los filtros avanzados.
   */
  inicioFormFiltroBusqueda(): void {
    this.formFiltroBusqueda = this.formBuilder.group({
      noRazonSocial: [''],
      coDocumentoIdentidad: [''],
      idTipoDocIdentidad: [''],
    });

    this.configurarFormulario();
    this.configurarAutocompletarNoRazonSocial();
    this.configurarAutocompletarCoDocIdentidad();
  }

  /**
   * Permite listar las supervisiones.
   */
  listarPersonas(): void {
    this.filtroBusqueda = this.formFiltroBusqueda.value;

    this.filtroBusqueda.textoBusqueda = this.txtBuscar.nativeElement.value;

    this.enProceso = true;

    this.personaService
      .listarPersonas(this.filtroBusqueda, this.paginador)
      .subscribe((respuesta) => {
        this.lPgimPersonaDTO = respuesta.content;
        this.paginador.totalElements = respuesta.totalElements;

        this.enProceso = false;
      });
  }

  /**
   * Permite configurar el formulario al arranque.
   */
  configurarFormulario() {
    this.enProceso = true;

    this.personaService.obtenerConfiguraciones().subscribe((respuesta) => {
      const claveTipoDocIdentidad = 'lPgimValorParametroDTOTipoDocIdentidad';

      // this.lPgimValorParametroDTOTipoDocIdentidad =
      //   respuesta[claveTipoDocIdentidad];

      this.enProceso = false;
    });
  }

  /***
   * Permite configurar el autocompletado.
   */
  private configurarAutocompletarNoRazonSocial() {
    const nombreControl = 'noRazonSocial';

    this.formFiltroBusqueda.controls[nombreControl].valueChanges
      .pipe(
        debounceTime(500),
        tap(() => {
          this.lPgimPersonaDTONoRazonSocial = [];
          this.cargandoPersonaNoRazonSocial = true;
        }),
        /** Revisar este metodo */
        switchMap((valor) =>
          this.personaService.listarPorNoRazonSocial(valor).pipe(
            finalize(() => {
              this.cargandoPersonaNoRazonSocial = false;
            })
          )
        )
      )
      .subscribe((respuesta) => {
        if (respuesta === undefined) {
          this.lPgimPersonaDTONoRazonSocial = [];
        } else {
          this.lPgimPersonaDTONoRazonSocial = respuesta;
        }
      });
  }

  /***
   * Permite configurar el autocompletado.
   */
  private configurarAutocompletarCoDocIdentidad() {
    const nombreControl = 'coDocumentoIdentidad';

    this.formFiltroBusqueda.controls[nombreControl].valueChanges
      .pipe(
        debounceTime(500),
        tap(() => {
          this.lPgimPersonaDTOCoDocIdentidad = [];
          this.cargandoPersonaCoDocIdentidad = true;
        }),
        /** Revisar este metodo */
        switchMap((valor) =>
          this.personaService.listarPorCoDocumentoIdentidad(valor).pipe(
            finalize(() => {
              this.cargandoPersonaCoDocIdentidad = false;
            })
          )
        )
      )
      .subscribe((respuesta) => {
        if (respuesta === undefined) {
          this.lPgimPersonaDTOCoDocIdentidad = [];
        } else {
          this.lPgimPersonaDTOCoDocIdentidad = respuesta;
        }
      });
  }

  /**
   * Permite filtrar los personas al enviar el formulario de filtros avanzados.
   */
  filtrarPersona(): void {
    this.reIniciarPaginador();
    this.listarPersonas();
  }

  /**
   * Cambiar el cambio de página o el cambio del tamaño de las páginas.
   * @param event Evento que sucede al cambiar de página o al cambiar el tamaño de las páginas.
   */
  paginar(event: PageEvent): void {
    this.paginador.page = event.pageIndex;
    this.paginador.size = event.pageSize;

    this.listarPersonas();
  }

  toggleSideNav() {
    this.sideNav.opened = !this.sideNav.opened;
  }

  /**
   * Permite realizar la búsqueda desde la caja de búsqueda general.
   * @param valor Palabra que se está buscando.
   */
  buscar(valor: string) {
    this.filtroBusqueda.textoBusqueda = valor;

    this.reIniciarPaginador();
    this.listarPersonas();
  }

  /**
   * Permite lanzar el reordenamiento de la lista de personas.
   * @param valorOption Valor seleccionado en la lista de ordenamiento.
   */
  ordenamiento(valorOption: string) {
    if (valorOption) {
      this.paginador.sort = valorOption;
      this.listarPersonas();
    }
  }

  /**
   * Permite abrir el formulario de personas en solo lectura.
   * @param idPersona Identificador de contrato.
   */
  consultarPersona(idPersona: number) {
    this.router.navigate([
      `/info-maestra/persona/${ETipoAccionCRUD.CONSULTAR}`,
      idPersona,
      0,
    ]);
  }

  /**
   * Permite iniciar la edición de una persona.
   * @param idPersona Identificador de la persona para la edición.
   */
  modificarPersona(idPersona: number) {
    this.router.navigate([
      `/info-maestra/persona/${ETipoAccionCRUD.MODIFICAR}`,
      idPersona,
      0,
    ]);
  }

  /**
   * Permite iniciar la eliminación de una persona
   * @param pgimPersonaDTO Objeto persona a eliminar.
   */
  eliminarPersona(pgimPersonaDTO: PgimPersonaDTO) {
    const contenidoMensaje = `Esta acción eliminará el registro de una persona <strong style="text-transform: uppercase;">${
      pgimPersonaDTO.apPaterno +
      ' ' +
      pgimPersonaDTO.apMaterno +
      ' ' +
      pgimPersonaDTO.noPersona
    } -
                              ${pgimPersonaDTO.coDocumentoIdentidad}</strong>
                           , si lo hace, ya no podrá acceder a su información.`;

    this.confirmService
      .confirmar({
        titulo: '¿Eliminar una persona?',
        mensaje: contenidoMensaje,
        nombreAccion: 'ELIMINAR',
      })
      .subscribe((respuesta) => {
        if (respuesta) {
          // Se procede con la eliminación lógica.
          this.personaService
            .eliminarPersona(pgimPersonaDTO.idPersona)
            .subscribe((respuesta) => {
              if (respuesta.status === 'success') {
                this.matSnackBar.open(respuesta.mensaje, 'Ok', {
                  duration: 5000,
                });
                this.listarPersonas();
              } else if (respuesta.status === 'validacion') {
                this.matSnackBar.open(respuesta.mensaje, 'AVISO', {
                  duration: 12000,
                });
              } else {
                this.matSnackBar.open(respuesta.mensaje, 'ERROR', {
                  duration: 12000,
                });
              }
            });
        }
      });
  }

  /**
   * Permite iniciar la creación de una persona natural o juridica
   */
  crearPersona() {
    this.router.navigate([`/info-maestra/persona/${ETipoAccionCRUD.CREAR}`]);
  }
}
