import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ETipoAccionCRUD } from 'src/app/core/enums/tipo-accion';
import { PgimPersonaDTO } from 'src/app/core/models/PgimPersonaDTO';

@Component({
  selector: 'app-persona-tarjeta',
  templateUrl: './persona-tarjeta.component.html',
  styleUrls: ['./persona-tarjeta.component.scss'],
})
export class PersonaTarjetaComponent implements OnInit {
  @Input() pgimPersonaDTO: PgimPersonaDTO;

  @Input() tipoAccionCrud = ETipoAccionCRUD.NINGUNA;

  @Input() eliminarEsVisible = true;

  @Output() eventoPersonaSolicitadoParaModificar = new EventEmitter<number>();

  @Output() eventoPersonaSolicitadoParaConsultar = new EventEmitter<number>();

  @Output() eventoPersonaSolicitadoParaEliminar =
    new EventEmitter<PgimPersonaDTO>();

  // TDI_DNI = ETipoDocumentoIdentidad.PARAM_TDI_DNI;
  // TDI_RUC = ETipoDocumentoIdentidad.PARAM_TDI_RUC;
  // TDI_CE = ETipoDocumentoIdentidad.PARAM_TDI_CE;

  constructor() {}

  ngOnInit(): void {}

  /**
   * Permite solicitar la modificación de personas.
   * @param idPersona Identificador interno de personas a modificar.
   */
  solicitarModificacion(idPersona: number) {
    this.eventoPersonaSolicitadoParaModificar.emit(idPersona);
  }

  /**
   * Permite abrir el formulario de personas en modo lectura
   * @param idPersona Identificador de personas.
   */
  solicitarConsulta(idPersona: number) {
    this.eventoPersonaSolicitadoParaConsultar.emit(idPersona);
  }

  /**
   * Permite abrir el formulario de consulta para confirmar la eliminación de un personas.
   * @param pgimContratoDTO Objeto de personas.
   */
  solicitarEliminacion(pgimPersonaDTO: PgimPersonaDTO) {
    this.eventoPersonaSolicitadoParaEliminar.emit(pgimPersonaDTO);
  }
}
