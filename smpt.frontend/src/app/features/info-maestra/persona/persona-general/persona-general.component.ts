import { debounceTime, finalize, map, switchMap, tap } from 'rxjs/operators';
import { PgimPersonaDTO } from './../../../../core/models/PgimPersonaDTO';
import { Observable, of } from 'rxjs';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ETipoAccionCRUD } from 'src/app/core/enums/tipo-accion';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { PersonaService } from 'src/app/core/services/persona.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UbigeoService } from 'src/app/core/services/ubigeo.service';
import { ValidadorService } from 'src/app/core/services/validador.service';
import { PgimUbigeoDTO } from 'src/app/core/models/PgimUbigeoDTO';

@Component({
  selector: 'app-persona-general',
  templateUrl: './persona-general.component.html',
  styleUrls: ['./persona-general.component.scss'],
})
export class PersonaGeneralComponent implements OnInit {
  @Input() pgimPersonaDTO: PgimPersonaDTO;

  @Input() tipoAccionCrud = ETipoAccionCRUD.NINGUNA;

  @Input() idPersona: number;

  @Output() eventoPersonaCreado = new EventEmitter<PgimPersonaDTO>();

  // @Input() pgimPersonaDTO: PgimPersonaDTO;

  @Output() eventoPersonaModificado = new EventEmitter<PgimPersonaDTO>();

  tesxto: number;

  enProceso = false;

  controlesNovalidos: any[];

  lPido: any;

  campo: number;

  esFormularioSoloLectura = false;

  frmReactivo: FormGroup;

  pgimUbigeoDTOSelec: PgimUbigeoDTO;

  lPgimUbigeoDTO: PgimUbigeoDTO[];

  cargandoUbigeos = false;

  personaNatural = false;

  personaJuridica = false;

  readonly encode: number;

  idDisabed = false;

  ubigeoReferencia = '';
  // habilitarReadOnlyCE = false;

  // lPgimValorParametroDTOTipoDocIdentidad: PgimValorParametroDTO[];
  // lPgimValorParametroDTOTipoDocIdentidadRuc: PgimValorParametroDTO[];
  // lPgimValorParametroDTOTipoDocIdentidadDniCe: PgimValorParametroDTO[];
  // lPgimValorParametroDTOTipoDocIdentidadDni: PgimValorParametroDTO[];
  // lPgimValorParametroDTOTipoDocIdentidadCe: PgimValorParametroDTO[];

  constructor(
    private formBuilder: FormBuilder,
    private personaService: PersonaService,
    private matSnackBar: MatSnackBar,
    private ubigeoService: UbigeoService,
    private validatorService: ValidadorService
  ) {}

  ngOnInit(): void {
    this.iniciarFormulario();
    this.configurarInicio();
    // this.validarCamposDNI();
  }

  iniciarFormulario() {
    if (this.tipoAccionCrud === ETipoAccionCRUD.CONSULTAR) {
      this.esFormularioSoloLectura = true;
    }

    let dateFeNacimiento: any;
    if (this.pgimPersonaDTO.feNacimiento != null) {
      dateFeNacimiento = new Date(this.pgimPersonaDTO.feNacimiento);
    }

    let dateFeAfiliadoDesde: any;
    if (this.pgimPersonaDTO.feAfiliadoDesde != null) {
      dateFeAfiliadoDesde = new Date(this.pgimPersonaDTO.feAfiliadoDesde);
    }

    this.frmReactivo = this.formBuilder.group({
      // Identificación
      // idTipoDocIdentidad: [
      //   this.pgimPersonaDTO.idTipoDocIdentidad,
      //   [Validators.required],
      // ],
      coDocumentoIdentidad: [this.pgimPersonaDTO.coDocumentoIdentidad],
      noPersona: [this.pgimPersonaDTO.noPersona, [Validators.required]],
      apPaterno: [this.pgimPersonaDTO.apPaterno, [Validators.required]],
      apMaterno: [this.pgimPersonaDTO.apMaterno, [Validators.required]],
      tiSexo: [this.pgimPersonaDTO.tiSexo],
      feNacimiento: [dateFeNacimiento],
      // detalle: [null],

      // Contacto
      deTelefono: [this.pgimPersonaDTO.deTelefono],
      deTelefono2: [this.pgimPersonaDTO.deTelefono2],
      deCorreo: [this.pgimPersonaDTO.deCorreo],
      deCorreo2: [this.pgimPersonaDTO.deCorreo2],

      // Ubicación
      descUbigeo: [this.pgimPersonaDTO.descUbigeo],
      diPersona: [this.pgimPersonaDTO.diPersona],

      // Notificaciones electrónicas
      flAfiliadoNtfccionElctrnca: [
        this.pgimPersonaDTO.flAfiliadoNtfccionElctrnca,
      ],
      deCorreoNtfccionElctrnca: [this.pgimPersonaDTO.deCorreoNtfccionElctrnca],
      feAfiliadoDesde: [dateFeAfiliadoDesde],
      cmNota: [this.pgimPersonaDTO.cmNota],
    });

    // const noCorto = 'noCorto';
    const noPersona = 'noPersona';
    const apPaterno = 'apPaterno';

    if (this.personaNatural) {
      this.frmReactivo.controls[noPersona].setValidators(Validators.required);
      this.frmReactivo.controls[apPaterno].setValidators(Validators.required);
      // this.frmReactivo.controls[noCorto].setValidators(null);
    } else if (this.personaJuridica) {
      this.frmReactivo.controls[noPersona].setValidators(null);
      this.frmReactivo.controls[apPaterno].setValidators(null);

      // this.frmReactivo.controls[noCorto].setValidators(Validators.required);
    }

    // Desde la Pido
    // const claveDescCoDocumentoIdentidadPido = 'coDocumentoIdentidad';
    // this.procesaObtenerCiudadano.bind(this.frmReactivo.controls[claveDescCoDocumentoIdentidadPido].updateValueAndValidity);

    // const claveDescCoDocumentoIdentidad = 'coDocumentoIdentidad';
    // const claveDescIdTipoDocIdentidad = 'idTipoDocIdentidad';
    // this.frmReactivo.controls[claveDescCoDocumentoIdentidad].setValidators([
    //   Validators.required,
    //   Validators.maxLength(15),
    //   this.validatorService.longitudNuDocIdentidadIncorrecta.bind(
    //     this.frmReactivo.controls[claveDescIdTipoDocIdentidad]
    //   ),
    // ]);

    // const claveDescCoDocumentoIdentidad2 = 'coDocumentoIdentidad';
    // this.frmReactivo.controls[
    //   claveDescCoDocumentoIdentidad2
    // ].setAsyncValidators([
    //   this.personaValidator(this.pgimPersonaDTO.idPersona, this.frmReactivo),
    // ]);

    const claveDescDeCorreo = 'deCorreo';
    this.frmReactivo.controls[claveDescDeCorreo].setValidators([
      this.esEmailValidoCorreo.bind(this),
    ]);

    const claveDescDeCorreo2 = 'deCorreo2';
    this.frmReactivo.controls[claveDescDeCorreo2].setValidators([
      this.esEmailValidoCorreo2.bind(this),
    ]);

    const claveDeCorreoNtfccionElctrnca = 'deCorreoNtfccionElctrnca';
    this.frmReactivo.controls[claveDeCorreoNtfccionElctrnca].setValidators([
      this.esEmailValidoDeCorreoNtfccionElctrnca.bind(this),
    ]);

    this.frmReactivo.controls[claveDescDeCorreo].updateValueAndValidity();
    this.frmReactivo.controls[claveDescDeCorreo2].updateValueAndValidity();
    this.frmReactivo.controls[
      claveDeCorreoNtfccionElctrnca
    ].updateValueAndValidity();

    if (this.esFormularioSoloLectura) {
      Object.values(this.frmReactivo.controls).forEach((control) =>
        control.disable()
      );
    }
  }

  configurarInicio() {
    this.enProceso = true;

    let idPersona = 0;

    if (this.pgimPersonaDTO.idPersona) {
      idPersona = this.pgimPersonaDTO.idPersona;
    }

    this.personaService
      .obtenerConfiguracionesGenerales(idPersona)
      .subscribe((respuesta) => {
        // const claveTipoDocIdentidad = 'lPgimValorParametroDTOTipoDocIdentidad';
        // const claveTipoDocIdentidadRuc =
        //   'lPgimValorParametroDTOTipoDocIdentidadRuc';
        // const claveTipoDocIdentidadDniCe =
        //   'lPgimValorParametroDTOTipoDocIdentidadDniCe';
        // const claveTipoDocIdentidadDni =
        //   'lPgimValorParametroDTOTipoDocIdentidadDni';
        // const claveTipoDocIdentidadCe =
        //   'lPgimValorParametroDTOTipoDocIdentidadCe';

        // this.lPgimValorParametroDTOTipoDocIdentidad =
        //   respuesta[claveTipoDocIdentidad];
        // this.lPgimValorParametroDTOTipoDocIdentidadRuc =
        //   respuesta[claveTipoDocIdentidadRuc];
        // this.lPgimValorParametroDTOTipoDocIdentidadDniCe =
        //   respuesta[claveTipoDocIdentidadDniCe];
        // this.lPgimValorParametroDTOTipoDocIdentidadDni =
        //   respuesta[claveTipoDocIdentidadDni];
        // this.lPgimValorParametroDTOTipoDocIdentidadCe =
        //   respuesta[claveTipoDocIdentidadCe];

        this.configurarAutocompletarUbigeo();

        if (
          this.tipoAccionCrud === ETipoAccionCRUD.MODIFICAR ||
          this.tipoAccionCrud === ETipoAccionCRUD.CONSULTAR
        ) {
          this.configurarValoresIniciales();
          this.enProceso = false;
        } else {
          this.configurarValoresIniciales();
          this.enProceso = false;
        }

        // this.enProceso = false;
      });
  }

  esEmailValidoCorreo(email: AbstractControl): { [key: string]: boolean } {
    if (
      email !== null &&
      email.value !== null &&
      email.value !== '' &&
      email.value !== undefined
    ) {
      const EMAIL_REGEX =
        /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      if (email.value.match(EMAIL_REGEX)) {
        return null;
      } else {
        return { descDeCorreoIncorrecto: true };
      }
    } else {
      return null;
    }
  }

  esEmailValidoCorreo2(email: AbstractControl): { [key: string]: boolean } {
    if (
      email !== null &&
      email.value !== null &&
      email.value !== '' &&
      email.value !== undefined
    ) {
      const EMAIL_REGEX =
        /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      if (email.value.match(EMAIL_REGEX)) {
        return null;
      } else {
        return { descDeCorreo2Incorrecto: true };
      }
    } else {
      return null;
    }
  }

  esEmailValidoDeCorreoNtfccionElctrnca(email: AbstractControl): {
    [key: string]: boolean;
  } {
    if (
      email !== null &&
      email.value !== null &&
      email.value !== '' &&
      email.value !== undefined
    ) {
      const EMAIL_REGEX =
        /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      if (email.value.match(EMAIL_REGEX)) {
        return null;
      } else {
        return { deCorreoNtfccionElctrncaIncorrecto: true };
      }
    } else {
      return null;
    }
  }

  private configurarValoresIniciales(): void {
    if (this.pgimPersonaDTO.idPersona) {
      this.pgimUbigeoDTOSelec = new PgimUbigeoDTO();
      this.pgimUbigeoDTOSelec.idUbigeo = this.pgimPersonaDTO.idUbigeo;
      this.pgimUbigeoDTOSelec.noUbigeo = this.pgimPersonaDTO.descUbigeo;
    }
  }

  ubigeoSeleccionado(
    seleccionado: boolean,
    pgimUbigeoDTO: PgimUbigeoDTO
  ): void {
    if (seleccionado) {
      this.pgimUbigeoDTOSelec = pgimUbigeoDTO;
    } else {
      this.pgimUbigeoDTOSelec = null;
    }
  }

  seCambioUbigeo(descUbigeo: string): void {
    if (descUbigeo !== '') {
      if (this.pgimUbigeoDTOSelec?.noUbigeo !== descUbigeo) {
        this.frmReactivo
          .get('descUbigeo')
          .setErrors({ nombreUbigeoIncorrecto: true });
      } else {
        if (
          this.frmReactivo.get('descUbigeo').hasError('nombreUbigeoIncorrecto')
        ) {
          delete this.frmReactivo.get('descUbigeo').errors
            .nombreUbigeoIncorrecto;
        }
      }
    } else {
      this.pgimUbigeoDTOSelec = null;
    }
  }

  private configurarAutocompletarUbigeo() {
    const nombreControl = 'descUbigeo';

    this.frmReactivo.controls[nombreControl].valueChanges
      .pipe(
        debounceTime(500),
        tap(() => {
          this.lPgimUbigeoDTO = [];
          this.cargandoUbigeos = true;
        }),
        switchMap((valor) =>
          this.ubigeoService.listarUbigeosPorPalabraAlternativo(valor).pipe(
            finalize(() => {
              this.cargandoUbigeos = false;
            })
          )
        )
      )
      .subscribe((respuesta) => {
        if (respuesta === undefined) {
          this.lPgimUbigeoDTO = [];
        } else {
          this.lPgimUbigeoDTO = respuesta;
        }
      });
  }

  /**
   * Permite verificar si el control dado por el nombre tiene algún tipo de error.
   * @param campo Nombre del control.
   * @param error Error buscado.
   */
  tieneError(campo: string, error: string): boolean {
    if (error === 'any' || error === '') {
      return this.frmReactivo.get(campo).invalid;
    }

    return this.frmReactivo.get(campo).hasError(error);
  }

  enviar() {
    // this.buscarControlesNoValidos();
    if (this.frmReactivo.invalid) {
      Object.values(this.frmReactivo.controls).forEach((control) =>
        control.markAllAsTouched()
      );

      this.matSnackBar.open(
        'Existen datos incorrectos o faltantes, por favor verifique',
        'CERRAR',
        {
          duration: 4000,
        }
      );
      return;
    }

    this.procesarCrearOModificar();
  }

  /**
   * Permite procesar la acción de crear o modificar una persona.
   */
  procesarCrearOModificar() {
    this.enProceso = true;

    let pgimContratoDTOCU = new PgimPersonaDTO();
    pgimContratoDTOCU = this.frmReactivo.value;

    // Recuperando las propiedades originales
    pgimContratoDTOCU.esRegistro = this.pgimPersonaDTO.esRegistro;
    pgimContratoDTOCU.idPersona = this.pgimPersonaDTO.idPersona;

    // Propiedades para el registro o modificacion del ubigeo dependiendo el tipo de Doc identidad
    if (
      this.pgimUbigeoDTOSelec != null ||
      this.pgimUbigeoDTOSelec != undefined
    ) {
      pgimContratoDTOCU.idUbigeo = this.pgimUbigeoDTOSelec.idUbigeo;
    } else {
      pgimContratoDTOCU.idUbigeo = null;
    }

    let peticion: Observable<PgimPersonaDTO>;
    let mensaje: string;

    switch (+this.tipoAccionCrud) {
      case ETipoAccionCRUD.CREAR:
        peticion = this.personaService.crearPersona(pgimContratoDTOCU);
        mensaje = 'Genial, la persona ha sido creada';
        break;
      case ETipoAccionCRUD.MODIFICAR:
        peticion = this.personaService.modificarPersona(pgimContratoDTOCU);
        mensaje = 'Genial, la persona ha sido modificada';
        break;
      default:
        console.log('Ninguna acción implementada');
        break;
    }

    if (peticion) {
      peticion.subscribe((respuesta) => {
        this.matSnackBar.open(mensaje, 'Ok', {
          duration: 4000,
        });

        this.pgimPersonaDTO = respuesta;

        if (this.tipoAccionCrud === ETipoAccionCRUD.CREAR) {
          this.eventoPersonaCreado.emit(this.pgimPersonaDTO);
        } else {
          this.eventoPersonaModificado.emit(this.pgimPersonaDTO);
          this.enProceso = false;
        }
      });
    }
  }

  get etiquetaAccion(): string {
    let nombreAccionCrud: string;
    if (this.tipoAccionCrud === ETipoAccionCRUD.CREAR) {
      nombreAccionCrud = 'CREAR';
    } else if (this.tipoAccionCrud === ETipoAccionCRUD.MODIFICAR) {
      nombreAccionCrud = 'MODIFICAR';
    }
    return nombreAccionCrud;
  }

  public buscarControlesNoValidos() {
    this.controlesNovalidos = [];
    const controls = this.frmReactivo.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        this.controlesNovalidos.push({
          control: name,
          errores: controls[name].errors,
        });
      }
    }
  }
}
