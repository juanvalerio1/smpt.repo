import { MatSidenav } from '@angular/material/sidenav';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PgimPersonaDTO } from 'src/app/core/models/PgimPersonaDTO';
import { ETipoAccionCRUD } from 'src/app/core/enums/tipo-accion';
import { ConsultaParametroGeneral } from 'src/app/core/models/ConsultaParametroGeneral';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonaService } from 'src/app/core/services/persona.service';

@Component({
  selector: 'app-persona-formulario',
  templateUrl: './persona-formulario.component.html',
  styleUrls: ['./persona-formulario.component.scss'],
})
export class PersonaFormularioComponent implements OnInit {
  @ViewChild(MatSidenav)
  private sideNav: MatSidenav;

  pgimPersonaDTO: PgimPersonaDTO;

  idPersona: PgimPersonaDTO;

  tipoAccionCrud = ETipoAccionCRUD.NINGUNA;

  textoRetorno = '';

  pestaniaSeleccionada = 0;

  idTipoDocIdentidad: number;

  enProceso: boolean;

  origen = 'defecto';

  consultaParametroGeneral: ConsultaParametroGeneral;

  constructor(
    private router: Router,
    private actividateRoute: ActivatedRoute,
    private personaService: PersonaService
  ) {
    this.enProceso = false;
    this.pestaniaSeleccionada = 2;
  }

  ngOnInit(): void {
    this.enProceso = true;

    this.actividateRoute.params.subscribe((respuesta: any) => {
      const claveIdPersona = 'idPersona';
      const claveNroPestania = 'nroPestania';

      const accion = this.actividateRoute.snapshot.url[0].path;

      if (accion === ETipoAccionCRUD.CREAR.toString()) {
        this.tipoAccionCrud = ETipoAccionCRUD.CREAR;
        this.pgimPersonaDTO = new PgimPersonaDTO();
        this.pgimPersonaDTO.idPersona = 0;
        this.enProceso = false;
      } else if (accion === ETipoAccionCRUD.MODIFICAR.toString()) {
        this.tipoAccionCrud = ETipoAccionCRUD.MODIFICAR;

        const idPersona = +respuesta[claveIdPersona];

        if (respuesta[claveNroPestania]) {
          this.pestaniaSeleccionada = +respuesta[claveNroPestania];
        }

        this.obtenerPersonalNatuJuriPorId(idPersona);
      } else if (accion === ETipoAccionCRUD.CONSULTAR.toString()) {
        this.tipoAccionCrud = ETipoAccionCRUD.CONSULTAR;

        const idPersona = +respuesta[claveIdPersona];

        if (respuesta[claveNroPestania]) {
          this.pestaniaSeleccionada = +respuesta[claveNroPestania];
        }

        this.obtenerPersonalNatuJuriPorId(idPersona);
      }
    });
  }

  /**
   * Permite obtener los datos de la persona juridica o natural
   * @param idPersona,
   */
  obtenerPersonalNatuJuriPorId(idPersona: number): void {
    this.personaService
      .obtenerPersonalNatuJuriPorId(idPersona)
      .subscribe((respuesta: any) => {
        const clavePgimPersonaDTO = 'pgimPersonaDTO';
        this.pgimPersonaDTO = respuesta[clavePgimPersonaDTO] as PgimPersonaDTO;
        this.enProceso = false;
      });
  }

  /**
   * Permite la Creacion de la persona natural o juridica
   *
   * @param pgimPersonaDTOCreado;
   */
  eventoPersonaCreado(pgimPersonaDTOCreado: PgimPersonaDTO) {
    this.router.navigate([
      '/info-maestra/persona/2',
      pgimPersonaDTOCreado.idPersona,
      0,
    ]);
  }

  eventoPersonaModificado(pgimPersonaDTOModificado: PgimPersonaDTO) {
    this.pgimPersonaDTO = pgimPersonaDTOModificado;
  }

  get habilitarOtrasPestanias(): boolean {
    let habilitar = false;

    if (
      this.tipoAccionCrud === ETipoAccionCRUD.MODIFICAR ||
      this.tipoAccionCrud === ETipoAccionCRUD.CONSULTAR
    ) {
      habilitar = true;
    }

    return habilitar;
  }

  toggleSideNav() {
    this.sideNav.opened = !this.sideNav.opened;
  }

  /**
   * Me permite retornar a la lista de personas juridicas y naturales
   */
  retornar() {
    this.router.navigate(['/info-maestra/persona']);
  }
}
