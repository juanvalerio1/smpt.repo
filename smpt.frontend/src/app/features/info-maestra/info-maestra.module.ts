import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InfoMaestraRoutingModule } from './info-maestra-routing.module';
import { PersonaContenedorDetalleComponent } from './persona/persona-contenedor-detalle/persona-contenedor-detalle.component';
import { PersonaListaComponent } from './persona/persona-lista/persona-lista.component';
import { PersonaTarjetaComponent } from './persona/persona-tarjeta/persona-tarjeta.component';
import { PersonaFormularioComponent } from './persona/persona-formulario/persona-formulario.component';
import { PersonaGeneralComponent } from './persona/persona-general/persona-general.component';

@NgModule({
  declarations: [
    PersonaContenedorDetalleComponent,
    PersonaListaComponent,
    PersonaTarjetaComponent,
    PersonaFormularioComponent,
    PersonaGeneralComponent,
  ],
  imports: [CommonModule, InfoMaestraRoutingModule, SharedModule],
})
export class InfoMaestraModule {}
