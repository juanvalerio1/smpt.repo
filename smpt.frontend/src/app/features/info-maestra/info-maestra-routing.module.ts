import { PersonaFormularioComponent } from './persona/persona-formulario/persona-formulario.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonaListaComponent } from './persona/persona-lista/persona-lista.component';
import { PersonaContenedorDetalleComponent } from './persona/persona-contenedor-detalle/persona-contenedor-detalle.component';

const routes: Routes = [
  {
    path: 'persona',
    component: PersonaContenedorDetalleComponent,
    data: {
      title: 'Personas',
      breadcrumb: 'Personas',
    },
    children: [
      {
        path: '',
        component: PersonaListaComponent,
        data: {
          title: 'Personas',
          breadcrumb: 'Personas',
        },
      },
      {
        path: '1',
        component: PersonaFormularioComponent,
        data: {
          title: 'Crear',
          breadcrumb: 'Crear persona',
        },
      },
      {
        path: '2/:idPersona/:nroPestania',
        component: PersonaFormularioComponent,
        data: {
          title: 'Modificar',
          breadcrumb: 'Modificar persona',
        },
      },
      {
        path: '4/:idPersona/:nroPestania',
        component: PersonaFormularioComponent,
        data: {
          title: 'Consultar',
          breadcrumb: 'Consultar persona',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfoMaestraRoutingModule {}
