import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutComponent } from './core/components/layout/full-layout/full-layout.component';
import { full } from './core/routes/full.routes';
import { ContentLayoutComponent } from './core/components/layout/content-layout/content-layout.component';
import { content } from './core/routes/content.routes';
import { SecureInnerPagesGuard } from './core/guard/secure-inner-pages.guard';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard/inicio', pathMatch: 'full' },

  {
    path: '',
    component: FullLayoutComponent,
    children: full,
  },

  {
    path: '',
    component: ContentLayoutComponent,
    canActivate: [SecureInnerPagesGuard],
    children: content,
  },

  {
    path: '**',
    redirectTo: 'sesiones/error',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
