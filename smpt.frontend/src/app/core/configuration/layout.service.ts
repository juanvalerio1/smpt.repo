import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


export interface ILayoutConf {
  sidebarStyle?: string; // full, compact, closed
  sidebarCompactToggle?: boolean; // sidebar expandable on hover
  sidebarColor?: string; // Sidebar background color
  dir?: string; // ltr, rtl
  isMobile?: boolean; // updated automatically
  useBreadcrumb?: boolean; // Breadcrumb enabled/disabled
  breadcrumb?: string; // simple, title
  topbarFixed?: boolean; // Fixed header
  footerFixed?: boolean; // Fixed Footer
  topbarColor?: string; // Header background color 
  footerColor?: string // Header background color 
  matTheme?: string; // material theme. sgm-blue, sgm-navy, sgm-light-purple, sgm-dark-purple,sgm-dark-pink
  perfectScrollbar?: boolean;
}

export interface ILayoutChangeOptions {
  duration?: number;
  transitionClass?: boolean;
}

interface IAdjustScreenOptions {
  browserEvent?: any;
  route?: string;
}


@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  //Variables globales de configuracion del layout content
  public layoutConf: ILayoutConf;
  layoutConfSubject = new BehaviorSubject<ILayoutConf>(null);
  layoutConf$ = this.layoutConfSubject.asObservable();
  public isMobile: boolean;
  public currentRoute: string;
  public fullWidthRoutes = ["shop"];

  constructor() {
    this.setAppLayout(
      {
        sidebarStyle: "full", // full, compact, closed
        sidebarColor: "white", //color menu - sidebar se puede personalizar en los styles
        sidebarCompactToggle: false,
        dir: "ltr", // ltr, rtl
        useBreadcrumb: true,
        topbarFixed: false,
        footerFixed: false,
        topbarColor: "indigo", //color tob bar se puede personalizar en los styles
        footerColor: "indigo", //color footer se puede personalizar en los styles
        matTheme: "sgm-navy", //tema personalizado en los styles sgm-blue, sgm-navy,sgm-light-purple,sgm-dark-purple,sgm-dark-pink
        breadcrumb: "simple", // simple, title
        perfectScrollbar: true
      }
    );
  }

  setAppLayout(layoutConf: ILayoutConf) {
    this.layoutConf = { ...this.layoutConf, ...layoutConf };
  }

  publishLayoutChange(lc: ILayoutConf, opt: ILayoutChangeOptions = {}) {
    this.layoutConf = Object.assign(this.layoutConf, lc);
    this.layoutConfSubject.next(this.layoutConf);
  }

  adjustLayout(options: IAdjustScreenOptions = {}) {
    let sidebarStyle: string;
    this.isMobile = this.isSm();
    this.currentRoute = options.route || this.currentRoute;
    sidebarStyle = this.isMobile ? "closed" : "full";

    if (this.currentRoute) {
      this.fullWidthRoutes.forEach(route => {
        if (this.currentRoute.indexOf(route) !== -1) {
          sidebarStyle = "closed";
        }
      });
    }

    this.publishLayoutChange({
      isMobile: this.isMobile,
      sidebarStyle: sidebarStyle
    });

  }

  isSm() {
    return window.matchMedia(`(max-width: 959px)`).matches;
  }


}
