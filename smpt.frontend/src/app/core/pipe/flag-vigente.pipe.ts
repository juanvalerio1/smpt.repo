import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'flagEducacion'
})
export class FlagEducacionPipe implements PipeTransform {

  transform(value: string): string {

    if (value === 'P')
      return 'Primaria'
    else if (value === 'S')
      return 'Secundaria'
    else if (value === 'I')
      return 'Instituto'
    else if (value === 'U')
      return 'Universidad'
    else
      return ''
  }

}
