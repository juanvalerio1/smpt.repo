import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppConfirmComponent } from './components/app-confirm/app-confirm.component';
import { AppLoaderComponent } from './components/app-loader/app-loader.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { ContentLayoutComponent } from './components/layout/content-layout/content-layout.component';
import { FullLayoutComponent } from './components/layout/full-layout/full-layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { AppConfirmService } from './components/app-confirm/app-confirm.service';
import { AppLoaderService } from './components/app-loader/app-loader.service';


@NgModule({
  declarations: [
    AppConfirmComponent,
    AppLoaderComponent,
    BreadcrumbComponent,
    FooterComponent,
    HeaderComponent,
    ContentLayoutComponent,
    FullLayoutComponent,
    SidebarComponent,
    SidenavComponent],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    SharedModule,
  ],
  providers: [    
    AppConfirmService,
    AppLoaderService
  ],
  exports: [
    FooterComponent, 
    HeaderComponent, 
    SidebarComponent, 
    ContentLayoutComponent, 
    FullLayoutComponent,
    BreadcrumbComponent, 
    SidenavComponent]
})
export class CoreModule { }
