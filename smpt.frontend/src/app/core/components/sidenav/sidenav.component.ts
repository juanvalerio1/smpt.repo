import { Component, OnInit } from '@angular/core';
import { BloqueFuncional } from '../../models/BloqueFuncional';
import { MenuService } from '../../services/menu.service';


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  bloquesFuncionales: BloqueFuncional[] = [];

  constructor(private menuService: MenuService) { }

  ngOnInit(): void {

    this.menuService.obtenerBloqueFuncional();

  }


}
