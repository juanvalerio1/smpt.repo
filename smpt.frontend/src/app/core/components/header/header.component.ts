import { Component, OnInit, Input } from '@angular/core';
import { ILayoutConf, LayoutService } from '../../configuration/layout.service';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  //@Input() notificPanel;

  public layoutConf: ILayoutConf ;

  constructor( private layoutService: LayoutService,
    ) { }

  ngOnInit(): void {
    this.layoutConf = this.layoutService.layoutConf;
  }

  abrirPanelNotificacion() {
    //this.notificPanel.toggle();
  }

  //Web
  toggleSidenav() {
    if(this.layoutConf.sidebarStyle === 'closed') {
      return this.layoutService.publishLayoutChange({
        sidebarStyle: 'full'
      })
    }
    this.layoutService.publishLayoutChange({
      sidebarStyle: 'closed'
    })
  }

  //mobile
  toggleCollapse() {
   
    if(this.layoutConf.sidebarStyle === 'compact') {
      return this.layoutService.publishLayoutChange({
        sidebarStyle: 'full',
        sidebarCompactToggle: false
      }, {transitionClass: true})
    }
    
    this.layoutService.publishLayoutChange({
      sidebarStyle: 'compact',
      sidebarCompactToggle: true
    }, {transitionClass: true})

  }


}
