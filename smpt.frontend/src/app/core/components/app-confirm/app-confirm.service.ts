import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { AppConfirmComponent } from './app-confirm.component';

interface ConfirmData {
  titulo?: string,
  mensaje?: string,
  nombreAccion?: string
  ocultarCerrar?: boolean
}

@Injectable({
  providedIn: 'root'
})
export class AppConfirmService {

  dialogRef: MatDialogRef<AppConfirmComponent>;

  constructor(private dialog: MatDialog) { }

  public confirmacion(data: ConfirmData = {}): Observable<boolean> {
    data.titulo = data.titulo || 'Confirmación';
    data.mensaje = data.mensaje || 'Esta seguro?';
    this.dialogRef = this.dialog.open(AppConfirmComponent, {
      width: '380px',
      disableClose: true,
      data: { titulo: data.titulo, mensaje: data.mensaje }
    });
    return this.dialogRef.afterClosed();
  }

  public confirmar(data: ConfirmData = {}): Observable<boolean> {
    data.titulo = data.titulo;
    data.mensaje = data.mensaje;
    data.nombreAccion = data.nombreAccion;
    data.ocultarCerrar = data.ocultarCerrar;

    this.dialogRef = this.dialog.open(AppConfirmComponent, {
      width: '380px',
      disableClose: true,
      data: { titulo: data.titulo, mensaje: data.mensaje, nombreAccion: data.nombreAccion, ocultarCerrar: data.ocultarCerrar }
    });
    return this.dialogRef.afterClosed();
  }
}
