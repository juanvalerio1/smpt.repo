export class Paginador {
    page: number;
    size: number;
    sort: string;
    totalElements: number;
    pageSizeOptions: number[];
}
