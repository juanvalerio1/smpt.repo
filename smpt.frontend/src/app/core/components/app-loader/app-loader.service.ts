import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { AppLoaderComponent } from './app-loader.component';
import { Observable } from 'rxjs';

interface LoaderData {
  titulo?: string,
  width?: string
}

@Injectable()
export class AppLoaderService {

  dialogRef: MatDialogRef<AppLoaderComponent>;

  constructor(private dialog: MatDialog) { }

  public abrir(data: LoaderData = {}): Observable<boolean> {
    data.titulo = data.titulo || 'Por favor, espere un momento';
    data.width = data.width || '200px';
    this.dialogRef = this.dialog.open(AppLoaderComponent, { disableClose: true, backdropClass: 'light-backdrop' });
    this.dialogRef.updateSize(data.width);
    this.dialogRef.componentInstance.titulo = data.titulo;
    return this.dialogRef.afterClosed();
  }

  public cerrar() {
    if (this.dialogRef)
      this.dialogRef.close();
  }
}
