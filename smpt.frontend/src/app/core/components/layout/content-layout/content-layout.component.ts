import { Component, OnInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd, 
     RouteConfigLoadStart, 
     ResolveStart, 
     RouteConfigLoadEnd, ResolveEnd } from '@angular/router';
     
import { filter } from 'rxjs/operators';
import { ILayoutConf, LayoutService } from 'src/app/core/configuration/layout.service';


@Component({
  selector: 'app-content-layout',
  templateUrl: './content-layout.component.html',
  styleUrls: ['./content-layout.component.scss']
})
export class ContentLayoutComponent implements OnInit {

  public isModuleLoading: Boolean = false;

  private moduleLoaderSub: Subscription;
  private layoutConfSub: Subscription;
  private routerEventSub: Subscription;

  public scrollConfig = {}
  public layoutConf: ILayoutConf = {};
  public adminContainerClasses: any = {};


  constructor(
    private router: Router,
    private layoutService: LayoutService,
    private cdr: ChangeDetectorRef) {

    // Close sidenav after route change in mobile
    this.routerEventSub = router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((routeChange: NavigationEnd) => {
        this.layoutService.adjustLayout({ route: routeChange.url });
        this.scrollToTop();
      });

  }


  ngOnInit(): void {

    // this.layoutConf = this.layout.layoutConf;
    this.layoutConfSub = this.layoutService.layoutConf$.subscribe((layoutConf) => {
      this.layoutConf = layoutConf;
      this.adminContainerClasses = this.updateAdminContainerClasses(this.layoutConf);
      this.cdr.markForCheck();
    });

    // FOR MODULE LOADER FLAG
    this.moduleLoaderSub = this.router.events.subscribe(event => {
      if (event instanceof RouteConfigLoadStart || event instanceof ResolveStart) {
        this.isModuleLoading = true;
      }
      if (event instanceof RouteConfigLoadEnd || event instanceof ResolveEnd) {
        this.isModuleLoading = false;
      }
    });

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.layoutService.adjustLayout(event);
  }

  scrollToTop() : void {
    if (document) {
      setTimeout(() => {
        let element;
        if (this.layoutConf.topbarFixed) {
          element = <HTMLElement>document.querySelector('#rightside-content-hold');
        } else {
          element = <HTMLElement>document.querySelector('#main-content-wrap');
        }
        element.scrollTop = 0;
      })
    }
  }

  ngOnDestroy() : void {
    if (this.moduleLoaderSub) {
      this.moduleLoaderSub.unsubscribe();
    }
    if (this.layoutConfSub) {
      this.layoutConfSub.unsubscribe();
    }
    if (this.routerEventSub) {
      this.routerEventSub.unsubscribe();
    }
  }


  closeSidebar() : void {
    this.layoutService.publishLayoutChange({
      sidebarStyle: 'closed'
    })
  }

  //mobile
  sidebarMouseenter(e) {
    if (this.layoutConf.sidebarStyle === 'compact') {
      this.layoutService.publishLayoutChange({ sidebarStyle: 'full' }, { transitionClass: true });
    }
  }

  //mobile
  sidebarMouseleave(e) {
    if (
      this.layoutConf.sidebarStyle === 'full' &&
      this.layoutConf.sidebarCompactToggle
    ) {
      this.layoutService.publishLayoutChange({ sidebarStyle: 'compact' }, { transitionClass: true });
    }
  }

  updateAdminContainerClasses(layoutConf)  {
    return {      
      'sidebar-full': layoutConf.sidebarStyle === 'full',
      'sidebar-compact': layoutConf.sidebarStyle === 'compact',
      'compact-toggle-active': layoutConf.sidebarCompactToggle,
      'sidebar-compact-big': layoutConf.sidebarStyle === 'compact-big' ,
      'sidebar-opened': layoutConf.sidebarStyle !== 'closed' ,
      'sidebar-closed': layoutConf.sidebarStyle === 'closed',
      'fixed-topbar': layoutConf.topbarFixed 
    }
  }


}
