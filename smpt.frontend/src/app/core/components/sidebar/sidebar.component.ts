import { Component, OnInit } from '@angular/core';
import { ILayoutConf, LayoutService } from '../../configuration/layout.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public layoutConf: ILayoutConf;

  constructor(private layoutService: LayoutService,
    ) { }

  ngOnInit(): void {
    this.layoutConf = this.layoutService.layoutConf;
  }

  /*ngOnDestroy() {
    if (this.menuItemsSub) {
      this.menuItemsSub.unsubscribe();
    }
  }*/ 

}
