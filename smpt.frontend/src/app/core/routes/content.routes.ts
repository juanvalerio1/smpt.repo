import { Routes } from '@angular/router';

export const content: Routes = [
  // {
  //   path: 'dialectos',
  //   loadChildren: () => import('../../features/dialectos/dialectos.module').then(dia => dia.DialectosModule),
  //   data: { title: 'Dialectos', breadcrumb: 'Dialectos' }
  // },
  // {
  //   path: 'profesiones',
  //   loadChildren: () => import('../../features/profesiones/profesiones.module').then(prof => prof.ProfesionesModule),
  //   data: { title: 'Profesiones', breadcrumb: 'Profesiones' }
  // },
  // {
  //   path: 'conyuges',
  //   loadChildren: () => import('../../features/conyuges/conyuges.module').then(con => con.ConyugesModule),
  //   data: { title: 'Conyuges', breadcrumb: 'Conyuges' }
  // },
  // {
  //   path: 'centros',
  //   loadChildren: () => import('../../features/centros/centros.module').then(cent => cent.CentrosModule),
  //   data: { title: 'Centro de estudios', breadcrumb: 'Centro de estudios' }
  // },
  {
    path: 'info-maestra',
    loadChildren: () =>
      import('../../features/info-maestra/info-maestra.module').then(
        (emp) => emp.InfoMaestraModule
      ),
    data: { title: 'Personas', breadcrumb: 'Personas' },
  },

  {
    path: 'dashboard',
    loadChildren: () =>
      import('../../features/dashboard/dashboard.module').then(
        (d) => d.DashboardModule
      ),
    data: { title: 'Dashboard', breadcrumb: 'Dashboard' },
  },
];
