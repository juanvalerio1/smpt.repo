export class LegCentroDTO {

  /*
  * Identificador interno del ubigeo. Secuencia: PGIM_SEQ_UBIGEO
  */
  idCentro: number;

  /*
   * Código del ubigeo
   */
  noCentro: string;

  /*
   * Imagen o foto del pais o dialecto
   */
  tipoCentro: string;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Fecha y hora de creación
   */
  feCreacionDesc: string;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;

  /*
   * Fecha y hora de modificación
   */
  feActualizacionDesc: string;

  /*
   * Indicador si se requiere exportar o no a MS-Excel
   */
  exportaExcel: number;

  /*
   * Texto utilizado para una búsqueda genérica.
   */
  textoBusqueda: string;
}
