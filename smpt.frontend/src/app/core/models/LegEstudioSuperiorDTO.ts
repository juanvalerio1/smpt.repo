export class LegEstudioSuperiorDTO {

  /*
   * Identificador interno de la profesión. Secuencia: LEG_ESTUDIO_SUPERIOR_SEQ
   */
  idEstudioSuperior: number;

  /**
   * Identificador foranea del empleado
   */
  idEmpleado: number;

  /**
   * Identificador foranea de la profesión
   */
  idProfesion: number;

  /**
   * Identificador foranea del centro
   */
  idCentro: number;

  /**
   * Tipo de educacion superior (instituto o universidad)
   */
  tiEducacion: string;

  /**
   * Fecha de inicio del estudio superior
   */
  feInicio: Date;

  /**
   * Fecha fin del estudio superior
   */
  feFin: Date;

  /**
   * Tipo grado academico de estudio superior (diplomado, tecnico, bachillerato,
   * licenciatura, maestria, especialidad, doctorado, colegiado asistencial)
   */
  tiGradoAcademico: string;

  /**
   * Número de colegiatura
   */
  nuColegiatura: string;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;

  descNoCentro: string;
  descNoProfesion: string;
}
