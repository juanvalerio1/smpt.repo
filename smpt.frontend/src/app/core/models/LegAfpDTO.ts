export class LegAfpDTO {

  /*
   * Identificador interno de la sustancia. Secuencia: PGIM_SEQ_SUSTANCIA
   */
  idAfp: number;

  /*
   * Tipo de sustancia de la unidad minera. Los valores admisibles se encuentran
   * en la tabla <<PGIM_TP_VALOR_PARAMETRO>> cuando el parámetro relacionado
   * cumple con la condición <<PGIM_TP_PARAMETRO.CO_NOMBRE>> = TIPO_SUSTANCIA.
   * Finalmente, el valor persistido en esta columna es el valor obtenido de
   * <<PGIM_TP_VALOR_PARAMETRO.ID_VALOR_PARAMETRO>>. Tabla padre:
   * PGIM_TP_PARAMETRO
   */
  idTipoAfp: number;

  /*
   * Nombre del método de explotación
   */
  noAfp: string;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;

}
