export class LegRenaesDTO {
    /*
     * Identificador interno de RENAES. Secuencia: LEG_RENAES_SEQ
     */
    idRenaes: number;
  
    /*
     * Codigo unico de renaes
     */
    codigoUnico: String;
  
    /*
     * Codigo unico de ubigeo
     */
    codigoUnicoUbigeo: String;
  
    /*
     * Nombre del dialecto
     */
    clasificacion: String;
  
    /*
     * Nombre del establecimiento
     */
    noEstablecimiento: String;
  
    /*
     * Nombre del departamento
     */
    departamento: String;
  
    /*
     * Nombre del provincia
     */
    provincia: String;
  
    /*
     * Nombre del distrito
     */
    distrito: String;
  }
  