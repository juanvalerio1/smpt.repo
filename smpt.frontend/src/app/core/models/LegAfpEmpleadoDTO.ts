export class LegAfpEmpleadoDTO {

  /*
   * Identificador interno de la sustancia por unidad minera. Secuencia:
   * LEG_AFP_EMPLEADO_SEQ
   */
  idAfpEmpleado: number;

  /*
   * Identificador interno de la unidad minera. Tabla padre: PGIM_TM_UNIDAD_MINERA
   */
  idEmpleado: number;

  /*
   * Identificador interno de la sustancia. Tabla padre: PGIM_TM_SUSTANCIA
   */
  idAfp: number;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;
}
