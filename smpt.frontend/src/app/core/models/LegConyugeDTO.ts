import { LegAfpDTO } from "./LegAfpDTO";

export class LegConyugeDTO {

  /*
     * Identificador interno de la profesión. Secuencia: LEG_PROFESION_SEQ
     */
  idConyuge: number;

  /*
   * Identificador interno de ubigeo. Tabla padre: LEG_UBIGEO
   */
  idUbigeoNacimiento: number;

  /*
   * El tamaño del nombre varia de acuerdo a la institucion ..... Nombre de AFP
   */
  idTipoAfp: number;

  /*
   * Nombre del conyuge
   */
  noConyuge: string;

  /*
   * Abreviatura de la Profesión ("INGENIERO", "ING")
   */
  apPaterno: string;

  /*
   * Abreviatura de la Profesión ("INGENIERO", "ING")
   */
  apMaterno: string;

  /*
   * Nombre del dialecto
   */
  deCelular: string;

  /*
   * Abreviatura de la Profesión ("INGENIERO", "ING")
   */
  dniConyuge: string;

  /*
   * Abreviatura de la Profesión ("INGENIERO", "ING")
   */
  rucConyuge: string;

  /*
   * Abreviatura de la Profesión ("INGENIERO", "ING")
   */
  tipoSexo: string;

  /*
   * Fecha de nacimiento del empleado
   */
  feNacimiento: Date;

  /*
   * Fecha de Afiliación de AFP
   */
  feAfiliacionAfp: Date;

  /*
   * Se necesita saber el país de nacimiento del empleado
   */
  tipoPaisNacimiento: string;

  /*
   * Se necesita saber el país de nacimiento del empleado
   */
  autogeneradoEssalud: string;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;

  /**
   * Texto busqueda
   */
  textoBusqueda: string;

  /*
   * Lista de nombres de AFP del empleado o conyuge
   */
  auxAfp: LegAfpDTO[];

  descDepartamento: string;
  descProvincia: string;
  descDistrito: string;
  descTipoAfp: string;
  descNombreCompleto: string;

  descUbigeoNacimientoCompleto: string;
  descCodigoUnicoNacimiento: string;
}
