import { LegAfpDTO } from "./LegAfpDTO";

export class LegEmpleadoDTO {
  /*
   * Identificador interno del empleado. Secuencia: LEG_EMPLEADO_SEQ
   */
  idEmpleado: number;

  /*
   * Identificador interno de la profesion. Tabla padre: LEG_PROFESION
   */
  idProfesion: number;

  /*
   * Identificador interno de RENAES. Tabla padre: LEG_RENAES
   */
  idRenaes: number;

  /*
   * Identificador interno de ubigeo. Tabla padre: LEG_UBIGEO
   */
  idUbigeoLaboral: number;

  /*
   * Identificador interno de ubigeo. Tabla padre: LEG_UBIGEO
   */
  idUbigeoNacimiento: number;

  /*
   * Sexo del empleado
   */
  tipoSexo: string;

  /*
   * Condicion laboral del empleadoCAS=CAS, NOMBRADO=NOM, CESANTE= CES
   */
  idTipoCondicionLaboral: number;

  /*
   * Grupo sanguíneo del empleado: O+, A+, B+, AB+, O-, A-, B-, AB-
   */
  tipoGrupoSanguineo: string;

  /*
   * Estado civil del empleado: Soltero(S), Casado(C), Viudo(V), Divorciado(D),
   * Conviviente(O)
   */
  tipoEstadoCivil: string;

  /*
   * Régimen pensionario
   */
  idTipoRegPensionario: number;

  /*
   * El tamaño del nombre varia de acuerdo a la institucion ..... Nombre de AFP
   */
  idTipoAfp: number;
  // idTipoNoAfp: number;

  /***
  *Lista de sustancias seleccionadas de la unidad minera
  */
  auxSustanciasUm: LegAfpDTO[];

  /*
   * Se necesita saber el país de nacimiento del empleado
   */
  tipoPaisNacimiento: string;

  /*
   * 728/1057/276/ DL19990
   */
  tipoRegLaboral: string;

  /*
   * Nombre del dialecto
   */
  dniEmpleado: string;

  /*
   * Nombre del dialecto
   */
  noEmpleado: string;

  /*
   * Nombre del dialecto
   */
  apPaterno: string;

  /*
   * Nombre del dialecto
   */
  apMaterno: string;

  /*
   * Nombre del dialecto
   */
  rucEmpleado: string;

  /*
   * Nombre del dialecto
   */
  deCorreo: string;

  /*
   * Nombre del dialecto
   */
  deTelefono: string;

  /*
   * Nombre del dialecto
   */
  deCelular: string;

  /*
   * Fecha de nacimiento del empleado
   */
  feNacimiento: Date;

  /*
   * Grupo sanguíneo del empleado: O+, A+, B+, AB+, O-, A-, B-, AB-
   */
  grupoSanguineo: string;
  /*
   * Estado civil del empleado: Soltero(S), Casado(C), Viudo(V), Divorciado(D),
   * Conviviente(O)
   */
  estadoCivil: string;
  /*
   * Condicion laboral del empleadoCAS=CAS, NOMBRADO=NOM, CESANTE= CES
   */
  descCondicionLaboral: string;
  /*
   * Carnet asegurado del empleado
   */
  carnetAsegurado: string;
  /*
   * Referencia de su domicilio empleado
   */
  domicilio: string;

  /*
   * Fecha de ingreso del empleado al establecimiento
   */
  feIngreso: Date;

  /*
   * Fecha de nombramiento del empleado
   */
  feNombramiento: Date;

  /*
   * Dirección del empleado
   */
  direccion: string;

  /*
   * Referencia de su domicilio empleado
   */
  nuLibretaMilitar: string;

  /*
   * Referencia de su domicilio empleado
   */
  regPensionario: string;

  /*
   * El tamaño del nombre varia de acuerdo a la institucion ..... Nombre de AFP
   */
  noAfp: string;

  /*
   * Fecha de Afiliación de AFP
   */
  feAfiliacionAfp: Date;

  /*
   * El numero es generado por la establecimiento
   */
  nuGenerado: string;

  /*
   * Sexo del empleado
   */
  tiSexo: string;

  /*
   * Se necesita saber el país de nacimiento del empleado
   */
  paisNacimiento: string;

  /*
   * Numero de Pasaporte
   */
  nuPasaporte: string;

  /*
   * Numero de CUSSP
   */
  nuCussp: string;

  /*
   * El tamaño varía de acuerdo al tipo de vehículo
   */
  nuBrevete: string;

  /*
   * 728/1057/276/ DL19990
   */
  rlabEmpleado: string;

  /*
   * Usar codificacion descrita en MOF de la Institución
   */
  catremEmpleado: string;

  /*
   * El nombre de la Oficina, Unidad o Area donde labora
   */
  noOficina: string;

  /*
   * Usuario del empleado
   */
  usuario: string;

  /*
   * Contraseña del Empleado
   */
  contrasenia: string;

  /*
   * Nivel que tendra el empleado 1. Administrador (A) 2. Registrador (R) 3.
   * Visualizador (V)
   */
  nivelRol: string;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;

  /*
   * Indicador si se requiere exportar o no a MS-Excel
   */
  exportaExcel: number;

  /*
   * Texto utilizado para una búsqueda genérica.
   */
  textoBusqueda: string;

  /**----nuevo--- */
  descNoProfesion: string;

  descDniEmpleado: string;
  descNoEmpleadoCompleto: string;

  //ubigeo actual
  descUbigeoActualCompleto: string;
  descCodigoUnicoActual: string;
  descDepartamentoLaboral: string;
  descProvinciaLaboral: string;
  descDistritoLaboral: string;

  //ubigeo nacimiento u origen
  descUbigeoNacimientoCompleto: string;
  descCodigoUnicoNacimiento: string;
  descDepartamentoNacimiento: string;
  descProvinciaNacimiento: string;
  descDistritoNacimiento: string;

  // renaes
  descReanesCompleto: string;
  descCodigoUnicoRenaes: string;
  descClasificacion: string;
  descNoEstablecimiento: string;

  //

  descTiSexo: string;
  descEstadoCivil: string;
  // descCondicionLaboral: string;
}
