export class LegUbigeoDTO {
    /*
     * Identificador interno del dialecto. Secuencia: LEG_UBIGEO_SEQ
     */
    idUbigeo: number;
  
    /*
     * Codigo unico de ubigeo
     */
    codigoUnico: String;
  
    /*
     * Nombre del departamento
     */
    departamento: String;
  
    /*
     * Nombre del provincia
     */
    provincia: String;
  
    /*
     * Nombre del distrito
     */
    distrito: String;
  }