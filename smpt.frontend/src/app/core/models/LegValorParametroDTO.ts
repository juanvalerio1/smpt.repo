export class LegValorParametroDTO {

  /*
   * Identificador interno del valor parametro. Secuencia: LEG_VALOR_PARAMETRO_SEQ
   */
  idValorParametro: number;

  /*
   * Código identificador del parametro tabla: LEG_PARAMETRO.
   */
  idParametro: number;

  /*
   * Código
   */
  coClave: number;

  /*
   * Nombre del valor parametro
   */
  noValorParametro: string;

  /*
   * Descripción del valor parametro
   */
  deValorParametro: string;

  /*
   * Numero de orden en la presentacion
   */
  nuOrden: number;

  /*
   * Valor alfanumerico del Codigo
   */
  deValorAlfanum: string;

  /*
   * Valor numerico del codigo
   */
  noValorNumerico: number;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;
}
