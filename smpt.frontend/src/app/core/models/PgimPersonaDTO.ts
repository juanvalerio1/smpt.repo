/***
* DTO para la entidad PGIM_TM_PERSONA: 
* Persona (natural o jurídica)
*
* Autor: hdiaz
* Versión: 1.0
* Creado el: 15/09/2021
*/
export class PgimPersonaDTO {

  /***
  *Identificador interno de la persona. Secuencia: PGIM_SEQ_PERSONA
  */
  idPersona: number;

  /***
  *Identificador interno del ubigeo. Tabla padre: PGIM_TM_UBIGEO
  */
  idUbigeo: number;

  /***
  *Código de tipo de documento de identidad de la persona. Los valores admisibles se encuentran en la tabla <<PGIM_TP_VALOR_PARAMETRO>> cuando el parámetro relacionado cumple con la condición <<PGIM_TP_PARAMETRO.CO_PARAMETRO>> = TIPO_DOCUMENTO_IDENTIDAD. Finalmente, el valor persistido en esta columna es el valor obtenido de <<PGIM_TP_VALOR_PARAMETRO.ID_VALOR_PARAMETRO>>.  Tabla padre: PGIM_TP_PARAMETRO
  */
  idTipoDocIdentidad: number;

  /***
  *Código del tipo de la fuente de la persona natural. Los valores admisibles se encuentran en la tabla <<PGIM_TP_VALOR_PARAMETRO>> cuando el parámetro relacionado cumple con la condición <<PGIM_TP_PARAMETRO.CO_PARAMETRO>> = FUENTE_PERSONA_NATURAL. Finalmente, el valor persistido en esta columna es el valor obtenido de <<PGIM_TP_VALOR_PARAMETRO.ID_VALOR_PARAMETRO>>.  Tabla padre: PGIM_TP_PARAMETRO
  */
  idFuentePersonaNatural: number;

  /***
  *Número de documento de identidad
  */
  coDocumentoIdentidad: string;

  /***
  *Razón social de la persona jurídica
  */
  noRazonSocial: string;

  /***
  *Nombre corto de la persona jurídica
  */
  noCorto: string;

  /***
  *Nombres de la persona natural
  */
  noPersona: string;

  /***
  *Apellido paterno de la persona natural
  */
  apPaterno: string;

  /***
  *Apellido materno de la persona natural
  */
  apMaterno: string;

  /***
  *Sexo. Los valores admisibles son: "1": Masculino y "0": Femenino
  */
  tiSexo: string;

  /***
  *Fecha de nacimiento
  */
  feNacimiento: Date;

  /*
  *Fecha de nacimiento
  */
  feNacimientoDesc: string;

  /***
  *Teléfono
  */
  deTelefono: string;

  /***
  *Teléfono 02
  */
  deTelefono2: string;

  /***
  *Correo electrónico
  */
  deCorreo: string;

  /***
  *Correo electrónico 02
  */
  deCorreo2: string;

  /***
  *Dirección
  */
  diPersona: string;

  /***
  *Nota
  */
  cmNota: string;

  /***
  *Restricción de acuerdo con el Reniec
  */
  deRestriccion: string;

  /***
  *Flag que indica si la persona se encuentra afiliado a notificación electrónica. Posibles valores: "1" = Sí y 0 = "No"
  */
  flAfiliadoNtfccionElctrnca: string;

  /***
  *Fecha desde que se afilió a la notificación electrónica
  */
  feAfiliadoDesde: Date;

  /*
  *Fecha desde que se afilió a la notificación electrónica
  */
  feAfiliadoDesdeDesc: string;

  /***
  *Correo de notificación electrónica
  */
  deCorreoNtfccionElctrnca: string;

  /***
  *Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
  */
  esRegistro: string;

  /***
  *Usuario creador
  */
  usCreacion: string;

  /***
  *Terminal de creación
  */
  ipCreacion: string;

  /***
  *Fecha y hora de creación
  */
  feCreacion: Date;

  /*
  *Fecha y hora de creación
  */
  feCreacionDesc: string;

  /***
  *Usuario modificador
  */
  usActualizacion: string;

  /***
  *Terminal de modificación
  */
  ipActualizacion: string;

  /***
  *Fecha y hora de modificación
  */
  feActualizacion: Date;

  /*
  *Fecha y hora de modificación
  */
  feActualizacionDesc: string;

  /***
  *Descripcion del nombre de tipo de documento de identidad DNI, CE y RUC
  */
  descIdTipoDocumento: string;

  /***
  *Descripcion del ubigeo
  */
  descUbigeo: string;

  /***
  *Descripción del tipo de documento de identidad
  */
  descIdTipoDocIdentidad: string;

  /***
  *Identificador interno del cliente Siged asociado a la persona de la PGIM
  */
  descIdClienteSiged: string;

  /*
  * Indicador si se requiere exportar o no a MS-Excel
  */
  exportaExcel: number;

  /*
  * Texto utilizado para una búsqueda genérica.
  */
  textoBusqueda: string;


}