export class LegParametroDTO {

  /*
   * Codigo identificador de la tabla parametro. Secuencia: LEG_PARAMETRO_SEQ
   */
  idParametro: number;

  /*
   * Codigo del parametro
   */
  coParametro: string;

  /*
   * Descripcion del parametro
   */
  deParametro: string;

  /*
   * Numero de orden en la presentacion
   */
  nuOrden: number;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;
}
