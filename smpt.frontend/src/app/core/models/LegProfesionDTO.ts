export class LegProfesionDTO {
    /*
     * Identificador interno de la profesión. Secuencia: LEG_PROFESION_SEQ
     */
    idProfesion: number;
  
    /*
     * Nombre de la profesión
     */
    noProfesion: String;
  
    /*
     * Abreviatura de la Profesión ("INGENIERO", "ING")
     */
    noAbreviatura: String;
  
    /*
     * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
     */
    esRegistro: String;
  
    /*
     * Usuario creador
     */
    usCreacion: String;
  
    /*
     * Terminal de creación
     */
    ipCreacion: String;
  
    /*
     * Fecha y hora de creación
     */
    feCreacion: Date;
  
    /*
     * Usuario modificador
     */
    usActualizacion: String;
  
    /*
     * Terminal de modificación
     */
    ipActualizacion: String;
  
    /*
     * Fecha y hora de modificación
     */
    feActualizacion: Date;
  
    /*
     * Indicador si se requiere exportar o no a MS-Excel
     */
    exportaExcel: number;
  
    /*
     * Texto utilizado para una búsqueda genérica.
     */
    textoBusqueda: String;
  }
  