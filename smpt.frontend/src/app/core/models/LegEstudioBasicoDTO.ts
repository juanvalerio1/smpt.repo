export class LegEstudioBasicoDTO {

  /*
   * Identificador interno de la profesión. Secuencia: LEG_ESTUDIO_BASICO_SEQ
   */
  idEstudioBasico: number;

  /**
   * Identificador foranea del empleado
   */
  idEmpleado: number;

  /**
   * Identificador foranea del centro
   */
  idCentro: number;

  /**
   * Tipo de educacion basica (primaria o secundaria)
   */
  tiEducacion: string;

  /**
   * Fecha de inicio del estudio basico
   */
  feInicio: Date;

  /**
   * Fecha fin del estudio basico
   */
  feFin: Date;

  /**
   * Flag de culminacion (Sí o No)
   */
  flCulminacion: string;

  /**
   * Grado academico de estudio basico
   */
  gradoAcademico: string;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;

  /**----------nuevo----------- */
  descNoCentro: string;
}
