/**
 * Clases para sostener la información de los botones de la lista en carga dinámica.
 */
export class BotonLista {
    codigo: string;
    nombre: string;
    nombreIcono: string;
    clic: string

    constructor(codigo: string, nombre: string, nombreIcono: string, clic: string) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.nombreIcono = nombreIcono;
        this.clic = clic;
    }

 }
  