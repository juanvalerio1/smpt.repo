/***
* DTO para la entidad PGIM_TM_UBIGEO: 
* Ubigeo utilizado en la plataforma
*
* Autor: hdiaz
* Versión: 1.0
* Creado el: 15/09/2021
*/
export class PgimUbigeoDTO {

  /***
  *Identificador interno del ubigeo. Secuencia: PGIM_SEQ_UBIGEO
  */
  idUbigeo: number;

  /***
  *Código del ubigeo
  */
  coUbigeo: string;

  /***
  *Nombre del ubigeo
  */
  noUbigeo: string;

  /***
  *Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
  */
  esRegistro: string;

  /***
  *Usuario creador
  */
  usCreacion: string;

  /***
  *Terminal de creación
  */
  ipCreacion: string;

  /***
  *Fecha y hora de creación
  */
  feCreacion: Date;

  /*
  *Fecha y hora de creación
  */
  feCreacionDesc: string;

  /***
  *Usuario modificador
  */
  usActualizacion: string;

  /***
  *Terminal de modificación
  */
  ipActualizacion: string;

  /***
  *Fecha y hora de modificación
  */
  feActualizacion: Date;

  /*
  *Fecha y hora de modificación
  */
  feActualizacionDesc: string;

  /***
  *Distrito
  */
  descDistrito: string;

  /***
  *Provincia
  */
  descProvincia: string;

  /***
  *Departamento
  */
  descDepartamento: string;

  /*
  * Indicador si se requiere exportar o no a MS-Excel
  */
  exportaExcel: number;

  /*
  * Texto utilizado para una búsqueda genérica.
  */
  textoBusqueda: string;


}