import { ItemBloqueFuncional } from './ItemBloqueFuncional';

export class BloqueFuncional {

    codigo: string;
    codigoEstilo: string;
    nombre: string;
    nombreIcono: string;
    items: ItemBloqueFuncional[]
 }
  