export class LegCapacitacionDTO {

  /*
   * Identificador interno de la capacitación. Secuencia: LEG_CAPACITACION_SEQ
   */
  idCapacitacion: number;

  /*
   * Identificador foranea del empleado. Tabla padre: LEG_EMPLEADO
   */
  idEmpleado: number;

  /**
   * Identificador foranea del centro
   */
  idCentro: number;

  /**
   * Tipo capacitación: Cuando el empleado obtiene una costancia interna (I) /
   * externa (E)
   */
  tiCapacitacion: string;

  /**
   * Tipo de participante: Asistente (A), Doctor (D), Enfermera (E)
   */
  tiParticipante: string;

  /**
   * Curso en que se basó la capacitación
   */
  noCurso: string;

  /**
   * Fecha de ejecucion(demostracion => del dia 24 hasta 25 de julio )
   */
  feEjecucionDesc: string;

  /*
   * Fecha en la que inicio la capacitacion
   */
  feInicio: Date;

  /*
   * Fecha en la que culmino la capacitación
   */
  feFin: Date;

  /*
   * Quien Financió el curso
   */
  financioCurso: string;

  /*
   * Numero de Horas que tomó la capacitación
   */
  nuHoras: number;

  /*
   * Numero de Libros
   */
  nuLibro: number;

  /*
   * Numero de folio
   */
  nuFolio: number;

  /*
   * Numero de Registros, se reinicia cada año, paginado de 20
   */
  nuRegistro: number;

  /*
   * Nombre del Director
   */
  noDirector: string;

  /*
   * Cargo de Nivel Director(Nombre de Cargo)
   */
  cargoNivelDirector: string;

  /*
   * Nombre de Director(a) de la Red
   */
  noDirectorRed: string;

  /*
   * Nombre de cargo
   */
  cargoDirectorRed: string;

  /*
   * Nombre de la coordinadora de la Red
   */
  noCoordinadorRed: string;

  /*
   * Cargo de Nivel Coordinadora(Nombre de cargo)
   */
  cargoNivelCoordinador: string;

  /*
   * Fecha cuando se emite la constancia
   */
  feEmision: Date;

  /*
   * Estado del registro. Los posibles valores son: "1" = Activo y "0" = Inactivo
   */
  esRegistro: string;

  /*
   * Usuario creador
   */
  usCreacion: string;

  /*
   * Terminal de creación
   */
  ipCreacion: string;

  /*
   * Fecha y hora de creación
   */
  feCreacion: Date;

  /*
   * Usuario modificador
   */
  usActualizacion: string;

  /*
   * Terminal de modificación
   */
  ipActualizacion: string;

  /*
   * Fecha y hora de modificación
   */
  feActualizacion: Date;

}
