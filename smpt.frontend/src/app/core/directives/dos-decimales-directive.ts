import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[appDosDecimales]'
})
export class DosDecimalesDirective {

    // Permite valores decimales y valores negativos
    private regex: RegExp = new RegExp(/^\d*\.?\d{0,2}$/g);

    // Permite códigos clave para eventos especiales. Reflect :
    // Backspace, tab, end, home
    private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight', 'Del', 'Delete'];

    constructor(private el: ElementRef) {
    }

    @HostListener('keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {

        // Permite Backspace, tab, end, and home keys
        if (this.specialKeys.indexOf(event.key) !== -1) {
            return;
        }

        let current: string = this.el.nativeElement.value;

        const position = this.el.nativeElement.selectionStart;
        const next: string = [current.slice(0, position), event.key == 'Decimal' ? '.' : event.key, current.slice(position)].join('');

        if (next && !String(next).match(this.regex)) {
            event.preventDefault();
        }

    }
}