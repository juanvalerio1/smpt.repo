import { Directive, HostListener, ElementRef, OnInit } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Directive({
    selector: '[appTresDecimalesFormato]'
})
export class TresDecimalesFormatoDirective implements OnInit {

  private el: HTMLInputElement;
  private value: any;

  constructor(private elementRef: ElementRef,
              private decimalPipe: DecimalPipe) {
    this.el = this.elementRef.nativeElement;
  }

  @HostListener('focus', ['$event.target.value'])
  onFocus() {
    this.el.value = this.value;
  }

  @HostListener('blur', ['$event.target.value'])
  onBlur(value) {
    this.cambiarFormato(value);
  }

  ngOnInit(){
    this.cambiarFormato(this.el.value);
  }

  cambiarFormato(value){
    /*this.value = value;
    this.el.value = this.decimalPipe.transform(this.value, '3.2-2');
    this.value = this.el.value;*/
    this.value = value;
    const aux = '' + Math.round(+this.value * 100);
    this.el.value = aux.slice(0, -2) + '.' + aux.slice(-2);
    if (this.el.value.indexOf(this.value) >= 0){
        this.value = this.el.value;
    }
  }
}
