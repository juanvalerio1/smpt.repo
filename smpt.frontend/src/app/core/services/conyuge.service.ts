import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Paginador } from '../components/paginador/paginador';
import { LegConyugeDTO } from '../models/LegConyugeDTO';
import { PageResponse } from '../models/PageResponse';

@Injectable({
  providedIn: 'root'
})
export class ConyugeService {

  private url = `${environment.HOST}/conyugesempleados`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
  }

  listarConyuge(filtro: LegConyugeDTO, paginador: Paginador): Observable<PageResponse<LegConyugeDTO>> {

    const urlEndPoint = `${this.url}/listarConyuge`;

    const parametros = new HttpParams()
      .set('page', paginador.page.toString())
      .set('size', paginador.size.toString())
      .set('sort', paginador.sort);

    return this.httpClient.post<PageResponse<LegConyugeDTO>>(urlEndPoint,
      filtro, { headers: this.httpHeaders, params: parametros });
  }

  listarPorConyuge(descNombreCompleto: string): Observable<LegConyugeDTO[]> {
    if (descNombreCompleto === '') {
      descNombreCompleto = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/nombreConyuge/${descNombreCompleto}`;
    const claveRespuesta = 'lLegConyugeDTONombreConyuge';

    return this.httpClient.get<LegConyugeDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveRespuesta] as LegConyugeDTO[]),
        catchError(error => this.manejarError(error))
      );
  }

  /**
   * Permite crear un nuevo conyuge del empleado
   * @param legCentroDTO,
   */
  crearConyuge(legConyugeDTO: LegConyugeDTO): Observable<LegConyugeDTO> {
    const urlEndPoint = `${this.url}/crearConyuge`;
    const claveConyugeCreado = 'legConyugeDTOCreado';

    return this.httpClient.post<LegConyugeDTO>(urlEndPoint, legConyugeDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveConyugeCreado] as LegConyugeDTO)
      );
  }

  /**
   * Permite modificar un conyuge del empleado
   * @param legCentroDTO,
   */
   modificarConyuge(legConyugeDTO: LegConyugeDTO): Observable<LegConyugeDTO> {
    const urlEndPoint = `${this.url}/modificarConyuge`;
    const claveConyugeModificada = 'legConyugeDTOModificada';
    return this.httpClient.put<LegConyugeDTO>(urlEndPoint, legConyugeDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveConyugeModificada] as LegConyugeDTO)
      );
  }

  /**
   * Permite eliminar un conyuge del empleado
   * @param idProfesion Identificador interno de conyuge del empleado para la eliminación.
   */
  eliminarConyuge(idConyuge: number) {
    const urlEndPoint = `${this.url}/eliminarConyuge/${idConyuge}`;

    return this.httpClient.delete<any>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => {
          if (respuesta['mensaje'] === 'ok') {
            return true;
          }
        })
      );
  }

  /**
   * Permite obtener las configuraciones necesarias para el listado de conyuge del empleado.
   */
  obtenerConfiguracionesGenerales(idConyuge: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguracionesGenerales/${idConyuge}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite obterner el empleado por id
   * @param idEmpleado codigo identificador interno del empleado,
   */
   obtenerConyugePorId(idConyuge: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConyugePorId/${idConyuge}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite analizar el error y responder un error específico para el usuario.
   * @param error que debe ser controlado.
   */
  private manejarError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error del lado del cliente o de la red. Manejarlo en consecuencia.
      console.error("Un error ha ocurrido:", error.error.message);
    } else {
      // El backend devolvió un código de respuesta fallido.
      // El cuerpo de respuesta puede contener pistas sobre lo que salió mal.
      console.error(
        `Código devuelto por el servidor ${error.status}, el cuerpo es: ${error.error.error}`
      );
    }
    // retornar un mensaje observable con un mensaje de error orientado al usuario.
    console.log(error);

    const claveRespuesta = "mensaje";
    const mensajeError = error.error[claveRespuesta] || error.message;

    return throwError(
      `${mensajeError}; por favor, vuelva a intentarlo más tarde.`
    );
  }
}
