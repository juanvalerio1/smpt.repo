import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { PgimPersonaDTO } from 'src/app/core/models/PgimPersonaDTO';
import {
  HttpHeaders,
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Paginador } from 'src/app/core/components/paginador/paginador';
import { PageResponse } from 'src/app/core/models/PageResponse';
import { ResponseDTO } from 'src/app/core/models/ResponseDTO';

/**
 * Servicio que permite el consumo de los servicios del proyecto backend
 *
 * @descripción: Permite consumir los servicios del backend relacionados a la persona
 *
 * @author: jvalerio
 * @version: 1.0
 * @fecha_de_creación: 25/05/2020
 * @fecha_de_ultima_actualización: 02/06/2020
 */
@Injectable({
  providedIn: 'root',
})
export class PersonaService {
  url = `${environment.HOST}/personas`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  /**
   * Permite listar las personas que coinciden con la palabra.
   * @param palabra Palabra que se proporciona como criterio filtro para las personas.
   */
  listarPersonasPorPalabra(palabra: string): Observable<PgimPersonaDTO[]> {
    if (palabra === '') {
      palabra = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/palabraClave/${palabra}`;
    const claveRespuesta = 'lPgimPersonaDTO';

    return this.httpClient
      .get<PgimPersonaDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(map((respuesta) => respuesta[claveRespuesta] as PgimPersonaDTO[]));
  }

  listarPersonas(
    filtro: PgimPersonaDTO,
    paginador: Paginador
  ): Observable<PageResponse<PgimPersonaDTO>> {
    const urlEndPoint = `${this.url}/listarPersonas`;

    const parametros = new HttpParams()
      .set('page', paginador.page.toString())
      .set('size', paginador.size.toString())
      .set('sort', paginador.sort);

    return this.httpClient.post<PageResponse<PgimPersonaDTO>>(
      urlEndPoint,
      filtro,
      { headers: this.httpHeaders, params: parametros }
    );
  }

  /**
   * Permite obtener las configuraciones necesarias para el listado de supervisiones.
   */
  obtenerConfiguraciones(): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguraciones`;

    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite obtener las configuraciones necesarias para el formulario de personas.
   * @param idPersona Identificador de persona, es cero cuando se trata del modo creación,
   * caso contrario debe tener valor.
   */
  obtenerConfiguracionesGenerales(idPersona: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguraciones/generales/${idPersona}`;

    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Me permite filtrar por el codigo o numero de contrato
   * @param palabra = nuContrato,
   */
  listarPorNoRazonSocial(palabra: string): Observable<PgimPersonaDTO[]> {
    if (palabra === '') {
      palabra = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/noRazonSocial/${palabra}`;
    const claveRespuesta = 'lPgimPersonaDTONoRazonSocial';

    return this.httpClient
      .get<PgimPersonaDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map((respuesta) => respuesta[claveRespuesta] as PgimPersonaDTO[]),
        catchError((error) => this.manejarError(error))
      );
  }

  /**
   * Me permite filtrar por el núnero de expediente
   * @param palabra = nuContrato,
   */
  listarPorCoDocumentoIdentidad(palabra: string): Observable<PgimPersonaDTO[]> {
    if (palabra === '') {
      palabra = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/coDocumentoIdentidad/${palabra}`;
    const claveRespuesta = 'lPgimPersonaDTOCoDocumentoIdentidad';

    return this.httpClient
      .get<PgimPersonaDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map((respuesta) => respuesta[claveRespuesta] as PgimPersonaDTO[]),
        catchError((error) => this.manejarError(error))
      );
  }

  /**
   * Permite obterner el contrato por id
   * @param idContrato codigo identificador interno del contrato,
   */
  obtenerPersonalNatuJuriPorId(idPersona: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerPersonalNatuJuriPorId/${idPersona}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite obtener el contrato que coincide con el identificador interno pasado como parámetro.
   * @param idContrato Identificador interno del contrato.
   */
  obtenerPersona(idPersona: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerPersona/${idPersona}`;

    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite crear un contrato.
   * @param pgimPersonaDTO,
   */
  crearPersona(pgimPersonaDTO: PgimPersonaDTO): Observable<PgimPersonaDTO> {
    const urlEndPoint = `${this.url}/crearPersona`;
    const clavePersonaCreado = 'pgimPersonaDTOCreado';

    return this.httpClient
      .post<PgimPersonaDTO>(urlEndPoint, pgimPersonaDTO, {
        headers: this.httpHeaders,
      })
      .pipe(
        // map(respuesta => respuesta['pgimContratoDTO'] as PgimContratoDTO)
        map((respuesta) => respuesta[clavePersonaCreado] as PgimPersonaDTO)
      );
  }

  /**
   * Permite modificar un contrato.
   * @param pgimContratoDTO,
   */
  modificarPersona(pgimPersonaDTO: PgimPersonaDTO): Observable<PgimPersonaDTO> {
    const urlEndPoint = `${this.url}/modificarPersona`;
    const clavePersonaModificada = 'pgimPersonaDTOModificada';
    return this.httpClient
      .put<PgimPersonaDTO>(urlEndPoint, pgimPersonaDTO, {
        headers: this.httpHeaders,
      })
      .pipe(
        map((respuesta) => respuesta[clavePersonaModificada] as PgimPersonaDTO)
      );
  }

  /**
   * Permite eliminar un contrato.
   * @param idContrato Identificador interno del contrato para la eliminación.
   */
  eliminarPersona(idPersona: number) {
    const urlEndPoint = `${this.url}/eliminarPersona/${idPersona}`;

    return this.httpClient.delete<ResponseDTO>(urlEndPoint, {
      headers: this.httpHeaders,
    });
  }

  yaExistePersona(
    idPersona: number,
    idTipoDocumento: number,
    numeroDocumento: string
  ): Observable<any> {
    const urlEndPoint = `${this.url}/existePersona/${idPersona}/${idTipoDocumento}/${numeroDocumento}`;

    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite analizar el error y responder un error específico para el usuario.
   * @param error que debe ser controlado.
   */
  private manejarError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error del lado del cliente o de la red. Manejarlo en consecuencia.
      console.error('Un error ha ocurrido:', error.error.message);
    } else {
      // El backend devolvió un código de respuesta fallido.
      // El cuerpo de respuesta puede contener pistas sobre lo que salió mal.
      console.error(
        `Código devuelto por el servidor ${error.status}, el cuerpo es: ${error.error.error}`
      );
    }
    // retornar un mensaje observable con un mensaje de error orientado al usuario.
    console.log(error);

    const claveRespuesta = 'mensaje';
    const mensajeError = error.error[claveRespuesta] || error.message;

    return throwError(
      `${mensajeError}; por favor, vuelva a intentarlo más tarde.`
    );
  }

  /**
   * Permite obtener los datos de la persona natural mediante DNI/CE desde la PIDO
   * @param numeroDNIoCE,
   */
  procesaObtenerCiudadano(numeroDNIoCE: string): Observable<any> {
    if (numeroDNIoCE === '') {
      numeroDNIoCE = '_vacio_';
    }
    const urlEndPoint = `${this.url}/procesaObtenerCiudadano/${numeroDNIoCE}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite obtener los datos de la persona natural mediante DNI/CE desde la PIDO
   * @param numeroDNIoCE,
   */
  procesaObtenerContribuyente(numeroRUC: string): Observable<any> {
    if (numeroRUC === '') {
      numeroRUC = '_vacio_';
    }
    const urlEndPoint = `${this.url}/procesaObtenerContribuyente/${numeroRUC}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  listarPersonaNaturalPorNoRazonSocial(
    idAgenteSupervisado: number,
    palabra: string
  ): Observable<PgimPersonaDTO[]> {
    if (palabra === '') {
      palabra = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrarPersonaNatural/noRazonSocial/${idAgenteSupervisado}/${palabra}`;
    const claveRespuesta = 'lPgimPersonaDTONoRazonSocial';

    return this.httpClient
      .get<PgimPersonaDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map((respuesta) => respuesta[claveRespuesta] as PgimPersonaDTO[]),
        catchError((error) => this.manejarError(error))
      );
  }
}
