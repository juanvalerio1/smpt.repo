import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { LegEmpleadoDTO } from '../models/LegEmpleadoDTO';
import { PageResponse } from '../models/PageResponse';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Paginador } from '../components/paginador/paginador';

@Injectable({
  providedIn: 'root',
})
export class EmpleadoService {
  private url = `${environment.HOST}/empleados`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  /**
   * Permite consultar la lista de contratos de acuerdo con los criterios filtro y de manera paginada.
   * @param filtro Objeto filtro que porta los criterios a aplicar.
   * @param paginador Objeto para la paginación.
   */
  listarEmpleado(
    filtro: LegEmpleadoDTO,
    paginador: Paginador
  ): Observable<PageResponse<LegEmpleadoDTO>> {
    const urlEndPoint = `${this.url}/listarEmpleado`;

    const parametros = new HttpParams()
      .set('page', paginador.page.toString())
      .set('size', paginador.size.toString())
      .set('sort', paginador.sort);

    return this.httpClient.post<PageResponse<LegEmpleadoDTO>>(
      urlEndPoint,
      filtro,
      { headers: this.httpHeaders, params: parametros }
    );
  }

  /**
   * Permite obtener las configuraciones necesarias para el listado de empleados.
   */
  obtenerConfiguraciones(): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguraciones`;

    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite listar mediante un autocomplete en los filtros avanzados por el nombre o DNI del empleado.
   * @param palabra
   * @returns
   */
  listarPorEmpleado(palabra: string): Observable<LegEmpleadoDTO[]> {
    if (palabra === '') {
      palabra = '_vacio_';
    }

    const urlEndPoint = `${this.url}/listarPorEmpleado/${palabra}`;
    const claveRespuesta = 'lLegEmpleadoDTO';

    return this.httpClient
      .get<LegEmpleadoDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map((respuesta) => respuesta[claveRespuesta] as LegEmpleadoDTO[]),
        catchError((error) => this.manejarError(error))
      );
  }

  /**
   * Permite listar mediante un autocomplete en los filtros avanzados por el RUC del empleado.
   * @param palabra
   * @returns
   */
  listarPorRucEmpleado(rucEmpleado: string): Observable<LegEmpleadoDTO[]> {
    if (rucEmpleado === '') {
      rucEmpleado = '_vacio_';
    }

    const urlEndPoint = `${this.url}/listarPorRucEmpleado/${rucEmpleado}`;
    const claveRespuesta = 'lLegEmpleadoDTORuc';

    return this.httpClient
      .get<LegEmpleadoDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map((respuesta) => respuesta[claveRespuesta] as LegEmpleadoDTO[]),
        catchError((error) => this.manejarError(error))
      );
  }

  /**
   * Permite eliminar un empleado.
   * @param idEmpleado Identificador interno del empleado para la eliminación.
   */
  eliminarEmpleado(idEmpleado: number) {
    const urlEndPoint = `${this.url}/eliminarEmpleado/${idEmpleado}`;

    return this.httpClient
      .delete<any>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map((respuesta) => {
          if (respuesta['mensaje'] === 'ok') {
            return true;
          }
        })
      );
  }

  /**
   * Permite obtener las configuraciones necesarias para el formulario de empleados.
   * @param idEmpleado Identificador del empleado, es cero cuando se trata del modo creación,
   * caso contrario debe tener valor.
   */
  obtenerConfiguracionesGenerales(idEmpleado: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguraciones/generales/${idEmpleado}`;

    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite obterner el empleado por id
   * @param idEmpleado codigo identificador interno del empleado,
   */
  obtenerEmpleadoPorId(idEmpleado: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerEmpleadoPorId/${idEmpleado}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite crear un empleado cas, cesante o nombrado.
   * @param legEmpleadoDTO,
   */
  crearEmpleado(legEmpleadoDTO: LegEmpleadoDTO): Observable<LegEmpleadoDTO> {
    const urlEndPoint = `${this.url}/crearEmpleado`;
    const claveEmpleadoCreado = 'legEmpleadoDTO';

    return this.httpClient
      .post<LegEmpleadoDTO>(urlEndPoint, legEmpleadoDTO, {
        headers: this.httpHeaders,
      })
      .pipe(
        // map(respuesta => respuesta['pgimContratoDTO'] as PgimContratoDTO)
        map((respuesta) => respuesta[claveEmpleadoCreado] as LegEmpleadoDTO)
      );
  }

  /***
   * Permite obtener las sustancias de una unidad minera dada.
   */
    obtenerSustanciasUM(idEmpleado: number): Observable<any> {
      const urlEndPoint = `${this.url}/obtenerSustanciasUM/${idEmpleado}`;

      return this.httpClient.get<any[]>(urlEndPoint);
    }

  /**
   * Permite modificar un empleado.
   * @param legEmpleadoDTO,
   */
  modificarEmpleado(
    legEmpleadoDTO: LegEmpleadoDTO
  ): Observable<LegEmpleadoDTO> {
    const urlEndPoint = `${this.url}/modificarEmpleado`;

    return this.httpClient
      .put<LegEmpleadoDTO>(urlEndPoint, legEmpleadoDTO, {
        headers: this.httpHeaders,
      })
      .pipe(
        map(
          (respuesta) => respuesta['legEmpleadoDTOModificada'] as LegEmpleadoDTO
        )
      );
  }

  /**
   * Permite analizar el error y responder un error específico para el usuario.
   * @param error que debe ser controlado.
   */
  private manejarError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error del lado del cliente o de la red. Manejarlo en consecuencia.
      console.error('Un error ha ocurrido:', error.error.message);
    } else {
      // El backend devolvió un código de respuesta fallido.
      // El cuerpo de respuesta puede contener pistas sobre lo que salió mal.
      console.error(
        `Código devuelto por el servidor ${error.status}, el cuerpo es: ${error.error.error}`
      );
    }
    // retornar un mensaje observable con un mensaje de error orientado al usuario.
    console.log(error);

    const claveRespuesta = 'mensaje';
    const mensajeError = error.error[claveRespuesta] || error.message;

    return throwError(
      `${mensajeError}; por favor, vuelva a intentarlo más tarde.`
    );
  }
}
