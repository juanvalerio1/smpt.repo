import { Injectable } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import { AppSettings } from "../constants/app-settings";


/**
 * Servicio que permite realizar validaciones para los tipos de documentos de la entidad persona
 *
 * @descripción: Permite realizar validaciones para los tipos de documentos de la entidad Persona
 *
 * @author: hdiaz
 * @version: 1.0
 * @fecha_de_creación: 24/05/2020
 * @fecha_de_ultima_actualización: 24/05/2020
 */
@Injectable({
  providedIn: 'root'
})
export class ValidadorService {

  constructor() { }

  /**
   * Permite validar si la longitud del número de documento de identidad es el correcto o no.
   * @param controlNumero Control input del número de documento de identidad.
   */
  longitudNuDocIdentidadIncorrecta(controlNumero: AbstractControl): { [key: string]: boolean } {
    const ctrlTipoDocIdentidad: any = this;

    let longitudRequerida: number;

    let valorTipo: number;
    let valorNumero: string;

    valorTipo = ctrlTipoDocIdentidad.value;
    valorNumero = controlNumero.value;

    if (!valorTipo || !valorNumero) {
      return null;
    }

    if (valorTipo === AppSettings.PARAM_TIDOC_ID_DNI) {
      longitudRequerida = 8;
    } else if (valorTipo === AppSettings.PARAM_TIDOC_ID_RUC) {
      longitudRequerida = 11;
    } else if (valorTipo === AppSettings.PARAM_TIDOC_ID_CE) {
      longitudRequerida = 9;
    }

    if (valorNumero.length !== longitudRequerida) {
      return { longitudNuDocIdentidadIncorrecta: true };
    } else {
      return null;
    }
  }

  longitudRucIncorrecta(controlNumero: AbstractControl): { [key: string]: boolean } {
    let longitudRequerida: number;
    let valorNumero: string;

    valorNumero = controlNumero.value;
    longitudRequerida = 11;

    if (!valorNumero) {
      return null;
    }

    if (valorNumero.length !== longitudRequerida) {
      return { longitudRucIncorrecta: true };
    } else {
      return null;
    }
  }

}
