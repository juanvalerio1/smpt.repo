import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { LegProfesionDTO } from "../models/LegProfesionDTO";
import { environment } from 'src/environments/environment';
import { Paginador } from '../components/paginador/paginador';
import { PageResponse } from '../models/PageResponse';

@Injectable({
  providedIn: 'root'
})
export class ProfesionService {

  private url = `${environment.HOST}/profesiones`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
  }

  listarProfesion(filtro: LegProfesionDTO, paginador: Paginador): Observable<PageResponse<LegProfesionDTO>> {

    const urlEndPoint = `${this.url}/listarProfesion`;

    const parametros = new HttpParams()
      .set('page', paginador.page.toString())
      .set('size', paginador.size.toString())
      .set('sort', paginador.sort);

    return this.httpClient.post<PageResponse<LegProfesionDTO>>(urlEndPoint,
      filtro, { headers: this.httpHeaders, params: parametros });
  }

  listarPorProfesion(noProfesion: string): Observable<LegProfesionDTO[]> {
    if (noProfesion === '') {
      noProfesion = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/palabraClave/${noProfesion}`;
    const claveRespuesta = 'lLegProfesionDTO';

    return this.httpClient.get<LegProfesionDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveRespuesta] as LegProfesionDTO[]),
        catchError(error => this.manejarError(error))
      );
  }

  /**
   * Permite crear una profesion.
   * @param legProfesionDTO,
   */
  crearProfesion(legProfesionDTO: LegProfesionDTO): Observable<LegProfesionDTO> {
    const urlEndPoint = `${this.url}/crearProfesion`;
    const claveProfesionCreado = 'legProfesionDTOCreado';

    return this.httpClient.post<LegProfesionDTO>(urlEndPoint, legProfesionDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveProfesionCreado] as LegProfesionDTO)
      );
  }

  /**
   * Permite modificar una profesion.
   * @param pgimContratoDTO,
   */
  modificarProfesion(legProfesionDTO: LegProfesionDTO): Observable<LegProfesionDTO> {
    const urlEndPoint = `${this.url}/modificarProfesion`;
    const claveProfesionModificada = 'legProfesionDTOModificada';
    return this.httpClient.put<LegProfesionDTO>(urlEndPoint, legProfesionDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveProfesionModificada] as LegProfesionDTO)
      );
  }

  /**
   * Permite eliminar una profesión.
   * @param idProfesion Identificador interno de la profesión para la eliminación.
   */
  eliminarProfesion(idProfesion: number) {
    const urlEndPoint = `${this.url}/eliminarProfesion/${idProfesion}`;

    return this.httpClient.delete<any>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => {
          if (respuesta['mensaje'] === 'ok') {
            return true;
          }
        })
      );
  }

  /**
   * Permite obtener las configuraciones necesarias para el listado de profesiones.
   */
  obtenerConfiguracionesGenerales(idProfesion: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguracionesGenerales/${idProfesion}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite analizar el error y responder un error específico para el usuario.
   * @param error que debe ser controlado.
   */
  private manejarError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error del lado del cliente o de la red. Manejarlo en consecuencia.
      console.error("Un error ha ocurrido:", error.error.message);
    } else {
      // El backend devolvió un código de respuesta fallido.
      // El cuerpo de respuesta puede contener pistas sobre lo que salió mal.
      console.error(
        `Código devuelto por el servidor ${error.status}, el cuerpo es: ${error.error.error}`
      );
    }
    // retornar un mensaje observable con un mensaje de error orientado al usuario.
    console.log(error);

    const claveRespuesta = "mensaje";
    const mensajeError = error.error[claveRespuesta] || error.message;

    return throwError(
      `${mensajeError}; por favor, vuelva a intentarlo más tarde.`
    );
  }
}
