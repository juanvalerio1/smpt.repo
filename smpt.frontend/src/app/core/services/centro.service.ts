import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Paginador } from '../components/paginador/paginador';
import { LegCentroDTO } from '../models/LegCentroDTO';
import { PageResponse } from '../models/PageResponse';

@Injectable({
  providedIn: 'root'
})
export class CentroService {

  private url = `${environment.HOST}/centrosestudios`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
  }

  listarCentro(filtro: LegCentroDTO, paginador: Paginador): Observable<PageResponse<LegCentroDTO>> {

    const urlEndPoint = `${this.url}/listarCentro`;

    const parametros = new HttpParams()
      .set('page', paginador.page.toString())
      .set('size', paginador.size.toString())
      .set('sort', paginador.sort);

    return this.httpClient.post<PageResponse<LegCentroDTO>>(urlEndPoint,
      filtro, { headers: this.httpHeaders, params: parametros });
  }

  listarPorCentro(noCentro: string): Observable<LegCentroDTO[]> {
    if (noCentro === '') {
      noCentro = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/nombreCentro/${noCentro}`;
    const claveRespuesta = 'lLegCentroDTONombreCentro';

    return this.httpClient.get<LegCentroDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveRespuesta] as LegCentroDTO[]),
        catchError(error => this.manejarError(error))
      );
  }

  /**
   * Permite crear un centro de estudio.
   * @param legCentroDTO,
   */
  crearCentro(legCentroDTO: LegCentroDTO): Observable<LegCentroDTO> {
    const urlEndPoint = `${this.url}/crearCentro`;
    const claveCentroCreado = 'legCentroDTOCreado';

    return this.httpClient.post<LegCentroDTO>(urlEndPoint, legCentroDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveCentroCreado] as LegCentroDTO)
      );
  }

  /**
   * Permite modificar un centro de estudio.
   * @param legCentroDTO,
   */
  modificarCentro(legCentroDTO: LegCentroDTO): Observable<LegCentroDTO> {
    const urlEndPoint = `${this.url}/modificarCentro`;
    const claveCentroModificada = 'legCentroDTOModificada';
    return this.httpClient.put<LegCentroDTO>(urlEndPoint, legCentroDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveCentroModificada] as LegCentroDTO)
      );
  }

  /**
   * Permite eliminar un centro de estudio.
   * @param idProfesion Identificador interno de centro de estudio para la eliminación.
   */
  eliminarCentro(idCentro: number) {
    const urlEndPoint = `${this.url}/eliminarCentro/${idCentro}`;

    return this.httpClient.delete<any>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => {
          if (respuesta['mensaje'] === 'ok') {
            return true;
          }
        })
      );
  }

  /**
   * Permite obtener las configuraciones necesarias para el listado de centro de estudio.
   */
  obtenerConfiguracionesGenerales(idCentro: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguracionesGenerales/${idCentro}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite analizar el error y responder un error específico para el usuario.
   * @param error que debe ser controlado.
   */
  private manejarError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error del lado del cliente o de la red. Manejarlo en consecuencia.
      console.error("Un error ha ocurrido:", error.error.message);
    } else {
      // El backend devolvió un código de respuesta fallido.
      // El cuerpo de respuesta puede contener pistas sobre lo que salió mal.
      console.error(
        `Código devuelto por el servidor ${error.status}, el cuerpo es: ${error.error.error}`
      );
    }
    // retornar un mensaje observable con un mensaje de error orientado al usuario.
    console.log(error);

    const claveRespuesta = "mensaje";
    const mensajeError = error.error[claveRespuesta] || error.message;

    return throwError(
      `${mensajeError}; por favor, vuelva a intentarlo más tarde.`
    );
  }
}
