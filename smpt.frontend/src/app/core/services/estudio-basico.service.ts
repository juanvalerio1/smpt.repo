import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LegEstudioBasicoDTO } from '../models/LegEstudioBasicoDTO';

@Injectable({
  providedIn: 'root'
})
export class EstudioBasicoService {

  private url = `${environment.HOST}/estudiosbasicos`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
  }

  /**
   * Permite crear un contrato siaf.
   * @param pgimContratoDTO,
   */
  crearEstudioBasico(legEstudioBasicoDTO: LegEstudioBasicoDTO): Observable<LegEstudioBasicoDTO> {
    const urlEndPoint = `${this.url}/crearEstudioBasico`;
    const claveContratoSiaf = 'legEstudioBasicoDTO';

    return this.httpClient.post<LegEstudioBasicoDTO>(urlEndPoint, legEstudioBasicoDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveContratoSiaf] as LegEstudioBasicoDTO)
      );
  }

  /**
   * Permite modificar un estudio basico
   * @param pgimContratoDTO,
   */
  modificarEstudioBasico(legEstudioBasicoDTO: LegEstudioBasicoDTO): Observable<LegEstudioBasicoDTO> {
    const urlEndPoint = `${this.url}/modificarEstudioBasico`;
    const claveProfesionModificada = 'legEstudioBasicoDTOModificada';
    return this.httpClient.put<any>(urlEndPoint, legEstudioBasicoDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveProfesionModificada] as LegEstudioBasicoDTO)
      );
  }

  obtenerEstudioBasicoPorId(idEstudioBasico: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerEstudioBasicoPorId/${idEstudioBasico}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  obtenerEstudioBasicoPorIdEmpleado(idEmpleado: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerEstudioBasicoPorIdEmpleado/${idEmpleado}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite modificar un estudio superior
   * @param pgimContratoDTO,
   */
  modificarEstudioSuperior(legEstudioSuperiorDTO: any): Observable<any> {
    const urlEndPoint = `${this.url}/modificarEstudioSuperior`;
    const claveProfesionModificada = 'legEstudioSuperiorDTOModificada';
    return this.httpClient.put<any>(urlEndPoint, legEstudioSuperiorDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveProfesionModificada] as any)
      );
  }

  /**
   * Permite obtener las configuraciones necesarias para los estudios basicos.
   */
  obtenerConfiguraciones(): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguraciones`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
    * Permite analizar el error y responder un error específico para el usuario.
    * @param error que debe ser controlado.
    */
  private manejarError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error del lado del cliente o de la red. Manejarlo en consecuencia.
      console.error("Un error ha ocurrido:", error.error.message);
    } else {
      // El backend devolvió un código de respuesta fallido.
      // El cuerpo de respuesta puede contener pistas sobre lo que salió mal.
      console.error(
        `Código devuelto por el servidor ${error.status}, el cuerpo es: ${error.error.error}`
      );
    }
    // retornar un mensaje observable con un mensaje de error orientado al usuario.
    console.log(error);

    const claveRespuesta = "mensaje";
    const mensajeError = error.error[claveRespuesta] || error.message;

    return throwError(
      `${mensajeError}; por favor, vuelva a intentarlo más tarde.`
    );
  }
}
