import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LegEstudioSuperiorDTO } from '../models/LegEstudioSuperiorDTO';

@Injectable({
  providedIn: 'root'
})
export class EstudioSuperiorService {

  private url = `${environment.HOST}/estudiossuperiores`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
  }

  /**
   * Permite crear un contrato siaf.
   * @param legEstudioSuperiorDTO,
   */
  crearEstudioSuperior(legEstudioSuperiorDTO: LegEstudioSuperiorDTO): Observable<LegEstudioSuperiorDTO> {
    const urlEndPoint = `${this.url}/crearEstudioSuperior`;
    const claveContratoSiaf = 'legEstudioSuperiorDTO';

    return this.httpClient.post<LegEstudioSuperiorDTO>(urlEndPoint, legEstudioSuperiorDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveContratoSiaf] as LegEstudioSuperiorDTO)
      );
  }

  obtenerEstudioSuperiorPorId(idEstudioSuperior: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerEstudioSuperiorPorId/${idEstudioSuperior}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  obtenerEstudioSuperiorPorIdEmpleado(idEmpleado: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerEstudioSuperiorPorIdEmpleado/${idEmpleado}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite modificar un estudio superior
   * @param pgimContratoDTO,
   */
  modificarEstudioSuperior(legEstudioSuperiorDTO: LegEstudioSuperiorDTO): Observable<LegEstudioSuperiorDTO> {
    const urlEndPoint = `${this.url}/modificarEstudioSuperior`;
    const claveProfesionModificada = 'legEstudioSuperiorDTOModificada';
    return this.httpClient.put<LegEstudioSuperiorDTO>(urlEndPoint, legEstudioSuperiorDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveProfesionModificada] as LegEstudioSuperiorDTO)
      );
  }

  /**
   * Permite obtener las configuraciones necesarias para los estudios basicos.
   */
  obtenerConfiguraciones(): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguraciones`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
    * Permite analizar el error y responder un error específico para el usuario.
    * @param error que debe ser controlado.
    */
  private manejarError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error del lado del cliente o de la red. Manejarlo en consecuencia.
      console.error("Un error ha ocurrido:", error.error.message);
    } else {
      // El backend devolvió un código de respuesta fallido.
      // El cuerpo de respuesta puede contener pistas sobre lo que salió mal.
      console.error(
        `Código devuelto por el servidor ${error.status}, el cuerpo es: ${error.error.error}`
      );
    }
    // retornar un mensaje observable con un mensaje de error orientado al usuario.
    console.log(error);

    const claveRespuesta = "mensaje";
    const mensajeError = error.error[claveRespuesta] || error.message;

    return throwError(
      `${mensajeError}; por favor, vuelva a intentarlo más tarde.`
    );
  }
}
