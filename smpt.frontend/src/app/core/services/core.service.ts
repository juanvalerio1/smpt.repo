import { Injectable } from '@angular/core';
import { BloqueFuncional } from '../models/BloqueFuncional';
import { ItemBloqueFuncional } from '../models/ItemBloqueFuncional';

/**
 * Servicio que permite el consumo de los servicios del proyecto backend 
 * 
 * @descripción: Permite consumir los servicios del backend relacionados a los módulos que tiene acceso el usuario
 * 
 * @author: hdiaz
 * @version: 1.0
 * @fecha_de_creación: 30/05/2020
 * @fecha_de_ultima_actualización: 08/07/2020
 */

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  private bloquesFuncionales: BloqueFuncional[];

  constructor() {
    this.bloquesFuncionales = [
      {
        codigo: 'lvlPanelControl',
        codigoEstilo: 'item-name lvlPanelControl',
        nombre: 'Panel',
        nombreIcono: 'dashboard',
        items: [
          {
            codigo: 'pg_inicio',
            nombre: 'Inicio',
            nombreIcono: 'home',
            enlace: '/dashboard/inicio'
          }
        ]
      },
       /* {
          codigo: 'lvlAnalisisRiesgos',
          codigoEstilo: 'item-name lvlAnalisisRiesgos',
          nombre: 'Riesgos',
          nombreIcono: 'sync_problem',
          items: [
              {
                  nombre: 'Metodología',
                  nombreIcono: 'home',
                  enlace: '/dashboard/inicio'
              },
              {
                  nombre: 'Ranking',
                  nombreIcono: 'home',
                  enlace: '/dashboard/inicio'
              }
          ]
        },
        {
          codigo: 'lvlProgramacion',
          codigoEstilo: 'item-name lvlProgramacion',
          nombre: 'Programación',
          nombreIcono: 'event_available',
          items: [
              {
                  nombre: 'Planes',
                  nombreIcono: 'home',
                  enlace: '/dashboard/inicio'
              },
              {
                  nombre: 'Programas',
                  nombreIcono: 'home',
                  enlace: '/dashboard/inicio'
              }
          ]
        },*/
        {
          codigo: 'lvlSupervision',
          codigoEstilo: 'item-name lvlSupervision',
          nombre: 'Supervisión',
          nombreIcono: 'policy',
          items: [
            {
              codigo: 'Supervisiones',
              nombre: 'Supervisiones',
              nombreIcono: 'home',
              enlace: '/supervision/supervisiones'
            },
            // {
            //   nombre: 'Medidas administrativas',
            //   nombreIcono: 'home',
            //   enlace: '/documento/documento-prueba'
            // }
          ]
        },
        /*
        {
          codigo: 'lvlPresupuestoYGasto',
          codigoEstilo: 'item-name lvlPresupuestoYGasto',
          nombre: 'Contratos',
          nombreIcono: 'monetization_on',
          items: [
              {
                  nombre: 'Contratos',
                  nombreIcono: 'home',
                  enlace: '/dashboard/inicio'
              },
              {
                  nombre: 'Liquidaciones',
                  nombreIcono: 'home',
                  enlace: '/dashboard/inicio'
              }
          ]
        },
        {
          codigo: 'lvlFiscalizacion',
          codigoEstilo: 'item-name lvlFiscalizacion',
          nombre: 'Fiscalización',
          nombreIcono: 'gavel',
          items: [
              {
                  nombre: 'PAS',
                  nombreIcono: 'home',
                  enlace: '/dashboard/inicio'
              }                
          ]
        },
        {
          codigo: 'lvlMoExpedientes',
          codigoEstilo: 'item-name lvlMoExpedientes',
          nombre: 'Monitoreo',
          nombreIcono: 'next_week',
          items: [
              {
                  nombre: 'Supervisión',
                  nombreIcono: 'home',
                  enlace: '/dashboard/inicio'
              },
              {
                  nombre: 'Fiscalización',
                  nombreIcono: 'home',
                  enlace: '/dashboard/inicio'
              }                
          ]
        },*/
      {
        codigo: 'lvlInfoMaestra',
        codigoEstilo: 'item-name lvlInfoMaestra',
        nombre: 'Maestros',
        nombreIcono: 'construction',
        items: [
          {
            codigo: 'agente-supervisado',
            nombre: 'Agente supervisado',
            nombreIcono: 'home',
            enlace: '/info-maestra/agente-supervisado'
          },
          {
            codigo: 'unidad-minera',
            nombre: 'Unidad minera',
            nombreIcono: 'home',
            enlace: '/info-maestra/unidad-minera'
          },
          {
            codigo: 'supervisora',
            nombre: 'Supervisora',
            nombreIcono: 'home',
            enlace: '/info-maestra/empresa-supervisora'
          }/*,
          {
              nombre: 'Fuentes externas',
              nombreIcono: 'home',
              enlace: '/dashboard/inicio'
          },
          {
              nombre: 'Normas',
              nombreIcono: 'home',
              enlace: '/dashboard/inicio'
          },
          {
              nombre: 'Tipificaciones',
              nombreIcono: 'home',
              enlace: '/dashboard/inicio'
          },
          {
              nombre: 'Matrices de supervisión',
              nombreIcono: 'home',
              enlace: '/dashboard/inicio'
          },*/
        ]
      },
      {
        codigo: 'lvlDocumentos',
        codigoEstilo: 'item-name lvlDocumentos',
        nombre: 'Documentos',
        nombreIcono: 'engineering',
        items: [
          {
            codigo: 'lvlDocumento',
            nombre: 'Documentos misionales y no misionales',
            nombreIcono: 'home',
            enlace: '/documento/documento-misional'
          },
          {
            codigo: 'lvlexpediente',
            nombre: 'Expediente y documento Siged',
            nombreIcono: 'home',
            enlace: '/documento/expediente-documento-siged'
          },
          {
            codigo: 'lvlformato',
            nombre: 'Generar formatos',
            nombreIcono: 'home',
            enlace: '/documento/generar-formato'
          }
        ]
      }
    ];
  }

  /**
   * Permite obtener los bloques funcionales
   */
  obtenerBloqueFuncional() {
    return this.bloquesFuncionales;
  }
}