import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LegRenaesDTO } from '../models/LegRenaesDTO';

@Injectable({
  providedIn: 'root'
})
export class RenaesService {

  private url = `${environment.HOST}/renaes`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ "Content-Type": "application/json" });
  }

  listarPorRenaes(codigoUnico: string): Observable<LegRenaesDTO[]> {
    if (codigoUnico === '') {
      codigoUnico = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/palabraClave/${codigoUnico}`;
    const claveRespuesta = 'lLegRenaesDTO';

    return this.httpClient.get<LegRenaesDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveRespuesta] as LegRenaesDTO[]),
        catchError(error => this.manejarError(error))
      );
  }

  /**
   * Permite analizar el error y responder un error específico para el usuario.
   * @param error que debe ser controlado.
   */
   private manejarError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error del lado del cliente o de la red. Manejarlo en consecuencia.
      console.error("Un error ha ocurrido:", error.error.message);
    } else {
      // El backend devolvió un código de respuesta fallido.
      // El cuerpo de respuesta puede contener pistas sobre lo que salió mal.
      console.error(
        `Código devuelto por el servidor ${error.status}, el cuerpo es: ${error.error.error}`
      );
    }
    // retornar un mensaje observable con un mensaje de error orientado al usuario.
    console.log(error);

    const claveRespuesta = "mensaje";
    const mensajeError = error.error[claveRespuesta] || error.message;

    return throwError(
      `${mensajeError}; por favor, vuelva a intentarlo más tarde.`
    );
  }
}