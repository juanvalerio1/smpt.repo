import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PgimUbigeoDTO } from '../models/PgimUbigeoDTO';
import { LegUbigeoDTO } from './../models/LegUbigeoDTO';

@Injectable({
  providedIn: 'root',
})
export class UbigeoService {
  url: string = `${environment.HOST}/ubigeos`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  /**
   * Permite listar los ubigeos que coinciden con la palabra.
   * @param palabra Palabra que se proporciona como criterio filtro para los ubigeos.
   */
  listarUbigeosPorPalabra(palabra: string): Observable<PgimUbigeoDTO[]> {
    if (palabra === '') {
      palabra = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/palabraClave/${palabra}`;
    const claveRespuesta = 'lPgimUbigeoDTO';

    return this.httpClient
      .get<PgimUbigeoDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(map((respuesta) => respuesta[claveRespuesta] as PgimUbigeoDTO[]));
  }

  listarUbigeosPorPalabraAlternativo(
    palabra: string
  ): Observable<PgimUbigeoDTO[]> {
    if (palabra === '') {
      palabra = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/palabraClaveAlternativo/${palabra}`;
    const claveRespuesta = 'lPgimUbigeoDTO';

    return this.httpClient
      .get<PgimUbigeoDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(map((respuesta) => respuesta[claveRespuesta] as PgimUbigeoDTO[]));
  }
}
