import { environment } from 'src/environments/environment';
import { LegDialectoDTO } from './../models/LegDialectoDTO';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { throwError, Observable, of } from 'rxjs';
import { PageResponse } from '../models/PageResponse';
import { map, catchError } from 'rxjs/operators';
import { Paginador } from '../components/paginador/paginador';

@Injectable({
  providedIn: 'root'
})
export class DialectoService {

  private url = `${environment.HOST}/dialectos`;

  private httpHeaders: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  /**
   * Permite consultar la lista de dialectos de acuerdo con los criterios filtro y de manera paginada.
   * @param filtro Objeto filtro que porta los criterios a aplicar.
   * @param paginador Objeto para la paginación.
   */
  listarDialecto(filtro: LegDialectoDTO, paginador: Paginador): Observable<PageResponse<LegDialectoDTO>> {

    const urlEndPoint = `${this.url}/listarDialecto`;

    const parametros = new HttpParams()
      .set('page', paginador.page.toString())
      .set('size', paginador.size.toString())
      .set('sort', paginador.sort);

    return this.httpClient.post<PageResponse<LegDialectoDTO>>(urlEndPoint,
      filtro, { headers: this.httpHeaders, params: parametros });
  }

  obtenerDialectoPorId(idDialecto: number): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerDialectoPorId/${idDialecto}`;
    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite eliminar un dialecto.
   * @param idDialecto Identificador interno del dialecto para la eliminación.
   */
  eliminarDialecto(idDialecto: number) {
    const urlEndPoint = `${this.url}/eliminarDialecto/${idDialecto}`;

    return this.httpClient.delete<any>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => {
          if (respuesta['mensaje'] === 'ok') {
            return true;
          }
        })
      );
  }

  /**
   * Me permite filtrar por el codigo o numero de dialecto
   * @param palabra = nuDialecto,
   */
  listarPorNombreDialecto(palabra: string): Observable<LegDialectoDTO[]> {
    if (palabra === '') {
      palabra = '_vacio_';
    }

    const urlEndPoint = `${this.url}/filtrar/nombreDialecto/${palabra}`;
    const claveRespuesta = 'lLegDialectoDTONombreDialecto';

    return this.httpClient.get<LegDialectoDTO[]>(urlEndPoint, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveRespuesta] as LegDialectoDTO[]),
        catchError(error => this.manejarError(error))
      );
  }

  /**
   * Permite crear un dialecto.
   * @param pgimPersonaDTO,
   */
  crearDialecto(legDialectoDTO: LegDialectoDTO): Observable<LegDialectoDTO> {
    const urlEndPoint = `${this.url}/crearDialecto`;
    const claveDialectoCreado = 'legDialectoDTOCreado';

    return this.httpClient.post<LegDialectoDTO>(urlEndPoint, legDialectoDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveDialectoCreado] as LegDialectoDTO)
      );
  }

  /**
   * Permite modificar un dialecto.
   * @param pgimContratoDTO,
   */
  modificarDialecto(legDialectoDTO: LegDialectoDTO): Observable<LegDialectoDTO> {
    const urlEndPoint = `${this.url}/modificarDialecto`;
    const claveDialectoModificada = 'legDialectoDTOModificada';
    return this.httpClient.put<LegDialectoDTO>(urlEndPoint, legDialectoDTO, { headers: this.httpHeaders })
      .pipe(
        map(respuesta => respuesta[claveDialectoModificada] as LegDialectoDTO)
      );
  }

  /**
   * Permite obtener las configuraciones necesarias para el listado de dialecto.
   */
  obtenerConfiguraciones(): Observable<any> {
    const urlEndPoint = `${this.url}/obtenerConfiguraciones`;

    return this.httpClient.get<any[]>(urlEndPoint);
  }

  /**
   * Permite analizar el error y responder un error específico para el usuario.
   * @param error que debe ser controlado.
   */
  private manejarError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error del lado del cliente o de la red. Manejarlo en consecuencia.
      console.error('Un error ha ocurrido:', error.error.message);
    } else {
      // El backend devolvió un código de respuesta fallido.
      // El cuerpo de respuesta puede contener pistas sobre lo que salió mal.
      console.error(`Código devuelto por el servidor ${error.status}, el cuerpo es: ${error.error.error}`);
    }
    // retornar un mensaje observable con un mensaje de error orientado al usuario.
    console.log(error);

    const claveRespuesta = 'mensaje';
    const mensajeError = error.error[claveRespuesta] || error.message;

    return throwError(`${mensajeError}; por favor, vuelva a intentarlo más tarde.`);
  }
}
