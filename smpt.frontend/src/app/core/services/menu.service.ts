import { Injectable } from '@angular/core';
import { Menu } from '../models/Menu';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { BloqueFuncional } from '../models/BloqueFuncional';
import { find } from 'lodash';
import { ItemBloqueFuncional } from '../models/ItemBloqueFuncional';
import { AuthService } from '../security/auth.service';

/**
 * Servicio que permite el consumo de los servicios del proyecto backend
 *
 * @descripción: Permite consumir los servicios del backend relacionados con el Menu
 *
 * @author: hdiaz
 * @version: 1.0
 * @fecha_de_creación: 05/08/2020
 * @fecha_de_ultima_actualización: 05/08/2020
 */
@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private bloquesFuncionales: BloqueFuncional[];

  constructor() {
    this.bloquesFuncionales = [
      {
        codigo: 'lvlPanelControl',
        codigoEstilo: 'item-name lvlPanelControl',
        nombre: 'Panel',
        nombreIcono: 'dashboard',
        items: [
          {
            codigo: 'pn-inicio',
            nombre: 'Inicio',
            nombreIcono: 'home',
            enlace: '/dashboard/inicio'
          },
          // {
          //   codigo: 'pn-alerta',
          //   nombre: 'Alerta',
          //   nombreIcono: 'home',
          //   enlace: '/dashboard/alerta'
          // }
        ]
      },
      // {
      //   codigo: 'lvlProgramacion',
      //   codigoEstilo: 'item-name lvlProgramacion',
      //   nombre: 'Programación',
      //   nombreIcono: 'event_available',
      //   items: [
      //     {
      //       codigo: 'programacion',
      //       nombre: 'Programación',
      //       nombreIcono: 'home',
      //       enlace: '/programacion/programa-lista'
      //     }
      //   ]
      // },
    ];

    // this.obtenerBloqueFuncional();

  }
  /**
 * Permite obtener los bloques funcionales
 */
  obtenerBloqueFuncional() {
    return this.bloquesFuncionales;
  }
}
