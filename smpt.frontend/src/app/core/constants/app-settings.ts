export class AppSettings {

    /**
     * Identificador interno del tipo de unidad minera UEA
     */
    static PARAM_TUM_ID_UEA = 33;

    /**
     * Identificador interno del tipo de unidad minera de concesión de beneficio.
     * El tipo se encuetra registrado en el la tabla PGIM_TP_VALOR_PARAMETRO cuando
     * PGIM_TP_PARAMETRO.TIPO_UNIDAD_MINERA = TIPO_UNIDAD_MINERA
     */
    static PARAM_TUM_ID_CONCESION_BENEFICIO = 36;

    /**
     * Identificador interno del tipo de unidad minera concesión minera.
     */
    static PARAM_TUM_ID_CONCESION_MINERA = 38;

    /**
     * Identificador interno del tipo de unidad minera acumulación.
     */
    static PARAM_TUM_ID_ACUMULACION = 39;

    /**
     * Identificador interno del tipo de método de minado cuando es subterráneo.
     */
    static PARAM_MM_SUBTERRANEO = 1;

    /**
     * Identificador interno de la empresa involucrada como TITULAR_MINERO.
     */
    static PARAM_TEI_TITULAR_MINERO = 65;
    static PARAM_TIP_ACTA_INICIO = 317;
    static PARAM_TIP_ACTA_SUPERVISION = 318;

    /**
     * Identificador interno del accidente mortal para el tipo de evento.
     */
    static PARAM_TE_ACCIDENTE_MORTAL = 63;

    /**
     * Identificador interno del tipo de documento de identidad cuando este es DNI.
     */
    static PARAM_TIDOC_ID_DNI = 5;

    /**
     * Identificador interno del tipo de documento de identidad cuando este es RUC.
     */
    static PARAM_TIDOC_ID_RUC = 6;

    /**
     * Identificador interno del tipo de documento de identidad cuando este es carné de extranjería.
     */
    static PARAM_TIDOC_ID_CE = 7;

    /**
     * Identificador interno del tipo de relación del flujo de trabajo.
     */
    static PARAM_TIPO_RELACION_PASO_CONTINUAR = 289;

    /**
     * Identificador interno del tipo de relación del flujo de trabajo.
     */
    static PARAM_TIPO_RELACION_PASO_REGRESAR = 290;

    /**
     * Identificador interno del tipo de relación del flujo de trabajo.
     */
    static PARAM_TIPO_RELACION_PASO_CANCELAR = 291;

    /**
     * Identificador interno del tipo de relación del flujo de trabajo.
     */
    static PARAM_TIPO_RELACION_PASO_FINALIZAR = 292;

}
