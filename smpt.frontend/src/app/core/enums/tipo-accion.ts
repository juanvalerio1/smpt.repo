/**
 * Tipo de acción CRUD.
 */
export enum ETipoAccionCRUD {
    NINGUNA = 0,
    CREAR = 1,
    // AGREGAR = 1,
    MODIFICAR = 2,
    ELIMINAR = 3,
    CONSULTAR = 4,
    ADJUNTAR = 5,
    INCLUIR = 6,
    REEMPLAZAR = 7,
}
